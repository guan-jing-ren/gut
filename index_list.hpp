#pragma once

#include "count.hpp"
#include "index.hpp"

namespace gut {
template <index_t... Indices> struct indices : index<Indices>... {
  constexpr static decltype(auto) count();

  static_assert(count(), "Index list should not be empty.");

  template <index_t Index> constexpr indices(index<Index>);
  constexpr indices() = default;
  constexpr indices(const indices &that) = default;
  constexpr indices(indices &&that) = default;
  constexpr indices &operator=(const indices &that) = default;
  constexpr indices &operator=(indices &&that) = default;

  template <index_t Index>
  constexpr decltype(auto) operator[](index<Index>) const;

  template <typename Each> constexpr static decltype(auto) each(Each &&e);
  template <typename Reduce> constexpr static decltype(auto) reduce(Reduce &&m);
};
template <index_t Index> indices(index<Index>)->indices<Index>;

template <index_t Index, index_t... Indices>
constexpr decltype(auto) operator+(index<Index>, indices<Indices...>);

template <index_t... Indices, index_t Index>
constexpr decltype(auto) operator+(indices<Indices...>, index<Index>);

template <index_t Index>
constexpr decltype(auto) operator+(index<Index>, indices<-1>);

template <index_t Index>
constexpr decltype(auto) operator+(indices<-1>, index<Index>);

template <index_t... Indices>
constexpr decltype(auto) operator+(indices<Indices...>, index<-1>);

template <index_t... Indices>
constexpr decltype(auto) operator+(index<-1>, indices<Indices...>);

constexpr decltype(auto) operator+(indices<-1>, index<-1>);

constexpr decltype(auto) operator+(index<-1>, indices<-1>);

constexpr decltype(auto) operator+(index<-1>, index<-1>);

constexpr decltype(auto) operator+(indices<-1>, indices<-1>);

template <index_t Index1, index_t Index2>
constexpr decltype(auto) operator+(index<Index1>, index<Index2>);

template <index_t Index2>
constexpr decltype(auto) operator+(index<-1>, index<Index2>);

template <index_t Index1>
constexpr decltype(auto) operator+(index<Index1>, index<-1>);

template <index_t End> constexpr decltype(auto) make_index_list();

template <index_t Start, index_t End>
constexpr decltype(auto) make_index_list();

template <index_t... Indices>
constexpr decltype(auto) indices<Indices...>::count() {
  return count_v<sizeof...(Indices)>;
}

template <index_t... Indices>
template <index_t Index>
constexpr indices<Indices...>::indices(index<Index>) {}

template <index_t... Indices>
template <index_t Index>
constexpr decltype(auto) indices<Indices...>::operator[](index<Index>) const {
  return index_v<*(std::initializer_list<index_t>{Indices...}.begin() + Index)>;
}

template <index_t... Indices>
template <typename Each>
constexpr decltype(auto) indices<Indices...>::each(Each &&e) {
  [[maybe_unused]] auto rc = (... + (e(index_v<Indices>), 0));
}

template <index_t... Indices>
template <typename Reduce>
constexpr decltype(auto) indices<Indices...>::reduce(Reduce &&m) {
  return m(index_v<Indices>...);
}

template <index_t... Indices, index_t Index>
constexpr decltype(auto) operator+(indices<Indices...>, index<Index>) {
  return indices<Indices..., Index>{};
}

template <index_t Index, index_t... Indices>
constexpr decltype(auto) operator+(index<Index>, indices<Indices...>) {
  return indices<Index, Indices...>{};
}

template <index_t Index>
constexpr decltype(auto) operator+(indices<-1>, index<Index>) {
  return indices{index_v<Index>};
}

template <index_t Index>
constexpr decltype(auto) operator+(index<Index>, indices<-1>) {
  return indices{index_v<Index>};
}

template <index_t... Indices>
constexpr decltype(auto) operator+(indices<Indices...>, index<-1>) {
  return indices<Indices...>{};
}

template <index_t... Indices>
constexpr decltype(auto) operator+(index<-1>, indices<Indices...>) {
  return indices<Indices...>{};
}

constexpr decltype(auto) operator+(indices<-1>, index<-1>) {
  return indices{index_v<-1>};
}

constexpr decltype(auto) operator+(index<-1>, indices<-1>) {
  return indices{index_v<-1>};
}

constexpr decltype(auto) operator+(index<-1>, index<-1>) {
  return indices{index_v<-1>};
}

constexpr decltype(auto) operator+(indices<-1>, indices<-1>) {
  return indices<-1>{};
}

template <index_t Index1, index_t Index2>
constexpr decltype(auto) operator+(index<Index1>, index<Index2>) {
  return indices{index_v<Index1>} + index_v<Index2>;
}

template <index_t Index2>
constexpr decltype(auto) operator+(index<-1>, index<Index2>) {
  return indices{index_v<Index2>};
}

template <index_t Index1>
constexpr decltype(auto) operator+(index<Index1>, index<-1>) {
  return indices{index_v<Index1>};
}

template <index_t End> constexpr decltype(auto) make_index_list() {
  return make_index_list<0, End>();
}

template <index_t Start, index_t End>
constexpr decltype(auto) make_index_list() {
  static_assert(End > Start, "Index list must have at least an element.");
  if constexpr (End - Start == 1)
    return indices{index_v<End - 1>};
  else
    return make_index_list<Start, End - 1>() + index_v<End - 1>;
}
} // namespace gut
