#include "concepts.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

#include "count.hpp"

using namespace gut;

namespace { // Anonymous namespace for canary compile time regression test
static_assert(!std::is_default_constructible_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");
static_assert(!std::is_copy_constructible_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");
static_assert(!std::is_move_constructible_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");
static_assert(!std::is_copy_assignable_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");
static_assert(!std::is_move_assignable_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");
static_assert(!std::is_destructible_v<unsupported>,
              "`unsupported` is intended only for metaprogramming.");

GUT_MEMFN(testable, test(std::declval<testable_args>()...));
GUT_MEMFN(datamember, m_data);

GUT_TYPEDEF(valuetyped, value_type);

GUT_FREEFN(fooable, foo(std::declval<fooable *>()));

struct Testable {
  using value_type = int;
  int m_data;
  void test() {}
  void test(char) {}
  void operator++(int) {}
  void operator++() {}
};

void foo [[maybe_unused]] (Testable *) {}

struct Untestable {};

static_assert(is_testable_v<Testable>,
              "Canary `Testable` should be `testable` as defined.");
static_assert(is_testable_v<Testable, int>,
              "Canary `Testable` should be `testable` as defined.");
static_assert(is_postincrementable_v<Testable>,
              "Canary `Testable` should be `postincrementable` as defined.");
static_assert(is_preincrementable_v<Testable>,
              "Canary `Testable` should be `preincrementable` as defined.");
static_assert(is_datamember_v<Testable>,
              "Canary `Testable` should have `datamember` as defined.");
static_assert(is_valuetyped_v<Testable>,
              "Canary `Testable` should have `valuetyped` as defined.");
static_assert(is_fooable_v<Testable>,
              "Canary `Testable` should be `fooable` as defined.");
static_assert(is_testable_v<Testable &&>,
              "Canary `Testable &&` should be `testable` as defined.");
static_assert(is_testable_v<Testable &&, int>,
              "Canary `Testable &&` should be `testable` as defined.");
static_assert(is_postincrementable_v<Testable &&>,
              "Canary `Testable &&` should be `postincrementable` as defined.");
static_assert(is_preincrementable_v<Testable &&>,
              "Canary `Testable &&` should be `preincrementable` as defined.");
static_assert(is_datamember_v<Testable &&>,
              "Canary `Testable` should have `datamember` as defined.");
static_assert(!is_testable_v<Untestable>,
              "Canary `Untestable` should not be `testable` as defined.");
static_assert(!is_testable_v<Untestable, int>,
              "Canary `Untestable` should not be `testable` as defined.");
static_assert(!is_datamember_v<Untestable>,
              "Canary `Testable` should not have `datamember` as defined.");
static_assert(
    !is_postincrementable_v<Untestable, int>,
    "Canary `Untestable` should not be `postincrementable` as defined.");
static_assert(
    !is_preincrementable_v<Untestable>,
    "Canary `Untestable` should not be `preincrementable` as defined.");
static_assert(!is_valuetyped_v<Untestable>,
              "Canary `Untestable` should not have `valuetyped` as defined.");
static_assert(!is_fooable_v<Untestable>,
              "Canary `Untestable` should not be `fooable` as defined.");

GUT_BINOP(summable, std::declval<const summable &>() +
                        std::declval<const other_summable &>());

struct Summable {};
int operator+ [[maybe_unused]](Summable, Summable) { return 0; }
int operator+ [[maybe_unused]](int, Summable) { return 0; }

static_assert(is_summable_v<int, long>,
              "Fundamental integer types should have inbuilt addition.");
static_assert(is_summable_v<long, int>,
              "Fundamental integer types should have inbuilt addition.");
static_assert(
    is_summable_v<Summable, Summable &>,
    "Canary `Summable` should be `summable` with itselftype as defined.");
static_assert(
    !is_summable_v<Summable, int>,
    "Canary `Summable` should not be `summable` with `int` as defined.");
static_assert(is_summable_v<int, Summable>,
              "`int` should be `summable` with canary `Summable` as defined.");

struct Test {};

static_assert(is_postincrementable_v<int &>,
              "Type expected to be `postincrementable`.");
static_assert(is_postdecrementable_v<int &>,
              "Type expected to be `postdecrementable`.");
static_assert(
    is_callable_v<int (*)(int, int &, int &&, int *), int, int &, int, int *>,
    "Type expected to be `callable`.");
static_assert(is_subscriptable_v<int[], count_t>,
              "Type expected to be `subscriptable`.");
// static_assert(is_memberpointerable_v<Test *>,
//               "Type expected to be `memberpointerable`.");
static_assert(is_pointermemberpointerable_v<Test *, int Test::*>,
              "Type expected to be `pointermemberpointerable`.");
static_assert(is_preincrementable_v<int &>,
              "Type expected to be `preincrementable`.");
static_assert(is_predecrementable_v<int &>,
              "Type expected to be `predecrementable`.");
static_assert(is_unaryplusable_v<int>, "Type expected to be `unaryplusable`.");
static_assert(is_unaryminusable_v<int>,
              "Type expected to be `unaryminusable`.");
static_assert(is_negatable_v<int>, "Type expected to be `negatable`.");
static_assert(is_bitnegatable_v<int>, "Type expected to be `bitnegatable`.");
static_assert(is_dereferenceable_v<int *>,
              "Type expected to be `dereferenceable`.");
static_assert(is_addressable_v<int &>, "Type expected to be `addressable`.");
static_assert(is_newable_v<int>, "Type expected to be `newable`.");
static_assert(is_arraynewable_v<int, gut::count_t>,
              "Type expected to be `arraynewable`.");
static_assert(is_deletable_v<int *>, "Type expected to be `deletable`.");
static_assert(is_arraydeletable_v<int *>,
              "Type expected to be `arraydeletable`.");
static_assert(is_multiplicable_v<int, int>,
              "Type expected to be `multiplicable`.");
static_assert(is_divisible_v<int, int>, "Type expected to be `divisible`.");
static_assert(is_modulable_v<int, int>, "Type expected to be `modulable`.");
static_assert(is_addable_v<int, int>, "Type expected to be `addable`.");
static_assert(is_subtractable_v<int, int>,
              "Type expected to be `subtractable`.");
static_assert(is_leftshiftable_v<int, int>,
              "Type expected to be `leftshiftable`.");
static_assert(is_rightshiftable_v<int, int>,
              "Type expected to be `rightshiftable`.");
static_assert(is_lesscomparable_v<int, int>,
              "Type expected to be `lesscomparable`.");
static_assert(is_lessequalcomparable_v<int, int>,
              "Type expected to be `lessequalcomparable`.");
static_assert(is_greatercomparable_v<int, int>,
              "Type expected to be `greatercomparable`.");
static_assert(is_greaterequalcomparable_v<int, int>,
              "Type expected to be `greaterequalcomparable`.");
static_assert(is_equalcomparable_v<int, int>,
              "Type expected to be `equalcomparable`.");
static_assert(is_notequalcomparable_v<int, int>,
              "Type expected to be `notequalcomparable`.");
static_assert(is_bitandable_v<int, int>, "Type expected to be `bitandable`.");
static_assert(is_bitxorable_v<int, int>, "Type expected to be `bitxorable`.");
static_assert(is_bitorable_v<int, int>, "Type expected to be `bitorable`.");
static_assert(is_andable_v<int, int>, "Type expected to be `andable`.");
static_assert(is_orable_v<int, int>, "Type expected to be `orable`.");
static_assert(is_multiplyassignable_v<int &, int>,
              "Type expected to be `multiplyassignable`.");
static_assert(is_divideassignable_v<int &, int>,
              "Type expected to be `divideassignable`.");
static_assert(is_moduloassignable_v<int &, int>,
              "Type expected to be `moduloassignable`.");
static_assert(is_directassignable_v<int &, int>,
              "Type expected to be `directassignable`.");
static_assert(is_addassignable_v<int &, int>,
              "Type expected to be `addassignable`.");
static_assert(is_subtractassignable_v<int &, int>,
              "Type expected to be `subtractassignable`.");
static_assert(is_leftshiftassignable_v<int &, int>,
              "Type expected to be `leftshiftassignable`.");
static_assert(is_rightshiftassignable_v<int &, int>,
              "Type expected to be `rightshiftassignable`.");
static_assert(is_bitandassignable_v<int &, int>,
              "Type expected to be `bitandassignable`.");
static_assert(is_bitxorassignable_v<int &, int>,
              "Type expected to be `bitxorassignable`.");
static_assert(is_bitorassignable_v<int &, int>,
              "Type expected to be `bitorassignable`.");
static_assert(is_commaable_v<int, int>, "Type expected to be `commaable`.");
} // namespace
