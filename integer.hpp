#pragma once

#include "type.hpp"

namespace gut {
template <count_t> struct integer;
template <typename...>
inline constexpr constant is_integer_v = constant_v<false>;
template <count_t N>
inline constexpr constant is_integer_v<integer<N>> = constant_v<true>;

template <count_t N> struct integer {
  constexpr static decltype(auto) count();

  static_assert(count() <= 64);

  using representation_type = std::conditional_t<
      count() <= 8, std::int8_t,
      std::conditional_t<
          count() <= 16, std::int16_t,
          std::conditional_t<count() <= 32, std::int32_t, std::int64_t>>>;

  constexpr static decltype(auto) value_type_v();

  constexpr integer() = default;
  constexpr integer(const integer &) = default;
  constexpr integer(integer &&) = default;
  constexpr integer &operator=(const integer &) = default;
  constexpr integer &operator=(integer &&) = default;

  constexpr integer(std::int64_t rep);
  template <typename V, V v> constexpr integer(constant<v>);
  template <typename V, V v> constexpr integer(gut::count<v>);
  template <typename V, V v> constexpr integer(index<v>);

  constexpr operator decltype(auto)() const;

  template <count_t M> constexpr integer<N - M> demote() const;
  template <count_t M> constexpr integer<N + M> promote() const;

  constexpr static integer max();
  constexpr static integer min();

#define GUT_DEFINE_BINOP(op)                                                   \
  template <count_t I>                                                         \
  friend constexpr integer<I> operator op(integer<I>, integer<I>)
#define GUT_DEFINE_BOOLBINOP(op)                                               \
  template <count_t I, count_t J>                                              \
  friend constexpr bool operator op(integer<I>, integer<J>)
#define GUT_DEFINE_BINEQ(op)                                                   \
  template <count_t I>                                                         \
  friend constexpr integer<I> &operator op(integer<I> &, integer<I>);          \
  template <count_t I>                                                         \
  friend constexpr integer<I> &&operator op(integer<I> &&, integer<I>)
#define GUT_DEFINE_PREFIX(op)                                                  \
  template <count_t I> friend constexpr integer<I> operator op(integer<I>)
#define GUT_DEFINE_PRECREMENT(op)                                              \
  template <count_t I> friend constexpr integer<I> &operator op(integer<I> &); \
  template <count_t I> friend constexpr integer<I> &&operator op(integer<I> &&)
#define GUT_DEFINE_BOOLPREFIX(op)                                              \
  template <count_t I> friend constexpr bool operator op(integer<I>)
#define GUT_DEFINE_POSTFIX(op)                                                 \
  template <count_t I>                                                         \
  friend constexpr integer<I> operator op(integer<I> &, int);                  \
  template <count_t I>                                                         \
  friend constexpr integer<I> operator op(integer<I> &&, int)

  GUT_DEFINE_POSTFIX(++);
  GUT_DEFINE_POSTFIX(--);
  GUT_DEFINE_PRECREMENT(++);
  GUT_DEFINE_PRECREMENT(--);
  GUT_DEFINE_PREFIX(+);
  GUT_DEFINE_PREFIX(-);
  GUT_DEFINE_BOOLPREFIX(!);
  GUT_DEFINE_PREFIX(~);
  // new, arraynew, delete, arraydelete
  GUT_DEFINE_BINOP(*);
  GUT_DEFINE_BINOP(/);
  GUT_DEFINE_BINOP(%);
  GUT_DEFINE_BINOP(+);
  GUT_DEFINE_BINOP(-);
  GUT_DEFINE_BINOP(<<);
  GUT_DEFINE_BINOP(>>);
  GUT_DEFINE_BOOLBINOP(<);
  GUT_DEFINE_BOOLBINOP(<=);
  GUT_DEFINE_BOOLBINOP(>);
  GUT_DEFINE_BOOLBINOP(>=);
  GUT_DEFINE_BOOLBINOP(==);
  GUT_DEFINE_BOOLBINOP(!=);
  GUT_DEFINE_BINOP(&);
  GUT_DEFINE_BINOP(^);
  GUT_DEFINE_BINOP(|);
  GUT_DEFINE_BOOLBINOP(&&);
  GUT_DEFINE_BOOLBINOP(||);
  GUT_DEFINE_BINEQ(*=);
  GUT_DEFINE_BINEQ(/=);
  GUT_DEFINE_BINEQ(%=);
  GUT_DEFINE_BINEQ(+=);
  GUT_DEFINE_BINEQ(-=);
  GUT_DEFINE_BINEQ(<<=);
  GUT_DEFINE_BINEQ(>>=);
  GUT_DEFINE_BINEQ(&=);
  GUT_DEFINE_BINEQ(^=);
  GUT_DEFINE_BINEQ(|=);

#undef GUT_DEFINE_BINOP
#undef GUT_DEFINE_BOOLBINOP
#undef GUT_DEFINE_BINEQ
#undef GUT_DEFINE_PREFIX
#undef GUT_DEFINE_PRECREMENT
#undef GUT_DEFINE_BOOLPREFIX
#undef GUT_DEFINE_POSTFIX

private:
  representation_type m_num : count().value();
};
template <typename V, V v> integer(constant<v>)->integer<max_bits(v) + 1>;
template <typename V, V v> integer(count<v>)->integer<max_bits(v) + 1>;
template <typename V, V v> integer(index<v>)->integer<max_bits(v) + 1>;

////////////////////
// IMPLEMENTATION //
////////////////////

template <count_t N> constexpr decltype(auto) integer<N>::count() {
  return count_v<N>;
}

template <count_t N> constexpr decltype(auto) integer<N>::value_type_v() {
  return gut::type_v<representation_type>;
}

template <count_t N>
constexpr integer<N>::integer(std::int64_t rep) : m_num(rep) {}

template <count_t N>
template <typename V, V v>
constexpr integer<N>::integer(constant<v>) : m_num(v) {}

template <count_t N>
template <typename V, V v>
constexpr integer<N>::integer(gut::count<v>) : m_num(v) {}

template <count_t N>
template <typename V, V v>
constexpr integer<N>::integer(index<v>) : m_num(v) {}

#define GUT_IMPL_BINOP(op)                                                     \
  template <count_t I>                                                         \
  constexpr integer<I> operator op(integer<I> left, integer<I> right) {        \
    return integer<I>(left.m_num op right.m_num);                              \
  }
#define GUT_IMPL_BOOLBINOP(op)                                                 \
  template <count_t I, count_t J>                                              \
  constexpr bool operator op(integer<I> left, integer<J> right) {              \
    return left.m_num op right.m_num;                                          \
  }
#define GUT_IMPL_BINEQ(op)                                                     \
  template <count_t I>                                                         \
  constexpr integer<I> &operator op(integer<I> &left, integer<I> right) {      \
    left.m_num op right.m_num;                                                 \
    return left;                                                               \
  }                                                                            \
  template <count_t I>                                                         \
  constexpr integer<I> &&operator op(integer<I> &&left, integer<I> right) {    \
    left.m_num op right.m_num;                                                 \
    return left;                                                               \
  }
#define GUT_IMPL_PREFIX(op)                                                    \
  template <count_t I> constexpr integer<I> operator op(integer<I> i) {        \
    return integer<I>(op i.m_num);                                             \
  }
#define GUT_IMPL_PRECREMENT(op)                                                \
  template <count_t I> constexpr integer<I> &operator op(integer<I> &i) {      \
    op i.m_num;                                                                \
    return i;                                                                  \
  }                                                                            \
  template <count_t I> constexpr integer<I> &&operator op(integer<I> &&i) {    \
    op i.m_num;                                                                \
    return std::move(i);                                                       \
  }
#define GUT_IMPL_BOOLPREFIX(op)                                                \
  template <count_t I> constexpr bool operator op(integer<I> i) {              \
    return op i.m_num;                                                         \
  }
#define GUT_IMPL_POSTFIX(op)                                                   \
  template <count_t I> constexpr integer<I> operator op(integer<I> &i, int) {  \
    return integer<I>(i.m_num op);                                             \
  }                                                                            \
  template <count_t I> constexpr integer<I> operator op(integer<I> &&i, int) { \
    return integer<I>(i.m_num op);                                             \
  }

GUT_IMPL_POSTFIX(++)
GUT_IMPL_POSTFIX(--)
GUT_IMPL_PRECREMENT(++)
GUT_IMPL_PRECREMENT(--)
GUT_IMPL_PREFIX(+)
GUT_IMPL_PREFIX(-)
GUT_IMPL_BOOLPREFIX(!)
GUT_IMPL_PREFIX(~)
// new, arraynew, delete, arraydelete
GUT_IMPL_BINOP(*)
GUT_IMPL_BINOP(/)
GUT_IMPL_BINOP(%)
GUT_IMPL_BINOP(+)
GUT_IMPL_BINOP(-)
GUT_IMPL_BINOP(<<)
GUT_IMPL_BINOP(>>)
GUT_IMPL_BOOLBINOP(<)
GUT_IMPL_BOOLBINOP(<=)
GUT_IMPL_BOOLBINOP(>)
GUT_IMPL_BOOLBINOP(>=)
GUT_IMPL_BOOLBINOP(==)
GUT_IMPL_BOOLBINOP(!=)
GUT_IMPL_BINOP(&)
GUT_IMPL_BINOP(^)
GUT_IMPL_BINOP(|)
GUT_IMPL_BOOLBINOP(&&)
GUT_IMPL_BOOLBINOP(||)
GUT_IMPL_BINEQ(*=)
GUT_IMPL_BINEQ(/=)
GUT_IMPL_BINEQ(%=)
GUT_IMPL_BINEQ(+=)
GUT_IMPL_BINEQ(-=)
GUT_IMPL_BINEQ(<<=)
GUT_IMPL_BINEQ(>>=)
GUT_IMPL_BINEQ(&=)
GUT_IMPL_BINEQ(^=)
GUT_IMPL_BINEQ(|=)

#undef GUT_IMPL_BINOP
#undef GUT_IMPL_BOOLBINOP
#undef GUT_IMPL_BINEQ
#undef GUT_IMPL_PREFIX
#undef GUT_IMPL_PRECREMENT
#undef GUT_IMPL_BOOLPREFIX
#undef GUT_IMPL_POSTFIX

template <count_t N> constexpr integer<N>::operator decltype(auto)() const {
  return m_num;
}

template <count_t N>
template <count_t M>
constexpr integer<N - M> integer<N>::demote() const {
  static_assert(M >= 0);
  return {m_num};
}

template <count_t N>
template <count_t M>
constexpr integer<N + M> integer<N>::promote() const {
  static_assert(M >= 0);
  return {m_num};
}

template <count_t N> constexpr auto integer<N>::max() -> integer {
  return ~min();
}

template <count_t N> constexpr auto integer<N>::min() -> integer {
  return (integer{1} << (count() - 1));
}
} // namespace gut
