#pragma once

#include "bitfield.hpp"
#include "ntuple.hpp"
#include "optional.hpp"

namespace gut {
template <count_t BitCount> constexpr decltype(auto) bitmap();

template <typename ArrayType> struct optional_buffer {
  using array_type = ArrayType;
  constexpr static decltype(auto) array_type_v();
  static_assert(array_type_v().is_array());
  using value_type = std::remove_all_extents_t<ArrayType>;
  constexpr static decltype(auto) count();

  constexpr optional_buffer() = default;
  constexpr optional_buffer(const optional_buffer &) = default;
  constexpr optional_buffer(optional_buffer &&) = default;
  constexpr optional_buffer &operator=(const optional_buffer &) = default;
  constexpr optional_buffer &operator=(optional_buffer &&) = default;
  constexpr optional_buffer(type<array_type>);
  constexpr optional_buffer &operator=(
      forwarding_ntuple<integer<max_bits(static_cast<index_t>(count())) + 1>,
                        const value_type &>);

  constexpr operator bool() const;

  constexpr decltype(auto)
      value(integer<max_bits(static_cast<index_t>(count())) + 1> i) const;
  constexpr decltype(auto)
      value(integer<max_bits(static_cast<index_t>(count())) + 1> i,
            const value_type &value);

  constexpr decltype(auto)
      reset(integer<max_bits(static_cast<index_t>(count())) + 1> i);

private:
  union Union {
    constexpr Union() : m_something() {}
    constexpr Union(const Union &) = default;
    constexpr Union(Union &&) = default;
    constexpr Union &operator=(const Union &) = default;
    constexpr Union &operator=(Union &&) = default;
    constexpr Union(const value_type &val);

    something m_something{};
    value_type m_val;
  };

  decltype(bitmap<std::extent_v<ArrayType>>()) m_bitmap{};
  buffer<Union, std::extent_v<ArrayType>> m_union{{}};
};

////////////////////
// IMPLEMENTATION //
////////////////////

template <count_t BitCount> constexpr decltype(auto) bitmap() {
  return make_index_list<BitCount>().reduce(
      [](auto... i) mutable constexpr->decltype(auto) {
        return bitfield<(BitCount - i)..., 0>{};
      });
}

template <typename ArrayType>
constexpr decltype(auto) optional_buffer<ArrayType>::array_type_v() {
  return type_v<array_type>;
}

template <typename ArrayType>
constexpr decltype(auto) optional_buffer<ArrayType>::count() {
  return array_type_v().extent();
}

template <typename ArrayType>
constexpr optional_buffer<ArrayType>::optional_buffer(type<array_type>)
    : m_bitmap(), m_union() {}

template <typename ArrayType>
constexpr auto optional_buffer<ArrayType>::operator=(
    forwarding_ntuple<integer<max_bits(static_cast<index_t>(count())) + 1>,
                      const value_type &>
        value) -> optional_buffer & {
  if (m_bitmap[value.get(index_v<0>)])
    m_union[value.get(index_v<0>)].m_val = value.get(index_v<1>);
  else {
    reset(value.get(index_v<0>));
    m_union[value.get(index_v<0>)] = Union{value.get(index_v<1>)};
    m_bitmap[value.get(index_v<0>)] = 1;
  }
  return *this;
}

template <typename ArrayType>
constexpr optional_buffer<ArrayType>::operator bool() const {
  return m_bitmap.template cast<std::extent_v<ArrayType>, 0>() > 0;
}

template <typename ArrayType>
constexpr decltype(auto) optional_buffer<ArrayType>::value(
    integer<max_bits(static_cast<index_t>(count())) + 1> i) const {
  optional<const value_type &> value;
  if (m_bitmap[i])
    value = m_union[i].m_val;
  return value;
}

template <typename ArrayType>
constexpr decltype(auto) optional_buffer<ArrayType>::value(
    integer<max_bits(static_cast<index_t>(count())) + 1> i,
    const value_type &value) {
  *this = forwarding_ntuple{i, value};
}

template <typename ArrayType>
constexpr decltype(auto) optional_buffer<ArrayType>::reset(
    integer<max_bits(static_cast<index_t>(count())) + 1> i) {
  if (m_bitmap[i]) {
    if constexpr (!std::is_trivially_destructible_v<Union>)
      m_union[i].m_val.~value_type();
    m_union[i] = Union{};
    m_bitmap[i] = false;
  }
}

template <typename ArrayType>
constexpr optional_buffer<ArrayType>::Union::Union(const value_type &val)
    : m_val(val) {}
} // namespace gut
