#include "thing.hpp"

#include "type_list.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(std::is_trivially_default_constructible_v<thing<int>>,
              "`thing` of trivially default constructible type should be "
              "trivially default constructible.");
static_assert(std::is_trivially_copy_constructible_v<thing<int>>,
              "`thing` of trivially copy constructible type should be "
              "trivially copy constructible.");
static_assert(std::is_trivially_move_constructible_v<thing<int>>,
              "`thing` of trivially move constructible type should be "
              "trivially move constructible.");
static_assert(std::is_trivially_copy_assignable_v<thing<int>>,
              "`thing` of trivially copy assignable type should be "
              "trivially copy assignable.");
static_assert(std::is_trivially_move_assignable_v<thing<int>>,
              "`thing` of move assignable type should be "
              "move assignable.");
static_assert(std::is_trivially_destructible_v<thing<int>>,
              "`thing` of trivially destructible type should be "
              "trivially destructible.");
static_assert(
    !std::is_trivially_default_constructible_v<thing<int &>>,
    "`thing` of reference type should not be trivially default constructible.");
static_assert(std::is_trivially_copy_constructible_v<thing<int &>>,
              "`thing` of trivially copy constructible type should be "
              "trivially copy constructible.");
static_assert(std::is_trivially_move_constructible_v<thing<int &>>,
              "`thing` of trivially move constructible type should be "
              "trivially move constructible.");
static_assert(std::is_trivially_copy_assignable_v<thing<int &>>,
              "`thing` of trivially copy assignable type should be "
              "trivially copy assignable.");
static_assert(std::is_trivially_move_assignable_v<thing<int &>>,
              "`thing` of move assignable type should be "
              "move assignable.");
static_assert(std::is_trivially_destructible_v<thing<int &>>,
              "`thing` of trivially destructible type should be "
              "trivially destructible.");
constexpr thing it = []() {
  thing it = 0;
  ref it_ref = it.get();
  static_assert(type_v<int &> == it_ref.declared_type_v());
  it = thing{long{1l}};
  it = it_ref;
  auto that = std::move(it);
  return that;
}();
static_assert(type_v<const int &> == type_v<decltype(it.get())>,
              "Expected type as specified.");
static_assert(it.get() == 1, "Expected value as specified.");
} // namespace
