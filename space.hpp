#pragma once

#include "bitfield.hpp"
#include "functor.hpp"
// To be used for function spaces that store metadata about space to reduce
// duplication, eg:
// - enum class GetSpaceProperty { size, data, begin, end, rebase, truncate };
// - then, m_mapper(variant<constant<size>, constant<data>, constant<begin>,
//   constant<end>>);
//
// Also to be used for partitioned spaces with variant_partition
#include "variant.hpp"

namespace gut {
template <typename ValueType>
constexpr decltype(auto) min(ValueType &&l, ValueType &&r) {
  return std::forward<ValueType>(l <= r ? l : r);
}

template <typename ValueType>
constexpr decltype(auto) max(ValueType &&l, ValueType &&r) {
  return std::forward<ValueType>(l < r ? r : l);
}

template <typename ValueType>
constexpr decltype(auto) clamp(ValueType &&mi, ValueType &&value,
                               ValueType &&ma) {
  return std::forward<ValueType>(
      max(std::forward<ValueType>(mi),
          std::forward<ValueType>(min(std::forward<ValueType>(value),
                                      std::forward<ValueType>(ma)))));
}

template <typename MeasureType> struct key;

template <typename MeasureType> struct key_base {};

template <> struct key_base<bool> { using representation_type = bool; };

template <count_t BitSize> struct key_base<integer<BitSize>> {
  using representation_type = integer<BitSize>;

  constexpr key_base() = default;
  constexpr key_base(const key_base &) = default;
  constexpr key_base(key_base &&) = default;
  constexpr key_base &operator=(const key_base &) = default;
  constexpr key_base &operator=(key_base &&) = default;
  constexpr key_base(representation_type k) : m_key(k) {}

  constexpr decltype(auto) clamped(const key_base &min,
                                   const key_base &max) const {
    key<representation_type> clamp{m_key};
    clamp.clamped(min, max);
    return clamp;
  }
  constexpr decltype(auto) clamped(const key_base &min, const key_base &max) {
    m_key = clamp<count_t>(min.m_key, m_key, max.m_key);
    return *this;
  }

  constexpr decltype(auto) reverse() {
    m_key = -m_key;
    return *this;
  }

  template <count_t Bits>
  friend constexpr bool operator==(const key_base<integer<Bits>> &,
                                   const key_base<integer<Bits>> &);

  template <count_t Bits>
  friend constexpr key_base<integer<Bits>> &
  operator++(const key_base<integer<Bits>> &);

  template <count_t Bits>
  friend constexpr key<integer<Bits>>
  operator+(const key_base<integer<Bits>> &, const key_base<integer<Bits>> &);

  template <count_t Bits>
  friend constexpr key<integer<Bits>>
  operator-(const key_base<integer<Bits>> &, const key_base<integer<Bits>> &);

private:
  template <typename, typename> friend struct space_base;

  representation_type m_key;
};

template <count_t Bits>
constexpr bool operator==(const key_base<integer<Bits>> &l,
                          const key_base<integer<Bits>> &r) {
  return l.m_key == r.m_key;
}

template <count_t Bits>
constexpr key_base<integer<Bits>> &
operator++(const key_base<integer<Bits>> &k) {
  ++k.m_key;
  return k;
}

template <count_t Bits>
constexpr key<integer<Bits>> operator+(const key_base<integer<Bits>> &l,
                                       const key_base<integer<Bits>> &r) {
  return {l.m_key + r.m_key};
}

template <count_t Bits>
constexpr key<integer<Bits>> operator-(const key_base<integer<Bits>> &l,
                                       const key_base<integer<Bits>> &r) {
  return {l.m_key - r.m_key};
}

template <typename BitfieldType> struct key_bitfield_base {
  using representation_type = BitfieldType;
  using projection_type =
      integer<max_bits(representation_type::count().value()) + 1>;

  constexpr key_bitfield_base() = default;
  constexpr key_bitfield_base(const key_bitfield_base &) = default;
  constexpr key_bitfield_base(key_bitfield_base &&) = default;
  constexpr key_bitfield_base &operator=(const key_bitfield_base &) = default;
  constexpr key_bitfield_base &operator=(key_bitfield_base &&) = default;
  constexpr key_bitfield_base(representation_type k) : m_key(k), m_proj() {}

  constexpr decltype(auto) clamped(const key_bitfield_base &min,
                                   const key_bitfield_base &max) const {
    key clamp{m_key};
    clamp.clamped(min, max);
    return clamp;
  }

  constexpr decltype(auto) clamped(const key_bitfield_base &min,
                                   const key_bitfield_base &max) {
    make_index_list<representation_type::count()>().each(
        [this, &min, &max](auto i) {
          m_key[i] = clamp<count_t>(min.m_key[i], m_key[i], max.m_key[i]);
        });
    return *this;
  }

  constexpr decltype(auto) rotate() {
    m_proj = (m_proj + 1) %
             projection_type{
                 static_cast<typename projection_type::representation_type>(
                     representation_type::count())};
    return *this;
  }
  constexpr decltype(auto) reverse() {
    make_index_list<representation_type::count()>()
        .each([this](auto i) mutable constexpr->decltype(auto) {
          if (i == m_proj)
            m_key[i] = -m_key[i];
        });
    return *this;
  }

  template <typename BitFieldType>
  friend constexpr bool operator==(const key_bitfield_base<BitFieldType> &,
                                   const key_bitfield_base<BitFieldType> &);

  template <typename BitFieldType>
  friend constexpr key_bitfield_base<BitFieldType> &
  operator++(key_bitfield_base<BitFieldType> &);

  template <typename BitFieldType>
  friend constexpr key<BitFieldType>
  operator+(const key_bitfield_base<BitFieldType> &,
            const key_bitfield_base<BitFieldType> &);

  template <typename BitFieldType>
  friend constexpr key<BitFieldType>
  operator-(const key_bitfield_base<BitFieldType> &,
            const key_bitfield_base<BitFieldType> &);

private:
  template <typename, typename> friend struct space_base;
  template <typename, bool, bool> friend struct space_array_base;
  representation_type m_key;
  projection_type m_proj;
};

template <typename BitfieldType>
constexpr bool operator==(const key_bitfield_base<BitfieldType> &l,
                          const key_bitfield_base<BitfieldType> &r) {
  return l.m_key == r.m_key;
}

template <typename BitfieldType>
constexpr key_bitfield_base<BitfieldType> &
operator++(key_bitfield_base<BitfieldType> &k) {
  make_index_list<BitfieldType::count()>().each([&k](auto i) mutable constexpr {
    if (i == k.m_proj)
      k.m_key[i] = k.m_key[i] + 1;
  });
  return k;
}

template <typename BitfieldType>
constexpr key<BitfieldType>
operator+(const key_bitfield_base<BitfieldType> &l,
          const key_bitfield_base<BitfieldType> &r) {
  return make_index_list<BitfieldType::count()>().reduce(
      [&l, &r ](auto... i) mutable constexpr->decltype(auto) {
        return key<BitfieldType>{{(l.m_key[i] + r.m_key[i])...}};
      });
}

template <typename BitfieldType>
constexpr key<BitfieldType>
operator-(const key_bitfield_base<BitfieldType> &l,
          const key_bitfield_base<BitfieldType> &r) {
  return make_index_list<BitfieldType::count()>().reduce(
      [&l, &r ](auto... i) mutable constexpr->decltype(auto) {
        return key<BitfieldType>{{(l.m_key[i] - r.m_key[i])...}};
      });
}

template <typename MeasureType>
struct key : std::conditional_t<is_bitfield_v<MeasureType>,
                                key_bitfield_base<MeasureType>,
                                key_base<MeasureType>> {
  using measure_type = MeasureType;
  using key_base = std::conditional_t<is_bitfield_v<MeasureType>,
                                      key_bitfield_base<MeasureType>,
                                      gut::key_base<MeasureType>>;

  using key_base::key_base;
  using key_base::operator=;

private:
  template <typename, typename> friend struct space;
};

template <typename MeasureType> key(MeasureType)->key<MeasureType>;

template <typename CoordinateType, typename SpanType> struct space;

template <typename CoordinateType, typename SpanType> struct space_base {
  template <typename... Args> constexpr space_base(Args &&...) {}

  static constexpr something span() { return {}; }
};

template <typename CoordinateType> struct space_base<CoordinateType, bool> {};

template <typename ValueType, count_t BitSize>
struct space_base<ValueType *, integer<BitSize>> {
  using pointer_type = integer<BitSize>;

  constexpr space_base() = default;
  constexpr space_base(const space_base &) = default;
  constexpr space_base(space_base &&) = default;
  constexpr space_base &operator=(const space_base &) = default;
  constexpr space_base &operator=(space_base &&) = default;

  template <count_t ElementCount>
  constexpr space_base(ValueType (&base)[ElementCount])
      : space_base(base,
                   pointer_type{
                       static_cast<typename pointer_type::representation_type>(
                           ElementCount)}) {}

  constexpr pointer_type span() const { return m_span; }

  constexpr decltype(auto) contains(key<pointer_type>) const;

  constexpr decltype(auto) rebase(key<pointer_type>) const;
  constexpr decltype(auto) rebase(key<pointer_type>);
  constexpr decltype(auto) truncate(key<pointer_type>) const;
  constexpr decltype(auto) truncate(key<pointer_type>);

  constexpr decltype(auto) operator[](key<pointer_type>) const;
  constexpr decltype(auto) operator[](key<pointer_type>);

private:
  ValueType *m_base;
  pointer_type m_span;

  constexpr space_base(ValueType *base, pointer_type span)
      : m_base(base), m_span(span) {}
};

template <typename ValueType, count_t BitSize>
constexpr decltype(auto)
space_base<ValueType *, integer<BitSize>>::contains(key<pointer_type> k) const {
  return k.m_key < m_span;
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto)
space_base<ValueType *, integer<BitSize>>::rebase(key<pointer_type> k) const {
  k.clamped({}, {m_span});
  return space{m_base + k.m_key, pointer_type{m_span - k.m_key}};
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto)
space_base<ValueType *, integer<BitSize>>::rebase(key<pointer_type> k) {
  k.clamped({}, {m_span});
  return space{m_base + k.m_key, pointer_type{m_span - k.m_key}};
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto)
space_base<ValueType *, integer<BitSize>>::truncate(key<pointer_type> k) const {
  k.clamped({}, {m_span});
  return space{m_base, pointer_type{m_span - k.m_key}};
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto)
space_base<ValueType *, integer<BitSize>>::truncate(key<pointer_type> k) {
  k.clamped({}, {m_span});
  return space{m_base, pointer_type{m_span - k.m_key}};
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto) space_base<ValueType *, integer<BitSize>>::
operator[](key<pointer_type> k) const {
  return contains(k) ? optional<const ValueType &>{*(m_base + k.m_key)}
                     : optional<const ValueType &>{};
}

template <typename ValueType, count_t BitSize>
constexpr decltype(auto) space_base<ValueType *, integer<BitSize>>::
operator[](key<pointer_type> k) {
  return contains(k) ? optional<ValueType &>{*(m_base + k.m_key)}
                     : optional<ValueType &>{};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
struct space_array_base {
  using array_type = ArrayType;
  using pointer_type = decltype(make_span_type<array_type>());
  constexpr static decltype(auto) span_count() {
    return count_v < Rebased ? 2 : 1 > ;
  }
  constexpr static bool traverse_multi() { return TraverseMulti; }
  using span_type = buffer<pointer_type, span_count()>;

  constexpr space_array_base() = default;
  constexpr space_array_base(const space_array_base &) = default;
  constexpr space_array_base(space_array_base &&) = default;
  constexpr space_array_base &operator=(const space_array_base &) = default;
  constexpr space_array_base &operator=(space_array_base &&) = default;

  constexpr space_array_base(array_type &base)
      : space_array_base(base, make_span_type<array_type>()){};
  constexpr pointer_type span() const {
    if constexpr (span_count() == 1)
      return m_span.back();
    else {
      auto k = key{m_span.back()} - key{m_span.front()};
      return k.m_key;
    }
  };

  constexpr decltype(auto) contains(key<pointer_type>) const;

  constexpr decltype(auto) rebase(key<pointer_type>) const;
  constexpr decltype(auto) rebase(key<pointer_type>);
  constexpr decltype(auto) truncate(key<pointer_type>) const;
  constexpr decltype(auto) truncate(key<pointer_type>);

  constexpr decltype(auto) operator[](key<pointer_type>) const;
  constexpr decltype(auto) operator[](key<pointer_type>);

  template <typename Map> constexpr decltype(auto) map(Map &&m) {
    return space{
        function([ self = *this, m = std::forward<Map>(m) ](
                     key<pointer_type> span) mutable constexpr->decltype(auto) {
          return (self[span]).map(m);
        }),
        m_span};
  }

private:
  template <typename, bool, bool> friend struct space_array_base;

  array_type *m_base = nullptr;
  span_type m_span;

  constexpr space_array_base(
      array_type &base,
      std::conditional_t<span_count() == 1, pointer_type, span_type> span)
      : m_base(&base), m_span({span}){};
};

template <typename Array, typename Index, typename... Indices>
constexpr decltype(auto) recurse(Array &&array, Index index,
                                 Indices &&... indices) {
  if constexpr (sizeof...(Indices) > 0)
    return recurse(array[index], indices...);
  else
    return optional<decltype(array[index])>{array[index]};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto)
space_array_base<ArrayType, Rebased, TraverseMulti>::contains(
    key<pointer_type> k) const {
  if constexpr (span_count() > 1)
    k = k + key{m_span.front()};
  return make_index_list<pointer_type::count()>().reduce(
      [ this, &k ](auto... i) mutable constexpr->decltype(auto) {
        return (... &&
                ((span_count() == 1 ? 0 : m_span.front()[i]) <= k.m_key[i])) &&
               (... && (m_span.back()[i] > k.m_key[i]));
      });
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto)
space_array_base<ArrayType, Rebased, TraverseMulti>::rebase(
    key<pointer_type> k) const {
  return space<array_type, buffer<pointer_type, 2>>{
      *m_base,
      {{(span_count() == 1 ? k.clamped({}, m_span.back())
                           : (key<pointer_type>{m_span.front()} + k)
                                 .clamped(m_span.front(), m_span.back()))
            .m_key,
        m_span.back()}}};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto)
space_array_base<ArrayType, Rebased, TraverseMulti>::rebase(
    key<pointer_type> k) {
  return space<array_type, buffer<pointer_type, 2>>{
      *m_base,
      {{(span_count() == 1 ? k.clamped({}, m_span.back())
                           : (key<pointer_type>{m_span.front()} + k)
                                 .clamped(m_span.front(), m_span.back()))
            .m_key,
        m_span.back()}}};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto)
space_array_base<ArrayType, Rebased, TraverseMulti>::truncate(
    key<pointer_type> k) const {
  auto make_span = [ this, &k ]() mutable constexpr->decltype(auto) {
    if constexpr (span_count() == 1)
      return (key<pointer_type>{m_span.back()} - k)
          .clamped({}, m_span.back())
          .m_key;
    else
      return buffer{{m_span.front(), (key<pointer_type>{m_span.back()} - k)
                                         .clamped({}, m_span.back())
                                         .m_key}};
  };
  return space<array_type, span_type>{*m_base, make_span()};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto)
space_array_base<ArrayType, Rebased, TraverseMulti>::truncate(
    key<pointer_type> k) {
  auto make_span = [ this, &k ]() mutable constexpr->decltype(auto) {
    if constexpr (span_count() == 1)
      return (key<pointer_type>{m_span.back()} - k)
          .clamped({}, m_span.back())
          .m_key;
    else
      return buffer{{m_span.front(), (key<pointer_type>{m_span.back()} - k)
                                         .clamped({}, m_span.back())
                                         .m_key}};
  };
  return space<array_type, span_type>{*m_base, make_span()};
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto) space_array_base<ArrayType, Rebased, TraverseMulti>::
operator[](key<pointer_type> k) const {
  return make_index_list<pointer_type::count()>().reduce(
      [ this, &k ](auto... i) mutable constexpr->decltype(auto) {
        using result_type =
            decltype(recurse(*m_base, static_cast<count_t>(k.m_key[i])...));
        return contains(k)
                   ? recurse(*m_base, static_cast<count_t>(k.m_key[i])...)
                   : result_type{};
      });
}

template <typename ArrayType, bool Rebased, bool TraverseMulti>
constexpr decltype(auto) space_array_base<ArrayType, Rebased, TraverseMulti>::
operator[](key<pointer_type> k) {
  return make_index_list<pointer_type::count()>().reduce(
      [ this, &k ](auto... i) mutable constexpr->decltype(auto) {
        using result_type =
            decltype(recurse(*m_base, static_cast<count_t>(k.m_key[i])...));
        return contains(k)
                   ? recurse(*m_base, static_cast<count_t>(k.m_key[i])...)
                   : result_type{};
      });
}

template <typename ValueType, count_t ElementCount, count_t SpanCount>
constexpr decltype(auto) choose_space_base() {
  if constexpr (!std::is_array_v<ValueType>)
    return space_base<ValueType *, integer<max_bits(ElementCount) + 1>>{};
  else
    return space_array_base<ValueType[ElementCount], SpanCount == 2, false>{};
}

template <typename ValueType, count_t ElementCount, count_t SpanCount>
struct space_base<
    ValueType[ElementCount],
    buffer<decltype(make_span_type<ValueType[ElementCount]>()), SpanCount>>
    : decltype(choose_space_base<ValueType, ElementCount, SpanCount>()) {
  using space_base_type =
      decltype(choose_space_base<ValueType, ElementCount, SpanCount>());
  using space_base_type::space_base_type;
};

struct space_stream_base {
  constexpr space_stream_base(const space_stream_base &) = delete;
  constexpr space_stream_base &operator=(const space_stream_base &) = delete;
  constexpr space_stream_base(space_stream_base &&) = default;
  constexpr space_stream_base &operator=(space_stream_base &&) = default;
};

template <typename MapFunctionType, typename Arity, typename SpanType>
struct space_base<function<MapFunctionType, Arity>, SpanType>
    : private std::conditional_t<function<MapFunctionType, Arity>::arity() == 0,
                                 space_stream_base,
                                 space_base<something, something>> {
  using image_type = function<MapFunctionType, Arity>;
  using span_type = SpanType;

  constexpr space_base() = delete;
  constexpr space_base(const space_base &) = default;
  constexpr space_base(space_base &&) = default;
  constexpr space_base &operator=(const space_base &) = default;
  constexpr space_base &operator=(space_base &&) = default;
  constexpr space_base(image_type mapper, span_type span)
      : m_mapper(mapper), m_span(span) {}

  template <typename KeyType> constexpr decltype(auto) operator[](KeyType k) {
    return m_mapper(key{k});
  }

  template <typename KeyType>
  constexpr decltype(auto) operator[](KeyType k) const {
    return m_mapper(key{k});
  }

private:
  image_type m_mapper;
  span_type m_span;
};

template <typename CoordinateType, typename SpanType>
struct space : space_base<CoordinateType, SpanType> {
  using coordinate_type = CoordinateType;
  using span_type = SpanType;
  using space_base = gut::space_base<CoordinateType, SpanType>;

  constexpr space() = default;
  constexpr space(const space &) = default;
  constexpr space(space &&) = default;
  constexpr space &operator=(const space &) = default;
  constexpr space &operator=(space &&) = default;

  constexpr operator bool() const;

  using space_base::operator[];

  constexpr space sub_space();

private:
  template <typename, typename> friend struct gut::space_base;
  template <typename, bool, bool> friend struct gut::space_array_base;

  using space_base::space_base;
};

template <typename CoordinateType>
space(CoordinateType, CoordinateType)
    ->space<CoordinateType, decltype(std::declval<CoordinateType>() -
                                     std::declval<CoordinateType>())>;

template <typename CoordinateType, typename SpanType>
space(CoordinateType, SpanType)->space<CoordinateType, SpanType>;

template <typename ValueType, count_t ElementCount>
space(const ValueType (&)[ElementCount])
    ->space<const ValueType[ElementCount],
            buffer<decltype(make_span_type<ValueType[ElementCount]>()), 1>>;

template <typename ValueType, count_t ElementCount>
space(ValueType (&)[ElementCount])
    ->space<ValueType[ElementCount],
            buffer<decltype(make_span_type<ValueType[ElementCount]>()), 1>>;

template <typename ValueType, count_t ElementCount>
space(const ValueType (&)[ElementCount],
      decltype(make_span_type<ValueType[ElementCount]>()))
    ->space<const ValueType[ElementCount],
            buffer<decltype(make_span_type<ValueType[ElementCount]>()), 1>>;

template <typename ValueType, count_t ElementCount>
space(ValueType (&)[ElementCount],
      decltype(make_span_type<ValueType[ElementCount]>()))
    ->space<ValueType[ElementCount],
            buffer<decltype(make_span_type<ValueType[ElementCount]>()), 1>>;

template <typename ValueType, typename SpanType>
space(const ValueType *, SpanType)->space<const ValueType *, SpanType>;

template <typename ValueType, typename SpanType>
space(ValueType *, SpanType)->space<ValueType *, SpanType>;

// n-furcate
// parallel/zip
// join
// absolute

// Space concepts:
// - memory address, file offset, memory mapped
// - terminal coordinates
// - graphics
// - input/output stream, socket
// - database query cursor
// - tree/trie, heap, list, graph, table

// Key concepts:
// - Arithmetic (++, --, +, -, [])
// - Radical (hash, identity)
// - Parent (>>), child (<<), sibling (++, --, +, -, [])

// Space connectivity:
// - Boolean
// - Scalar
// - Vector

// Space attributes:
// - Ordered
// - Partitioned
// - Disjointed
// - Mirrored
// - Linked

// Traversal concepts
// - single-pass (streaming), multi-pass
// - Forward, backward
// - Stride, chunk, linear, logarithmic
} // namespace gut
