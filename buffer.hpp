#pragma once

#include "integer.hpp"

namespace gut {

template <typename ValueType, count_t BufferSize> struct buffer;
template <typename... CandidateTypes>
inline constexpr constant is_buffer_v = constant_v<false>;
template <typename ValueType, count_t BufferSize>
inline constexpr constant is_buffer_v<buffer<ValueType, BufferSize>> =
    constant_v<true>;

template <typename ValueType, count_t BufferSize> struct buffer {
  using value_type = ValueType;
  using index_type = integer<max_bits(BufferSize) + 1>;

  constexpr static decltype(auto) count();
  static_assert(count());
  constexpr static decltype(auto) value_type_v();

  value_type m_buffer[count()];

  constexpr buffer() = default;
  constexpr buffer(const buffer &that) = default;
  constexpr buffer(buffer &&that) = default;
  constexpr buffer &operator=(const buffer &that) = default;
  constexpr buffer &operator=(buffer &&that) = default;

  constexpr decltype(auto) data() const;
  constexpr decltype(auto) data();
  constexpr decltype(auto) operator[](index_type index) const;
  constexpr decltype(auto) operator[](index_type index);
  template <index_t Index>
  constexpr decltype(auto) get(index<Index> = {}) const;
  template <index_t Index> constexpr decltype(auto) get(index<Index> = {});
  constexpr decltype(auto) begin() const;
  constexpr decltype(auto) begin();
  constexpr decltype(auto) end() const;
  constexpr decltype(auto) end();
  constexpr decltype(auto) cbegin() const;
  constexpr decltype(auto) cend() const;
  constexpr decltype(auto) front() const;
  constexpr decltype(auto) front();
  constexpr decltype(auto) back() const;
  constexpr decltype(auto) back();
};

template <typename ValueType, count_t BufferSize>
buffer(const ValueType (&array)[BufferSize])->buffer<ValueType, BufferSize>;

template <typename ValueTypeL, count_t BufferSizeL, typename ValueTypeR,
          count_t BufferSizeR>
constexpr bool operator==(const buffer<ValueTypeL, BufferSizeL> &,
                          const buffer<ValueTypeR, BufferSizeR> &);

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::count() {
  return count_v<BufferSize>;
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::value_type_v() {
  return gut::type_v<value_type>;
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::data() const {
  return (m_buffer);
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::data() {
  return (m_buffer);
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto)
    buffer<ValueType, BufferSize>::operator[](index_type index) const {
  return data()[index];
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto)
    buffer<ValueType, BufferSize>::operator[](index_type index) {
  return data()[index];
}

template <typename ValueType, count_t BufferSize>
template <index_t Index>
constexpr decltype(auto)
buffer<ValueType, BufferSize>::get(index<Index>) const {
  return data()[Index];
}

template <typename ValueType, count_t BufferSize>
template <index_t Index>
constexpr decltype(auto) buffer<ValueType, BufferSize>::get(index<Index>) {
  return data()[Index];
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::begin() const {
  return data() + 0;
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::begin() {
  return data() + 0;
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::end() const {
  return data() + count();
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::end() {
  return data() + count();
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::cbegin() const {
  return data() + 0;
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::cend() const {
  return data() + count();
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::front() const {
  return data()[0];
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::front() {
  return data()[0];
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::back() const {
  return data()[count() - 1];
}

template <typename ValueType, count_t BufferSize>
constexpr decltype(auto) buffer<ValueType, BufferSize>::back() {
  return data()[count() - 1];
}

template <typename ValueTypeL, count_t BufferSizeL, typename ValueTypeR,
          count_t BufferSizeR>
constexpr bool operator==(const buffer<ValueTypeL, BufferSizeL> &l,
                          const buffer<ValueTypeR, BufferSizeR> &r) {
  if constexpr (BufferSizeL != BufferSizeR)
    return false;
  else if constexpr (is_equalcomparable_v<ValueTypeL, ValueTypeR>) {
    auto l_first = l.cbegin(), l_last = l.end();
    auto r_first = r.cbegin(), r_last = r.end();
    for (; l_first != l_last && r_first != r_last; ++l_first, ++r_first)
      if (!(*l_first == *r_first))
        return false;
    return true;
  } else
    return false;
}
} // namespace gut

template <typename ValueType, gut::count_t BufferSize>
class std::tuple_size<gut::buffer<ValueType, BufferSize>>
    : public gut::constant<BufferSize> {};

template <std::size_t Index, typename ValueType, gut::count_t BufferSize>
class std::tuple_element<Index, gut::buffer<ValueType, BufferSize>> {
public:
  using type = ValueType;
};
