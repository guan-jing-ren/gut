#pragma once

#include "fstring.hpp"
#include "ntuple.hpp"
#include "preprocess.hpp"

namespace gut {
template <typename Enum, typename Members, typename Name, typename Names>
struct reflect_enum;
template <typename Name, typename FunctionOverloads> struct reflect_overloads;
template <typename Struct, typename Members, typename Bases, typename Name,
          typename Names>
struct reflect_struct;
template <typename Members, typename Name> struct reflect_namespace;

template <typename...>
inline constexpr constant is_reflect_enum_v = constant_v<false>;
template <typename Enum, typename Members, typename Name, typename Names>
inline constexpr constant
    is_reflect_enum_v<reflect_enum<Enum, Members, Name, Names>> =
        constant_v<true>;
template <typename...>
inline constexpr constant is_reflect_overloads_v = constant_v<false>;
template <typename FunctionOverloads, typename Name>
inline constexpr constant
    is_reflect_overloads_v<reflect_overloads<FunctionOverloads, Name>> =
        constant_v<true>;
template <typename...>
inline constexpr constant is_reflect_struct_v = constant_v<false>;
template <typename Struct, typename Members, typename Bases, typename Name,
          typename Names>
inline constexpr constant
    is_reflect_struct_v<reflect_struct<Struct, Members, Bases, Name, Names>> =
        constant_v<true>;
template <typename...>
inline constexpr constant is_reflect_namespace_v = constant_v<false>;
template <typename Members, typename Name>
inline constexpr constant
    is_reflect_namespace_v<reflect_namespace<Members, Name>> = constant_v<true>;

template <typename Enum, typename Members, typename Name, typename Names>
struct reflect_enum : private ntuple<Name, Names> {
  using reflect_enum_base = ntuple<Name, Names>;
  constexpr static decltype(auto) reflect_enum_base_v() {
    return gut::type_v<reflect_enum_base>;
  }
  static_assert(is_ntuple_v<Members>);
  static_assert(is_fstring_v<Name>);
  static_assert(is_ntuple_v<Names>);

  constexpr static decltype(auto) count() { return Members::count(); }

  constexpr reflect_enum(type<Enum>, Members, Name name, Names names)
      : reflect_enum_base(name, names) {}

  constexpr auto name() const { return this->get(index_v<0>); }
  constexpr static auto type_v() { return ::gut::type_v<Enum>; }

  constexpr static auto members() { return Members{}; }
  constexpr auto names() const { return this->get(index_v<1>); }

  template <index_t MemberIndex>
  constexpr static auto member(index<MemberIndex> = {}) {
    return members().get(index_v<MemberIndex>);
  }

  template <index_t MemberIndex>
  constexpr auto name(index<MemberIndex> = {}) const {
    return names().get(index_v<MemberIndex>);
  }

  template <index_t MemberIndex>
  constexpr auto member_and_name(index<MemberIndex> = {}) const {
    return ntuple{members().get(index_v<MemberIndex>),
                  names().get(index_v<MemberIndex>)};
  }

  constexpr auto members_and_names() const {
    return make_index_list<count()>().reduce([this](auto... i) {
      return ntuple<decltype(this->member_and_name(i))...>{
          this->member_and_name(i)...};
    });
  }
};

template <typename FunctionOverloads, typename Name>
struct reflect_overloads : private Name {
  using reflect_overloads_base = type<Name>;
  constexpr static decltype(auto) reflect_overloads_base_v() {
    return reflect_overloads_base{};
  }
  static_assert(is_ntuple_v<FunctionOverloads>);
  static_assert(is_fstring_v<Name>);

  constexpr reflect_overloads(FunctionOverloads, Name name)
      : reflect_overloads_base::raw_type(name) {}

  constexpr auto name() const { return static_cast<const Name &>(*this); }

  constexpr static auto members() { return FunctionOverloads{}; }

  template <index_t MemberIndex>
  constexpr static auto member(index<MemberIndex> = {}) {
    return members().get(index_v<MemberIndex>);
  }
};

template <typename Struct, typename Members, typename Bases, typename Name,
          typename Names>
struct reflect_struct : private ntuple<Name, Names> {
  using reflect_struct_base = ntuple<Name, Names>;
  constexpr static decltype(auto) reflect_struct_base_v() {
    return gut::type_v<reflect_struct_base>;
  }
  static_assert(is_ntuple_v<Members>);
  static_assert(is_ntuple_v<Bases>);
  static_assert(is_fstring_v<Name>);
  static_assert(is_ntuple_v<Names>);

  constexpr static decltype(auto) count() { return Members::count(); }

  constexpr reflect_struct(type<Struct>, Members, Bases, Name name, Names names)
      : reflect_struct_base(name, names) {}

  constexpr auto name() const { return this->get(index_v<0>); }
  constexpr static auto type_v() { return ::gut::type_v<Struct>; }

  constexpr static auto members() { return Members{}; }
  constexpr auto names() const { return this->get(index_v<1>); }
  constexpr static auto bases() { return Bases{}; }

  template <index_t MemberIndex>
  constexpr static auto member(index<MemberIndex> = {}) {
    return members().get(index_v<MemberIndex>);
  }

  template <index_t MemberIndex>
  constexpr auto name(index<MemberIndex> = {}) const {
    return names().get(index_v<MemberIndex>);
  }

  template <index_t MemberIndex>
  constexpr auto member_and_name(index<MemberIndex> = {}) const {
    return ntuple{members().get(index_v<MemberIndex>),
                  names().get(index_v<MemberIndex>)};
  }

  constexpr auto members_and_names() const {
    return make_index_list<count()>().reduce([this](auto... i) {
      return ntuple<decltype(this->member_and_name(i))...>{
          this->member_and_name(i)...};
    });
  }

  template <index_t MemberIndex>
  constexpr static auto base(index<MemberIndex> = {}) {
    return bases().get(index_v<MemberIndex>);
  }
};

GUT_MEMFN(reflecttype, type_v());
template <typename MemberType> struct reflect_type_filter {
  template <typename CandidateType>
  struct reflect_type_matcher_base
      : constant<(type_v<MemberType> == CandidateType::type_v()).value> {};

  template <typename CandidateType>
  struct reflect_type_matcher
      : std::conditional_t<!is_reflecttype_v<CandidateType>, constant<false>,
                           reflect_type_matcher_base<CandidateType>> {};

  template <typename ReflectType>
  constexpr static decltype(auto) match(ReflectType &&reflected) {
    return reflected.template filter<reflect_type_matcher>();
  }
};

template <typename Members, typename Name>
struct reflect_namespace : private ntuple<Members, Name> {
  using reflect_namespace_base = type<ntuple<Members, Name>>;
  constexpr static decltype(auto) reflect_namespace_base_v() {
    return reflect_namespace_base{};
  }
  static_assert(is_ntuple_v<Members>);
  static_assert(is_fstring_v<Name>);

  constexpr reflect_namespace(Members members, Name name)
      : reflect_namespace_base::raw_type(members, name) {}

  constexpr decltype(auto) members() const { return this->get(index_v<0>); }
  constexpr decltype(auto) name() const { return this->get(index_v<1>); }

  template <index_t MemberIndex>
  constexpr decltype(auto) member(index<MemberIndex> = {}) const {
    return members().get(index_v<MemberIndex>);
  }

  template <typename MemberType>
  constexpr decltype(auto) member(type<MemberType> = {}) const {
    return reflect_type_filter<MemberType>::match(members()).get(index_v<0>);
  }

  template <index_t MemberIndex>
  constexpr decltype(auto) name(index<MemberIndex> = {}) const {
    return name() + "::" + member(index_v<MemberIndex>).name();
  }

  template <typename MemberType>
  constexpr decltype(auto) name(type<MemberType> = {}) const {
    return name() + "::" + member(type_v<MemberType>).name();
  }
};
} // namespace gut

#define GUT_REFLECT_MEMBERS(...) (__VA_ARGS__)

#define GUT_REFLECT_ENUM(e, m)                                                 \
  ::gut::reflect_enum {                                                        \
    ::gut::type_v<e>,                                                          \
        ::gut::ntuple<GUT_PP_FOR(GUT_REFLECT_ENUM_MEMBER, e, m)>{},            \
        ::gut::fstring{GUT_PP_STRINGIZE(e)}, ::gut::ntuple {                   \
      GUT_PP_FOR(GUT_REFLECT_STRUCT_MEMBER_NAME, e, m)                         \
    }                                                                          \
  }

#define GUT_REFLECT_OVERLOADS(f, a)                                            \
  ::gut::reflect_overloads {                                                   \
    ::gut::ntuple<GUT_PP_FOR(GUT_REFLECT_OVERLOAD, f, a)>{}, ::gut::fstring {  \
      GUT_PP_STRINGIZE(f)                                                      \
    }                                                                          \
  }

#define GUT_REFLECT_BASES(...) (__VA_ARGS__)
#define GUT_REFLECT_STRUCT(s, m, b)                                            \
  ::gut::reflect_struct {                                                      \
    ::gut::type_v<s>,                                                          \
        ::gut::ntuple<GUT_PP_FOR(GUT_REFLECT_STRUCT_MEMBER, s, m)>{},          \
        ::gut::ntuple<GUT_PP_FOR(GUT_REFLECT_STRUCT_BASE, s, b)>{},            \
        ::gut::fstring{GUT_PP_STRINGIZE(s)}, ::gut::ntuple {                   \
      GUT_PP_FOR(GUT_REFLECT_STRUCT_MEMBER_NAME, s, m)                         \
    }                                                                          \
  }

#define GUT_REFLECT_NAMESPACE(n, m)                                            \
  ::gut::reflect_namespace {                                                   \
    ::gut::ntuple{GUT_PP_FOR(GUT_REFLECT_NAMESPACE_MEMBER, n, m)},             \
        ::gut::fstring {                                                       \
      GUT_PP_STRINGIZE(n)                                                      \
    }                                                                          \
  }

#define GUT_REFLECT_ENUM_MEMBER(...)                                           \
  ::gut::constant<GUT_PP_LOOP_CONTEXT(__VA_ARGS__)::GUT_PP_LOOP_VAR(           \
      __VA_ARGS__)>                                                            \
      GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
#define GUT_REFLECT_OVERLOAD(...)                                              \
  ::gut::constant<static_cast<GUT_PP_LOOP_VAR(__VA_ARGS__)>(                   \
      &GUT_PP_LOOP_CONTEXT(__VA_ARGS__))>                                      \
      GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
#define GUT_REFLECT_STRUCT_MEMBER(...)                                         \
  ::gut::constant<&GUT_PP_LOOP_CONTEXT(__VA_ARGS__)::GUT_PP_LOOP_VAR(          \
      __VA_ARGS__)>                                                            \
      GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
#define GUT_REFLECT_STRUCT_MEMBER_NAME(...)                                    \
  ::gut::fstring{GUT_PP_STRINGIZE(GUT_PP_LOOP_VAR(                             \
      __VA_ARGS__))} GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
#define GUT_REFLECT_STRUCT_BASE(...)                                           \
  ::gut::type<GUT_PP_LOOP_VAR(__VA_ARGS__)> GUT_PP_LOOP_CHECK_LAST(            \
      __VA_ARGS__)(GUT_PP_COMMA)
#define GUT_REFLECT_NAMESPACE_MEMBER(...)                                      \
  GUT_PP_LOOP_CONTEXT(__VA_ARGS__)::GUT_PP_LOOP_VAR(__VA_ARGS__)               \
      GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
