#pragma once

#include "fstring.hpp"
#include "type_list.hpp"

namespace gut {
inline constexpr fstring count_separator = ",";
inline constexpr fstring signed_prefix = "i";
inline constexpr fstring unsigned_prefix = "u";
inline constexpr fstring floating_prefix = "f";
inline constexpr fstring buffer_prefix = "[";
inline constexpr fstring transposed_prefix = "{";
inline constexpr fstring transposed_suffix = "}";
inline constexpr fstring adt_prefix = "(";
inline constexpr fstring adt_suffix = ")";

template <count_t ByteSize>
inline constexpr fstring bytesize_suffix = [
]() mutable constexpr->decltype(auto) {
  constexpr constant bitsize = constant_v<ByteSize * CHAR_BIT>;
  if constexpr (bitsize == 8)
    return "3";
  else if constexpr (bitsize == 16)
    return "4";
  else if constexpr (bitsize == 32)
    return "5";
  else if constexpr (bitsize == 64)
    return "6";
  else if constexpr (bitsize == 128)
    return "7";
  else if constexpr (bitsize == 256)
    return "8";
}
();

template <typename TypeHead, typename... TypeTail>
inline constexpr fstring type_signature = adt_prefix +
                                          (type_signature<TypeHead> + ... +
                                           type_signature<TypeTail>)+adt_suffix;

template <> inline constexpr fstring type_signature<void> = "0";
template <> inline constexpr fstring type_signature<void *> = "0";
template <> inline constexpr fstring type_signature<std::nullptr_t> = "0";
template <> inline constexpr fstring type_signature<bool> = "b";

template <typename ArithmeticType>
inline constexpr fstring arithmetic_signature = [
]() mutable constexpr->decltype(auto) {
  if constexpr (type_v<ArithmeticType>.is_floating_point())
    return floating_prefix;
  else if constexpr (type_v<ArithmeticType>.is_unsigned())
    return unsigned_prefix;
  else if constexpr (type_v<ArithmeticType>.is_signed())
    return signed_prefix;
}
() + bytesize_suffix<sizeof(ArithmeticType)>;

template <count_t Count>
inline constexpr fstring count_signature = []() {
  constexpr fstring hexdigits = "0123456789ABCDEF";
  fstring<char, max_bits(Count) / 4 + (max_bits(Count) % 4 == 0 ? 0 : 1)>
      count{};
  for (count_t i = 0; i < count.count(); ++i)
    count[i] =
        hexdigits[((Count >> ((count.count() - i - 1) * 4)) & count_v<0xF>)];
  return count;
}();

template <count_t Count, typename... Types>
inline constexpr fstring type_signature<type_list<Types...>[Count]> = []() {
  constexpr fstring adtsig = type_signature<Types...>;
  return transposed_prefix + count_signature<Count> + count_separator +
         adtsig.substr(index_v<1>, index_v<adtsig.count() - 1>) +
         transposed_suffix;
}();

template <typename Type>
inline constexpr fstring type_signature<Type> = []() {
  if constexpr (type_v<Type>.is_arithmetic())
    return arithmetic_signature<Type>;
  if constexpr (type_v<Type>.is_enum())
    return type_signature<decltype(type_v<Type>.underlying_type())>;
  else if constexpr (type_v<Type>.is_array())
    return buffer_prefix + count_signature<type_v<Type>.extent()> +
           count_separator +
           type_signature<decltype(type_v<Type>.remove_extent())>;
}();

template <typename Type>
inline constexpr fstring type_signature<type<Type>> = type_signature<Type>;

template <typename... Types>
inline constexpr fstring type_signature<type_list<Types...>> =
    type_signature<Types...>;
} // namespace gut
