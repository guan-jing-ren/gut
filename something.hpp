#pragma once

#include "constant.hpp"

namespace gut {
struct something;

template <typename Something, typename... Tail>
inline constexpr constant is_something_v =
    constant_v<std::is_same_v<something, Something> && sizeof...(Tail) == 0>;

struct something {
  constexpr something() = default;
  constexpr something(const something &) = default;
  constexpr something(something &&) = default;
  constexpr something &operator=(const something &) = default;
  constexpr something &operator=(something &&) = default;
  template <typename... Args> constexpr explicit something(Args &&...) {}
  template <typename T> constexpr something operator=(T &&) { return {}; }
  template <typename T> constexpr operator T() { return {}; }
  template <typename... Args> constexpr something operator()(Args &&...) const {
    return {};
  }
  template <typename... Args> constexpr something operator[](Args &&...) const {
    return {};
  }
};

#define GUT_COMMA ,

#define GUT_DEFINE_POSTFIX(op) something operator op(something, int)
#define GUT_DEFINE_PREFIX(op) something operator op(something)
#define GUT_DEFINE_BINOP(op) something operator op(something, something)

GUT_DEFINE_POSTFIX(++);
GUT_DEFINE_POSTFIX(--);
GUT_DEFINE_BINOP(->*);
GUT_DEFINE_PREFIX(++);
GUT_DEFINE_PREFIX(--);
GUT_DEFINE_PREFIX(+);
GUT_DEFINE_PREFIX(-);
GUT_DEFINE_PREFIX(!);
GUT_DEFINE_PREFIX(~);
GUT_DEFINE_PREFIX(*);
GUT_DEFINE_PREFIX(&);
GUT_DEFINE_BINOP(*);
GUT_DEFINE_BINOP(/);
GUT_DEFINE_BINOP(%);
GUT_DEFINE_BINOP(+);
GUT_DEFINE_BINOP(-);
GUT_DEFINE_BINOP(<<);
GUT_DEFINE_BINOP(>>);
GUT_DEFINE_BINOP(<);
GUT_DEFINE_BINOP(<=);
GUT_DEFINE_BINOP(>);
GUT_DEFINE_BINOP(>=);
GUT_DEFINE_BINOP(==);
GUT_DEFINE_BINOP(!=);
GUT_DEFINE_BINOP(&);
GUT_DEFINE_BINOP(^);
GUT_DEFINE_BINOP(|);
GUT_DEFINE_BINOP(&&);
GUT_DEFINE_BINOP(||);
GUT_DEFINE_BINOP(*=);
GUT_DEFINE_BINOP(/=);
GUT_DEFINE_BINOP(%=);
GUT_DEFINE_BINOP(+=);
GUT_DEFINE_BINOP(-=);
GUT_DEFINE_BINOP(<<=);
GUT_DEFINE_BINOP(>>=);
GUT_DEFINE_BINOP(&=);
GUT_DEFINE_BINOP(^=);
GUT_DEFINE_BINOP(|=);
GUT_DEFINE_BINOP(GUT_COMMA);

#undef GUT_COMMA
#undef GUT_DEFINE_POSTFIX
#undef GUT_DEFINE_PREFIX
#undef GUT_DEFINE_BINOP
} // namespace gut
