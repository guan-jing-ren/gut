#include "space.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;
using namespace std;

namespace {
constexpr key summed_key = key{bitfield<20, 16, 12, 8, 4, 0>{2, 2, 2, 2, 2}} +
                           key{bitfield<20, 16, 12, 8, 4, 0>{3, 3, 3, 3, 3}};
static_assert(std::is_trivially_default_constructible_v<
              remove_cvref_t<decltype(summed_key)>>);
static_assert(std::is_trivially_copy_constructible_v<
              remove_cvref_t<decltype(summed_key)>>);
static_assert(std::is_trivially_move_constructible_v<
              remove_cvref_t<decltype(summed_key)>>);
static_assert(
    std::is_trivially_copy_assignable_v<remove_cvref_t<decltype(summed_key)>>);
static_assert(
    std::is_trivially_move_assignable_v<remove_cvref_t<decltype(summed_key)>>);
static_assert(
    std::is_trivially_destructible_v<remove_cvref_t<decltype(summed_key)>>);
static_assert(summed_key == key{bitfield<20, 16, 12, 8, 4, 0>{5, 5, 5, 5, 5}});
static_assert(summed_key - key{bitfield<20, 16, 12, 8, 4, 0>{4, 4, 4, 4, 4}} ==
              key{bitfield<20, 16, 12, 8, 4, 0>{1, 1, 1, 1, 1}});
constexpr buffer<int, 200> int_storage_space{};
constexpr space int_space{int_storage_space.data()};
constexpr integer int_space_span = int_space.span();
static_assert(int_space_span == 200);
static_assert(int_space_span == integer{count_v<200>});
constexpr space int_space_clamped = int_space.rebase({5}).truncate({194});
static_assert(int_space_clamped.span() == integer{count_v<1>});
static_assert(int_space_clamped.contains({0}));
static_assert(!int_space_clamped.contains({1}));
constexpr int int_multi_storage_space[3][4][5]{};
constexpr space int_multi_space{int_multi_storage_space};
static_assert(!int_multi_space.contains({{3, 4, 5}}));
static_assert(!int_multi_space.contains({{-1, -1, -1}}));
static_assert(int_multi_space.contains({}));
static_assert(int_multi_space.contains({{2, 3, 4}}));
static_assert(!int_multi_space.contains({{2, 4, 4}}));
static_assert(int_multi_space.truncate({{1, 1, 1}}).span_count() == 1);
constexpr space int_multi_space_clamped =
    int_multi_space.rebase({{1, 1, 1}}).truncate({{1, 1, 1}});
static_assert(int_multi_space_clamped.span() == bitfield<11, 8, 4, 0>{1, 2, 3});
static_assert(!int_multi_space_clamped.contains({{1, 2, 3}}));
static_assert(int_multi_space_clamped.contains({}));
static_assert(int_multi_space_clamped.contains({{0, 1, 2}}));
static_assert(!int_multi_space_clamped.contains({{1, 1, 2}}));
constexpr bitfield int_multi_space_span = int_multi_space.span();
static_assert(int_multi_space_span == bitfield<11, 8, 4, 0>{3, 4, 5});
static_assert(int_multi_space_span[index_v<0>] == 3);
static_assert(int_multi_space_span[index_v<1>] == 4);
static_assert(int_multi_space_span[index_v<2>] == 5);
constexpr auto rotated_reversed = []() constexpr {
  key int_multi_key = int_multi_space_span;
  buffer rotated{{
      (++int_multi_key).rotate(),
      (++int_multi_key).rotate(),
      (++int_multi_key).rotate(),
      (++int_multi_key).rotate(),
      (++int_multi_key).rotate(),
      (++int_multi_key).rotate(),
  }};
  buffer reversed{{
      int_multi_key.rotate().reverse(),
      int_multi_key.rotate().reverse(),
      int_multi_key.rotate().reverse(),
      int_multi_key.rotate().reverse(),
      int_multi_key.rotate().reverse(),
      int_multi_key.rotate().reverse(),
  }};

  return buffer{{rotated, reversed}};
}
();
static_assert(rotated_reversed[0][0] == key{bitfield<11, 8, 4, 0>{4, 4, 5}});
static_assert(rotated_reversed[0][1] == key{bitfield<11, 8, 4, 0>{4, 5, 5}});
static_assert(rotated_reversed[0][2] == key{bitfield<11, 8, 4, 0>{4, 5, 6}});
static_assert(rotated_reversed[0][3] == key{bitfield<11, 8, 4, 0>{-3, 5, 6}});
static_assert(rotated_reversed[0][4] == key{bitfield<11, 8, 4, 0>{5, 6, 6}});
static_assert(rotated_reversed[0][5] == key{bitfield<11, 8, 4, 0>{5, 6, 7}});
static_assert(rotated_reversed[1][0] == key{bitfield<11, 8, 4, 0>{5, -6, 7}});
static_assert(rotated_reversed[1][1] == key{bitfield<11, 8, 4, 0>{5, -6, -7}});
static_assert(rotated_reversed[1][2] == key{bitfield<11, 8, 4, 0>{-5, -6, -7}});
static_assert(rotated_reversed[1][3] == key{bitfield<11, 8, 4, 0>{-5, 6, -7}});
static_assert(rotated_reversed[1][4] == key{bitfield<11, 8, 4, 0>{3, 6, 7}});
static_assert(rotated_reversed[1][5] == key{bitfield<11, 8, 4, 0>{5, 6, 7}});
static_assert([]() {
  constexpr bitfield<15, 12, 8, 4, 0> one_two_three_four{1, 2, 3, 4};
  int multi[3][4][5][6]{};
  int total = 0;
  for (auto i = 0; i < 3; ++i)
    for (auto j = 0; j < 4; ++j)
      for (auto k = 0; k < 5; ++k)
        for (auto l = 0; l < 6; ++l)
          multi[i][j][k][l] = total++;
  space multi_space{multi};
  optional<int &> result = multi_space[one_two_three_four];
  if (result.value() != 4 + (3 * 6) + (2 * 5 * 6) + (1 * 4 * 5 * 6))
    return result.value();

  space multi_mapped_space = multi_space.map(arg_v * 2);
  optional<int> mapped_result = multi_mapped_space[one_two_three_four];
  if (mapped_result.value() != result.value() * 2)
    return mapped_result.value();

  return 0;
}() == 0);
static_assert(
    !std::is_copy_constructible_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_copy_constructible_v<space<function<something, ntuple<arg>>, int>>);
static_assert(
    std::is_copy_constructible_v<space<function<something, ntuple<arg>>, int>>);
static_assert(
    !std::is_copy_assignable_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_copy_assignable_v<space<function<something, ntuple<arg>>, int>>);
static_assert(
    std::is_copy_assignable_v<space<function<something, ntuple<arg>>, int>>);
static_assert(
    std::is_move_constructible_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_move_constructible_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_move_constructible_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_move_assignable_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_move_assignable_v<space<function<something, ntuple<>>, int>>);
static_assert(
    std::is_move_assignable_v<space<function<something, ntuple<>>, int>>);
} // namespace
