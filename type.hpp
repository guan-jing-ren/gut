#pragma once

#include "concepts.hpp"
#include "index_list.hpp"
#include "something.hpp"

namespace gut {
template <typename Type> struct type;
template <typename...> inline constexpr constant is_type_v = constant_v<false>;
template <typename Type>
inline constexpr constant is_type_v<type<Type>> = constant_v<true>;

template <typename Type> struct type {
  using raw_type = Type;
  constexpr type() = default;
  constexpr type(const type &that) = default;
  constexpr type(type &&that) = default;
  constexpr type &operator=(const type &that) = default;
  constexpr type &operator=(type &&that) = default;

  template <typename ThatType> constexpr type(ThatType &&) {}

  constexpr static decltype(auto) size_of() { return sizeof(Type); }
  constexpr static decltype(auto) bitsize_of() { return size_of() * CHAR_BIT; }

  template <template <typename> typename CheckType>
  constexpr static decltype(auto) test() {
    return CheckType<raw_type>::value();
  }

  template <typename... ArgumentTypes>
  constexpr static decltype(auto) construct(ArgumentTypes &&... arguments) {
    return raw_type{std::forward<ArgumentTypes>(arguments)...};
  }

  template <typename ArgumentType>
  constexpr static decltype(auto) cast(ArgumentType &&argument) {
    if constexpr (is_pointer())
      return reinterpret_cast<raw_type>(argument);
    else
      return static_cast<raw_type>(std::forward<ArgumentType>(argument));
  }

  constexpr static decltype(auto) is_void() {
    return constant_v<std::is_void_v<raw_type>>;
  }
  constexpr static decltype(auto) is_null_pointer() {
    return constant_v<std::is_null_pointer_v<raw_type>>;
  }
  constexpr static decltype(auto) is_integral() {
    return constant_v<std::is_integral_v<raw_type>>;
  }
  constexpr static decltype(auto) is_floating_point() {
    return constant_v<std::is_floating_point_v<raw_type>>;
  }
  constexpr static decltype(auto) is_array() {
    return constant_v<std::is_array_v<raw_type>>;
  }
  constexpr static decltype(auto) is_enum() {
    return constant_v<std::is_enum_v<raw_type>>;
  }
  constexpr static decltype(auto) is_union() {
    return constant_v<std::is_union_v<raw_type>>;
  }
  constexpr static decltype(auto) is_class() {
    return constant_v<std::is_class_v<raw_type>>;
  }
  constexpr static decltype(auto) is_function() {
    return constant_v<std::is_function_v<raw_type>>;
  }
  constexpr static decltype(auto) is_pointer() {
    return constant_v<std::is_pointer_v<raw_type>>;
  }
  constexpr static decltype(auto) is_lvalue_reference() {
    return constant_v<std::is_lvalue_reference_v<raw_type>>;
  }
  constexpr static decltype(auto) is_rvalue_reference() {
    return constant_v<std::is_rvalue_reference_v<raw_type>>;
  }
  constexpr static decltype(auto) is_member_object_pointer() {
    return constant_v<std::is_member_object_pointer_v<raw_type>>;
  }
  constexpr static decltype(auto) is_member_function_pointer() {
    return constant_v<std::is_member_function_pointer_v<raw_type>>;
  }
  constexpr static decltype(auto) is_fundamental() {
    return constant_v<std::is_fundamental_v<raw_type>>;
  }
  constexpr static decltype(auto) is_arithmetic() {
    return constant_v<std::is_arithmetic_v<raw_type>>;
  }
  constexpr static decltype(auto) is_scalar() {
    return constant_v<std::is_scalar_v<raw_type>>;
  }
  constexpr static decltype(auto) is_object() {
    return constant_v<std::is_object_v<raw_type>>;
  }
  constexpr static decltype(auto) is_compound() {
    return constant_v<std::is_compound_v<raw_type>>;
  }
  constexpr static decltype(auto) is_reference() {
    return constant_v<std::is_reference_v<raw_type>>;
  }
  constexpr static decltype(auto) is_member_pointer() {
    return constant_v<std::is_member_pointer_v<raw_type>>;
  }
  constexpr static decltype(auto) is_const() {
    return constant_v<std::is_const_v<raw_type>>;
  }
  constexpr static decltype(auto) is_volatile() {
    return constant_v<std::is_volatile_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivial() {
    return constant_v<std::is_trivial_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_copyable() {
    return constant_v<std::is_trivially_copyable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_standard_layout() {
    return constant_v<std::is_standard_layout_v<raw_type>>;
  }
  [[deprecated]] constexpr static decltype(auto) is_pod() {
    return constant_v<std::is_pod_v<raw_type>>;
  }
  [[deprecated]] constexpr static decltype(auto) is_literal_type() {
    return constant_v<std::is_literal_type_v<raw_type>>;
  }
  constexpr static decltype(auto) has_unique_object_representations() {
    return constant_v<std::has_unique_object_representations_v<raw_type>>;
  }
  constexpr static decltype(auto) is_empty() {
    return constant_v<std::is_empty_v<raw_type>>;
  }
  constexpr static decltype(auto) is_polymorphic() {
    return constant_v<std::is_polymorphic_v<raw_type>>;
  }
  constexpr static decltype(auto) is_abstract() {
    return constant_v<std::is_abstract_v<raw_type>>;
  }
  constexpr static decltype(auto) is_final() {
    return constant_v<std::is_final_v<raw_type>>;
  }
  constexpr static decltype(auto) is_aggregate() {
    return constant_v<std::is_aggregate_v<raw_type>>;
  }
  constexpr static decltype(auto) is_signed() {
    return constant_v<std::is_signed_v<raw_type>>;
  }
  constexpr static decltype(auto) is_unsigned() {
    return constant_v<std::is_unsigned_v<raw_type>>;
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) is_constructible_with() {
    return constant_v<std::is_constructible_v<raw_type, ArgumentTypes...>>;
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto)
  is_constructible_with(type<ArgumentTypes>...) {
    return is_constructible_with<ArgumentTypes...>();
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) is_trivially_constructible_with() {
    return constant_v<
        std::is_trivially_constructible_v<raw_type, ArgumentTypes...>>;
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) is_nothrow_constructible_with() {
    return constant_v<
        std::is_nothrow_constructible_v<raw_type, ArgumentTypes...>>;
  }
  constexpr static decltype(auto) is_default_constructible() {
    return constant_v<std::is_default_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_default_constructible() {
    return constant_v<std::is_trivially_default_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_default_constructible() {
    return constant_v<std::is_nothrow_default_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_copy_constructible() {
    return constant_v<std::is_copy_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_copy_constructible() {
    return constant_v<std::is_trivially_copy_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_copy_constructible() {
    return constant_v<std::is_nothrow_copy_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_move_constructible() {
    return constant_v<std::is_move_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_move_constructible() {
    return constant_v<std::is_trivially_move_constructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_move_constructible() {
    return constant_v<std::is_nothrow_move_constructible_v<raw_type>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_assignable_from() {
    return constant_v<std::is_assignable_v<raw_type, ThatType>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_trivially_assignable_from() {
    return constant_v<std::is_trivially_assignable_v<raw_type, ThatType>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_nothrow_assignable_from() {
    return constant_v<std::is_nothrow_assignable_v<raw_type, ThatType>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_assignable_to() {
    return constant_v<std::is_assignable_v<ThatType, raw_type>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_trivially_assignable_to() {
    return constant_v<std::is_trivially_assignable_v<ThatType, raw_type>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_nothrow_assignable_to() {
    return constant_v<std::is_nothrow_assignable_v<ThatType, raw_type>>;
  }
  constexpr static decltype(auto) is_copy_assignable() {
    return constant_v<std::is_copy_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_copy_assignable() {
    return constant_v<std::is_trivially_copy_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_copy_assignable() {
    return constant_v<std::is_nothrow_copy_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_move_assignable() {
    return constant_v<std::is_move_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_move_assignable() {
    return constant_v<std::is_trivially_move_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_move_assignable() {
    return constant_v<std::is_nothrow_move_assignable_v<raw_type>>;
  }
  constexpr static decltype(auto) is_destructible() {
    return constant_v<std::is_destructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_trivially_destructible() {
    return constant_v<std::is_trivially_destructible_v<raw_type>>;
  }
  constexpr static decltype(auto) is_nothrow_destructible() {
    return constant_v<std::is_nothrow_destructible_v<raw_type>>;
  }
  constexpr static decltype(auto) has_virtual_destructor() {
    return constant_v<std::has_virtual_destructor_v<raw_type>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_swappable_with() {
    return constant_v<std::is_swappable_with_v<raw_type, ThatType>>;
  }
  constexpr static decltype(auto) is_swappable() {
    return constant_v<std::is_swappable_v<raw_type>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_nothrow_swappable_with() {
    return constant_v<std::is_nothrow_swappable_with_v<raw_type, ThatType>>;
  }
  constexpr static decltype(auto) is_nothrow_swappable() {
    return constant_v<std::is_nothrow_swappable_v<raw_type>>;
  }
  constexpr static decltype(auto) alignment_of() {
    return constant_v<std::alignment_of_v<raw_type>>;
  }
  constexpr static decltype(auto) rank() {
    return constant_v<std::rank_v<raw_type>>;
  }
  template <unsigned N = 0> constexpr static decltype(auto) extent() {
    return constant_v<std::extent_v<raw_type, N>>;
  }
  template <typename ThatType> constexpr static decltype(auto) is_same() {
    return constant_v<std::is_same_v<raw_type, ThatType>>;
  }
  template <typename ThatType>
  constexpr static decltype(auto) is_same(type<ThatType>) {
    return is_same<ThatType>();
  }
  template <typename DerivedType> constexpr static decltype(auto) is_base_of() {
    return constant_v<std::is_base_of_v<raw_type, DerivedType>>;
  }
  template <typename BaseType>
  constexpr static decltype(auto) is_derived_from() {
    return constant_v<std::is_base_of_v<BaseType, raw_type>>;
  }
  template <typename ToType>
  constexpr static decltype(auto) is_convertible_to() {
    return constant_v<std::is_convertible_v<raw_type, ToType>>;
  }
  template <typename FromType>
  constexpr static decltype(auto) is_convertible_from() {
    return constant_v<std::is_convertible_v<FromType, raw_type>>;
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) is_invocable() {
    return constant_v<std::is_invocable_v<raw_type, ArgumentTypes...>>;
  }
  template <typename ReturnType, typename... ArgumentTypes>
  constexpr static decltype(auto) is_invocable_r() {
    return constant_v<
        std::is_invocable_r_v<ReturnType, raw_type, ArgumentTypes...>>;
  }
  template <typename FunctionType, typename... ArgumentTypes>
  constexpr static decltype(auto) is_returned() {
    return constant_v<
        std::is_invocable_r_v<raw_type, FunctionType, ArgumentTypes...>>;
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) is_nothrow_invocable() {
    return constant_v<std::is_nothrow_invocable_v<raw_type, ArgumentTypes...>>;
  }
  template <typename FunctionType, typename... ArgumentTypes>
  constexpr static decltype(auto) is_nothrow_returned() {
    return constant_v<std::is_nothrow_invocable_r_v<raw_type, FunctionType,
                                                    ArgumentTypes...>>;
  }
  constexpr static decltype(auto) remove_cv() {
    return type<std::remove_cv_t<raw_type>>{};
  }
  constexpr static decltype(auto) remove_const() {
    return type<std::remove_const_t<raw_type>>{};
  }
  constexpr static decltype(auto) remove_volatile() {
    return type<std::remove_volatile_t<raw_type>>{};
  }
  constexpr static decltype(auto) add_cv() {
    return type<std::add_cv_t<raw_type>>{};
  }
  constexpr static decltype(auto) add_const() {
    return type<std::add_const_t<raw_type>>{};
  }
  constexpr static decltype(auto) add_volatile() {
    return type<std::add_volatile_t<raw_type>>{};
  }
  template <typename OnType>
  constexpr static decltype(auto) mirror_cv_on(type<OnType> on = {}) {
    if constexpr (is_const() && is_volatile())
      return on.remove_cvref().add_volatile().add_const().mirror_reference_of(
          on);
    else if constexpr (is_const())
      return on.remove_cvref().add_const().mirror_reference_of(on);
    else if constexpr (is_volatile())
      return on.remove_cvref().add_volatile().mirror_reference_of(on);
    else
      return on.remove_cvref().mirror_reference_of(on);
  }
  template <typename OfType>
  constexpr static decltype(auto) mirror_cv_of(type<OfType> of = {}) {
    return of.mirror_cv_on(type{});
  }
  constexpr static decltype(auto) remove_reference() {
    return type<std::remove_reference_t<raw_type>>{};
  }
  constexpr static decltype(auto) add_lvalue_reference() {
    return type<std::add_lvalue_reference_t<raw_type>>{};
  }
  constexpr static decltype(auto) add_rvalue_reference() {
    return type<std::add_rvalue_reference_t<raw_type>>{};
  }
  template <typename OnType>
  constexpr static decltype(auto) mirror_reference_on(type<OnType> on = {}) {
    if constexpr (is_lvalue_reference())
      return on.remove_reference().add_lvalue_reference();
    else if constexpr (is_rvalue_reference())
      return on.remove_reference().add_rvalue_reference();
    else
      return on.remove_reference();
  }
  template <typename OfType>
  constexpr static decltype(auto) mirror_reference_of(type<OfType> of = {}) {
    return of.mirror_reference_on(type{});
  }
  constexpr static decltype(auto) remove_pointer() {
    return type<std::remove_pointer_t<raw_type>>{};
  }
  template <typename ClassType, typename MemberType>
  constexpr static MemberType get_member_type(MemberType ClassType::*);
  constexpr static decltype(auto) remove_member_pointer() {
    return type<decltype(get_member_type(std::declval<raw_type>()))>{};
  }
  constexpr static decltype(auto) add_pointer() {
    return type<std::add_pointer_t<raw_type>>{};
  }
  constexpr static decltype(auto) make_signed() {
    return type<std::make_signed_t<raw_type>>{};
  }
  constexpr static decltype(auto) make_unsigned() {
    return type<std::make_unsigned_t<raw_type>>{};
  }
  constexpr static decltype(auto) remove_extent() {
    return type<std::remove_extent_t<raw_type>>{};
  }
  constexpr static decltype(auto) remove_all_extents() {
    return type<std::remove_all_extents_t<raw_type>>{};
  }
  constexpr static decltype(auto) get_extents() {
    if constexpr (!std::is_array_v<raw_type>)
      return indices<-1>{};
    else
      return make_index_list<rank()>().reduce(
          [](auto... i) constexpr mutable->decltype(auto) {
            return indices<extent<i>()...>{};
          });
  }
  template <std::size_t Len = 1>
  constexpr static decltype(auto) aligned_storage() {
    return type<std::aligned_storage_t<Len, alignof(raw_type)>>{};
  }
  constexpr static decltype(auto) aligned_union() {
    // For type_list
    // return type<std::aligned_union_t<raw_type>>{};
  }
  constexpr static decltype(auto) decay() {
    return type<std::decay_t<raw_type>>{};
  }
  constexpr static decltype(auto) remove_cvref() {
    return remove_reference().remove_cv();
  }
  template <typename OnType>
  constexpr static decltype(auto) mirror_cvref_on(type<OnType> on = {}) {
    return on.mirror_cv_of(type{}).mirror_reference_of(type{});
  }
  template <typename OfType>
  constexpr static decltype(auto) mirror_cvref_of(type<OfType> of = {}) {
    return of.mirror_cv_on(type{});
  }
  template <bool Enable> constexpr static decltype(auto) enable_if() {
    // Also for constant<bool>
    return type<std::enable_if_t<Enable, raw_type>>{};
  }
  constexpr static decltype(auto) conditional() {
    // For constant<bool>
    // return type<std::conditional_t<raw_type>>{};
  }
  template <typename... ThatTypes>
  constexpr static decltype(auto) common_type_with() {
    // Also for type_list
    return type<std::common_type_t<raw_type, ThatTypes...>>{};
  }
  constexpr static decltype(auto) underlying_type() {
    return type<std::underlying_type_t<raw_type>>{};
  }
  [[deprecated]] constexpr static decltype(auto) result_of() {
    return type<std::result_of_t<raw_type>>{};
  }
  template <typename... ArgumentTypes>
  constexpr static decltype(auto) invoke_result() {
    // Also for type_list
    return type<std::invoke_result_t<raw_type, ArgumentTypes...>>{};
  }
  constexpr static decltype(auto) conjunction() {
    // For type_list
    // return type<std::conjunction_t<raw_type>>{};
  }
  constexpr static decltype(auto) disjunction() {
    // For type_list
    // return type<std::disjunction_t<raw_type>>{};
  }
  constexpr static decltype(auto) negation() {
    //    return type<std::negation_t<raw_type>>{};
  }
};
template <typename Type> type(Type &&)->type<Type>;

template <typename TypeL, typename TypeR>
constexpr decltype(auto) operator==(type<TypeL>, type<TypeR>);

template <typename Type> struct to_type { using type = gut::type<Type>; };

template <typename Type> struct to_type<type<Type>> {
  using type = gut::type<Type>;
};

template <typename Type> using to_type_t = typename to_type<Type>::type;

template <typename Type> inline constexpr type type_v = type<Type>{};

template <typename Type>
inline constexpr type to_type_v = typename to_type<Type>::type{};

template <typename TypeL, typename TypeR>
constexpr decltype(auto) operator!=(type<TypeL>, type<TypeR>);

template <typename TypeL, typename TypeR>
constexpr decltype(auto) operator==(type<TypeL> l, type<TypeR> r) {
  return l.is_same(r);
}

template <typename ForwardReferenceType>
inline constexpr type remove_cvref_v =
    type_v<ForwardReferenceType>.remove_reference().remove_cv();

template <typename ForwardReferenceType>
using remove_cvref_t =
    typename decltype(remove_cvref_v<ForwardReferenceType>)::raw_type;

template <typename TypeL, typename TypeR>
constexpr decltype(auto) operator!=(type<TypeL>, type<TypeR>) {
  return constant_v<!(type_v<TypeL> == type_v<TypeR>)>;
}

template <typename T> void ctti();

template <typename Functor, typename... Args>
constexpr decltype(auto) call(Functor &&f, Args &&... args) {
  if constexpr (!is_callable_v<Functor, Args...>)
    return something{};
  else {
    using return_type = decltype(f(std::forward<Args>(args)...));
    if constexpr (type_v<return_type>.is_void()) {
      f(std::forward<Args>(args)...);
      return something{};
    } else
      return f(std::forward<Args>(args)...);
  }
}

template <typename Functor, typename... Args>
using return_type_t =
    decltype(call(std::declval<Functor>(), std::declval<Args>()...));

template <typename ValueType> constexpr count_t max_bits(ValueType v);

template <typename ValueType> constexpr count_t max_bits(ValueType v) {
  if (v < 0)
    v = -v;
  auto vv = [v]() mutable constexpr->decltype(auto) {
    if constexpr (is_index_v<ValueType> || is_count_v<ValueType> ||
                  is_constant_v<ValueType>)
      return v.value();
    else
      return v;
  }
  ();
  if (vv == 0)
    return 1;
  count_t bits = 0;
  for (; vv; vv >>= 1, ++bits)
    ;
  return bits;
}
} // namespace gut
