#include "fstring.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
constexpr fstring hello{"hello"};
static_assert(hello == "hello");
static_assert(hello != "world");
static_assert("hello" == hello);
static_assert("world" != hello);
static_assert(hello == fstring{"hello"});
static_assert(hello != fstring{"world"});
static_assert(hello < "world");
static_assert(hello < "hellp");
static_assert("hello" < fstring{"world"});
static_assert("hello" < fstring{"hellp"});
static_assert(fstring{"hello"} + "world" == "helloworld");
static_assert("hello" + fstring{"world"} == "helloworld");
static_assert(fstring{"hello"} + fstring{"world"} == "helloworld");
static_assert(fstring{"hello"} + fstring{"world"} ==
              fstring<char, 10>{"helloworld"});
static_assert(fstring{"hello"} + fstring{"world"} !=
              fstring<char, 10>{"hello"});
static_assert(fstring<char, 10>{"hello"} == "hello\0\0\0\0\0");
static_assert(fstring<char, 10>{"hello"} == fstring{"hello\0\0\0\0\0"});
static_assert(!(fstring<char, 10>{"hello"} < fstring{"hello\0\0\0\0\0"}));
static_assert(fstring{"helloworld"}
                  .substr()
                  .substr({}, index_v<9>)
                  .substr(index_v<1>)
                  .substr(index_v<1>, index_v<7>) == "llowor");
} // namespace
