#pragma once

#include "functor.hpp"

namespace gut {
struct any {
  constexpr decltype(auto) base() const &;
  constexpr decltype(auto) base() &;

  template <typename Derived, auto Op, typename... Args>
  constexpr decltype(auto) operator()(constant<Op>, Args... args) const;
  template <typename Derived, auto Op, typename... Args>
  constexpr decltype(auto) operator()(constant<Op>, Args... args);

  template <typename Derived, typename Op, typename... Args>
  constexpr decltype(auto) operator()(type<Op>, Args... args) const;
  template <typename Derived, typename Op, typename... Args>
  constexpr decltype(auto) operator()(type<Op>, Args... args);
};

template <typename AnyType> struct concrete : any, thing<AnyType> {
  using value_type = AnyType;
  constexpr static decltype(auto) value_type_v();

  template <typename ValueType> constexpr concrete(ValueType &&value);

  template <auto Op, typename Ret, typename... Args>
  constexpr decltype(auto) func() const;
  template <auto Op, typename Ret, typename... Args>
  constexpr decltype(auto) func();
};
template <typename ValueType> concrete(ValueType &&)->concrete<ValueType>;

////////////////////
// IMPLEMENTATION //
////////////////////

template <typename Derived, auto Op, typename... Args>
constexpr decltype(auto) any::operator()(constant<Op>, Args... args) const {
  return static_cast<const Derived &>(*this).get()(constant_v<Op>,
                                                   std::forward<Args>(args)...);
}

template <typename Derived, auto Op, typename... Args>
constexpr decltype(auto) any::operator()(constant<Op>, Args... args) {
  return static_cast<Derived &>(*this).get()(constant_v<Op>,
                                             std::forward<Args>(args)...);
}

template <typename Derived, typename Op, typename... Args>
constexpr decltype(auto) any::operator()(type<Op>, Args... args) const {
  return static_cast<const Derived &>(*this).get()(constant_v<Op>,
                                                   std::forward<Args>(args)...);
}

template <typename Derived, typename Op, typename... Args>
constexpr decltype(auto) any::operator()(type<Op>, Args... args) {
  return static_cast<Derived &>(*this).get()(constant_v<Op>,
                                             std::forward<Args>(args)...);
}

constexpr decltype(auto) any::base() const & { return ref{*this}; }
constexpr decltype(auto) any::base() & { return ref{*this}; }

template <typename AnyType>
template <typename ValueType>
constexpr concrete<AnyType>::concrete(ValueType &&value)
    : thing<AnyType>(std::forward<AnyType>(value)) {}

template <typename AnyType>
template <auto Op, typename Ret, typename... Args>
constexpr decltype(auto) concrete<AnyType>::func() const {
  using op_t = constant<static_cast<decltype(Op)>(Op)>;
  using any_const = Ret (any::*)(op_t, Args...) const;
  return gut::function{
      static_cast<any_const>(&any::operator()<concrete, Op, Args...>)};
}

template <typename AnyType>
template <auto Op, typename Ret, typename... Args>
constexpr decltype(auto) concrete<AnyType>::func() {
  using op_t = constant<static_cast<decltype(Op)>(Op)>;
  using any_nonconst = Ret (any::*)(op_t, Args...);
  return gut::function{
      static_cast<any_nonconst>(&any::operator()<concrete, Op, Args...>)};
}
} // namespace gut
