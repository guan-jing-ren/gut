#include "integer.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(sizeof(integer<8>) == sizeof(std::int8_t));
static_assert(sizeof(integer<16>) == sizeof(std::int16_t));
static_assert(sizeof(integer<32>) == sizeof(std::int32_t));
static_assert(sizeof(integer<64>) == sizeof(std::int64_t));
static_assert(integer<8>::max() == std::numeric_limits<std::int8_t>::max());
static_assert(integer<16>::max() == std::numeric_limits<std::int16_t>::max());
static_assert(integer<32>::max() == std::numeric_limits<std::int32_t>::max());
static_assert(integer<64>::max() == std::numeric_limits<std::int64_t>::max());
static_assert(integer<8>::min() == std::numeric_limits<std::int8_t>::min());
static_assert(integer<16>::min() == std::numeric_limits<std::int16_t>::min());
static_assert(integer<32>::min() == std::numeric_limits<std::int32_t>::min());
static_assert(integer<64>::min() == std::numeric_limits<std::int64_t>::min());
static_assert(integer<5>::max() == 15);
static_assert(integer<5>::min() == -16);
static_assert(integer<5>::min() < 0);
static_assert(integer<5>::max() < std::numeric_limits<std::int64_t>::max());
static_assert(integer<5>::max() < integer<64>::max());
static_assert(integer<5>::min() > integer<64>::min());
static_assert(integer<5>::max() > integer<64>{});
static_assert(integer<1>{} == integer<64>{});
static_assert(++integer<64>{integer<63>::max()} > 0);
static_assert(++integer<63>::max().promote<1>() > 0);
static_assert(-1 == static_cast<std::int64_t>(--integer<64>{}));
static_assert(decltype(integer{constant_v<8>})::count() == 5);
static_assert(decltype(integer{index_v<7>})::count() == 4);
} // namespace
