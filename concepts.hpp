#pragma once

#include "constant.hpp"

namespace gut {
struct unsupported;

template <typename Unsupported>
inline constexpr constant is_unsupported_v =
    constant_v<std::is_same_v<unsupported, Unsupported>>;

struct unsupported {
  unsupported() = delete;
  unsupported(const unsupported &) = delete;
  unsupported(unsupported &&) = delete;
  unsupported &operator=(const unsupported &) = delete;
  unsupported &operator=(unsupported &&) = delete;
  ~unsupported() = delete;
};

template <template <typename...> typename Concept, typename... FuncArgs>
struct detect {
  constexpr static auto concept(...) -> unsupported;
  template <typename CheckType>
  constexpr static auto concept(CheckType &&)
      -> Concept<CheckType, FuncArgs...>;
};
template <template <typename...> typename Concept, typename CheckType,
          typename... FuncArgs>
[[maybe_unused]] inline constexpr constant is_detected_v =
    constant_v<!is_unsupported_v<decltype(
        detect<Concept, FuncArgs...>::concept(std::declval<CheckType>()))>>;

#define GUT_MEMFN(concept, macro)                                              \
  template <typename concept, typename... concept##_args>                      \
  using concept##_result_t = decltype(std::declval<concept>().macro);          \
  template <typename concept, typename... concept##_args>                      \
  [[maybe_unused]] inline constexpr ::gut::constant is_##concept##_v =         \
      ::gut::is_detected_v<concept##_result_t, concept, concept##_args...>

#define GUT_TYPEDEF(concept, macro)                                            \
  template <typename concept, typename... concept##_args>                      \
  using concept##_result_t = typename concept ::macro;                         \
  template <typename concept, typename... concept##_args>                      \
  [[maybe_unused]] inline constexpr ::gut::constant is_##concept##_v =         \
      ::gut::is_detected_v<concept##_result_t, concept, concept##_args...>

#define GUT_BINOP(concept, macro)                                              \
  template <typename concept, typename other_##concept>                        \
  using concept##_result_t = decltype(macro);                                  \
  template <typename concept, typename other_##concept>                        \
  [[maybe_unused]] inline constexpr ::gut::constant is_##concept##_v =         \
      ::gut::is_detected_v<concept##_result_t, concept, other_##concept>

#define GUT_FREEFN(concept, macro)                                             \
  template <typename concept, typename... concept##_args>                      \
  using concept##_result_t = decltype(macro);                                  \
  template <typename concept, typename... concept##_args>                      \
  [[maybe_unused]] inline constexpr ::gut::constant is_##concept##_v =         \
      ::gut::is_detected_v<concept##_result_t, concept, concept##_args...>

GUT_FREEFN(postincrementable, std::declval<postincrementable>()++);
GUT_FREEFN(postdecrementable, std::declval<postdecrementable>()--);
GUT_FREEFN(callable,
           std::declval<callable>()(std::declval<callable_args>()...));
GUT_BINOP(subscriptable,
          std::declval<subscriptable>()[std::declval<other_subscriptable>()]);
GUT_MEMFN(memberpointerable, operator->());
GUT_BINOP(pointermemberpointerable,
          std::declval<pointermemberpointerable>()
                  ->*std::declval<other_pointermemberpointerable>());

GUT_FREEFN(preincrementable, ++std::declval<preincrementable>());
GUT_FREEFN(predecrementable, --std::declval<predecrementable>());
GUT_FREEFN(unaryplusable, +std::declval<unaryplusable>());
GUT_FREEFN(unaryminusable, -std::declval<unaryminusable>());
GUT_FREEFN(negatable, !std::declval<negatable>());
GUT_FREEFN(bitnegatable, ~std::declval<bitnegatable>());
GUT_FREEFN(dereferenceable, *std::declval<dereferenceable>());
GUT_FREEFN(addressable, &std::declval<addressable>());
GUT_FREEFN(newable, new newable(std::declval<newable_args>()...));
GUT_FREEFN(arraynewable, new arraynewable[std::declval<std::size_t>()]);
GUT_FREEFN(deletable, delete std::declval<deletable>());
GUT_FREEFN(arraydeletable, delete[] std::declval<arraydeletable>());

GUT_BINOP(multiplicable,
          std::declval<multiplicable>() * std::declval<other_multiplicable>());
GUT_BINOP(divisible,
          std::declval<divisible>() / std::declval<other_divisible>());
GUT_BINOP(modulable,
          std::declval<modulable>() % std::declval<other_modulable>());

GUT_BINOP(addable, std::declval<addable>() + std::declval<other_addable>());
GUT_BINOP(subtractable,
          std::declval<subtractable>() - std::declval<other_subtractable>());

GUT_BINOP(leftshiftable, std::declval<leftshiftable>()
                             << std::declval<other_leftshiftable>());
GUT_BINOP(rightshiftable, std::declval<rightshiftable>() >>
                              std::declval<other_rightshiftable>());

GUT_BINOP(lesscomparable, std::declval<lesscomparable>() <
                              std::declval<other_lesscomparable>());
GUT_BINOP(lessequalcomparable, std::declval<lessequalcomparable>() <=
                                   std::declval<other_lessequalcomparable>());
GUT_BINOP(greatercomparable, std::declval<greatercomparable>() >
                                 std::declval<other_greatercomparable>());
GUT_BINOP(greaterequalcomparable,
          std::declval<greaterequalcomparable>() >=
              std::declval<other_greaterequalcomparable>());

GUT_BINOP(equalcomparable, std::declval<equalcomparable>() ==
                               std::declval<other_equalcomparable>());
GUT_BINOP(notequalcomparable, std::declval<notequalcomparable>() !=
                                  std::declval<other_notequalcomparable>());

GUT_BINOP(bitandable,
          std::declval<bitandable>() & std::declval<other_bitandable>());
GUT_BINOP(bitxorable,
          std::declval<bitxorable>() ^ std::declval<other_bitxorable>());
GUT_BINOP(bitorable,
          std::declval<bitorable>() | std::declval<other_bitorable>());

GUT_BINOP(andable, std::declval<andable>() && std::declval<other_andable>());
GUT_BINOP(orable, std::declval<orable>() || std::declval<other_orable>());

GUT_BINOP(multiplyassignable, std::declval<multiplyassignable>() *=
                              std::declval<other_multiplyassignable>());
GUT_BINOP(divideassignable, std::declval<divideassignable>() /=
                            std::declval<other_divideassignable>());
GUT_BINOP(moduloassignable, std::declval<moduloassignable>() %=
                            std::declval<other_moduloassignable>());

GUT_BINOP(directassignable, std::declval<directassignable>() =
                                std::declval<other_directassignable>());
GUT_BINOP(addassignable,
          std::declval<addassignable>() += std::declval<other_addassignable>());
GUT_BINOP(subtractassignable, std::declval<subtractassignable>() -=
                              std::declval<other_subtractassignable>());

GUT_BINOP(leftshiftassignable, std::declval<leftshiftassignable>() <<=
                               std::declval<other_leftshiftassignable>());
GUT_BINOP(rightshiftassignable, std::declval<rightshiftassignable>() >>=
                                std::declval<other_rightshiftassignable>());

GUT_BINOP(bitandassignable, std::declval<bitandassignable>() &=
                            std::declval<other_bitandassignable>());
GUT_BINOP(bitxorassignable, std::declval<bitxorassignable>() ^=
                            std::declval<other_bitxorassignable>());
GUT_BINOP(bitorassignable, std::declval<bitorassignable>() |=
                           std::declval<other_bitorassignable>());

GUT_BINOP(commaable,
          (std::declval<commaable>(), std::declval<other_commaable>()));
} // namespace gut
