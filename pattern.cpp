#include "pattern.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace gut {
struct pattern_test {
  static_assert(sizeof(pattern_base) == 12);

  static_assert([]() {
    parser parser;
    return parser.match((parser.string("Hell") | parser.string("H")) &
                            parser.string("ello!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.string("a") | parser.string("hello!") |
                             parser.string("Hello\0"),
                         "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("a") | parser.string("hello!") |
                            parser.string("Hello\0") | parser.string("Hello!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.string("Hello\0") | parser.string("hello!"),
                         "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match((parser.string("h") | parser.string("H")) &
                             parser.string("ello\0"),
                         "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match((parser.string("Hell") | parser.string("H")) &
                             (parser.string("o\0") | parser.string("ello\0")),
                         "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match((parser.string("Hell") | parser.string("H")) &
                            (parser.string("o\0") | parser.string("ello!")),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(((parser.string("Hell") | parser.string("H")) &
                         (parser.string("o\0") | parser.string("ello\0"))) |
                            parser.string("Hello!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match((parser.string("Hell") | parser.string("H")) &
                             parser.string("ello\0"),
                         "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.span_range('a', 'z') & parser.found(), "Hello");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("H") &
            (parser.range('0', '9') |
             (parser.span_range('a', 'z') & parser.string("!")) |
             parser.span_range('a', 'z')) &
            parser.string("!"),
        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.span_set("ole") & parser.found(), "Hello");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") &
                            (parser.range('0', '9') |
                             (parser.span_set("ole") & parser.string("!")) |
                             parser.span_set("ole")) &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("H") &
            (parser.range('0', '9') |
             (parser.not_span_range('!', 'Z') & parser.string("!")) |
             parser.not_span_range('!', 'Z')) &
            parser.string("!"),
        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("H") &
            (parser.range('0', '9') |
             (parser.not_span_set("!abc") & parser.string("!")) |
             parser.not_span_set("!abc")) &
            parser.string("!"),
        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    auto alpha = parser.range('A', 'Z') | parser.range('0', '9') |
                 parser.range('a', 'z');
    auto five_choice = (alpha | alpha);
    auto five_alpha = five_choice & five_choice & five_choice & five_choice &
                      five_choice & five_choice;
    return !parser.match(five_alpha, "Hello!");
  }());
  static_assert([]() {
    parser parser;
    auto alpha = parser.range('A', 'Z') | parser.range('0', '9') |
                 parser.range('a', 'z');
    auto five_choice = (alpha | alpha);
    auto five_alpha = (five_choice & five_choice & five_choice & five_choice &
                       five_choice & five_choice) |
                      parser.string("Hello!");
    return parser.match(five_alpha, "Hello!");
  }());
  static_assert([]() {
    parser parser;
    auto alpha = parser.range('A', 'Z') | parser.range('0', '9') |
                 parser.range('a', 'z') | parser.range('A', 'Z') |
                 parser.range('0', '9') | parser.range('a', 'z') |
                 parser.set("!");
    auto five_choice = (alpha | alpha | alpha | alpha | alpha);
    auto five_alpha = five_choice & five_choice & five_choice & five_choice &
                      five_choice & five_choice;
    return parser.match(five_alpha, "Hello!");
  }());
  static_assert([]() {
    parser parser;
    auto alpha = parser.range('A', 'Z') | parser.range('0', '9') |
                 parser.range('a', 'z') | parser.range('A', 'Z') |
                 parser.range('0', '9') | parser.range('a', 'z');
    auto five_choice = (alpha | alpha | alpha | alpha | alpha);
    auto five_alpha = five_choice & five_choice & five_choice & five_choice &
                      five_choice & (five_choice | parser.string("!"));
    return parser.match(five_alpha, "Hello!");
  }());
  static_assert([]() {
    parser parser;
    auto alpha = parser.range('A', 'Z') |
                 ((parser.range('a', 'z') &
                   (parser.range('0', '9') | parser.range('a', 'z')) &
                   (parser.range('0', '9') | parser.range('a', 'z')))) |
                 parser.range('a', 'z');
    auto five_choice = (alpha | alpha);
    auto five_alpha = (five_choice & five_choice & five_choice & five_choice &
                       five_choice & five_choice) |
                      parser.string("Hello!");
    return parser.match(five_alpha, "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_character() & parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") & parser.arbitrary_character() &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") & parser.arbitrary_character() &
                            parser.arbitrary_character() & parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") &
                            (parser.string("el") | parser.string("ell") |
                             parser.arbitrary_character()) &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") &
                            (parser.string("el") |
                             parser.arbitrary_character() |
                             parser.string("ell")) &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_character() & parser.string("aaaa") &
                             parser.arbitrary_character() &
                             parser.string("bbbbb"),
                         "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.arbitrary_character() & parser.string("aaaa") &
            (parser.arbitrary_character() & parser.string("bbbbb") |
             parser.arbitrary_character() & parser.string("bbbb")),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.not_span_range_greedy('!', '!') & parser.string("!"), "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") &
                            parser.not_span_range_greedy('!', '!') &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.not_span_range_greedy('a', 'a') & parser.string("aaaa") &
            parser.not_span_range_greedy('b', 'b') & parser.string("bbbb"),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.not_span_range_greedy('a', 'a') & parser.string("aaaa") &
            parser.not_span_range_greedy('b', 'b') & parser.string("bbbbb"),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.not_span_range_greedy('a', 'a') & parser.string("aaaa") &
            (parser.not_span_range_greedy('b', 'b') & parser.string("bbbbb") |
             parser.not_span_range_greedy('b', 'b') & parser.string("bbbb")),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.not_span_set_greedy("dc!") & parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("H") & parser.not_span_set_greedy("dc!") &
                            parser.string("!"),
                        "Hello!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.not_span_set_greedy("dca") & parser.string("aaaa") &
            parser.not_span_set_greedy("dcb") & parser.string("bbbb"),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.not_span_set_greedy("dca") & parser.string("aaaa") &
            parser.not_span_set_greedy("dcb") & parser.string("bbbbb"),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.not_span_set_greedy("dca") & parser.string("aaaa") &
            (parser.not_span_set_greedy("dcb") & parser.string("bbbbb") |
             parser.not_span_set_greedy("dcb") & parser.string("bbbb")),
        "bbbaaaaaaabbbaaaaaaaabbbbaa!");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.string("abcdefgh"),
                        "abcdefgabcdefgabcdefgabcdefgabcdefgabcdefghabcdefg");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("abcdefg") &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.string("abcdefgh"),
                        "abcdefgabcdefgabcdefgabcdefgabcdefgabcdefghabcdefg");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.arbitrary_pattern(parser.string("abcdefg")) &
            parser.arbitrary_pattern(parser.string("abcdefg")) &
            parser.string("abcdefgh"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.arbitrary_pattern(
                            (parser.arbitrary_character()))) &
                            parser.string("b"),
                        "aaaaab");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern(parser.arbitrary_pattern(
                             (parser.arbitrary_character()))) &
                             parser.string("c"),
                         "aaaaab");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_character() &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdef")) &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.arbitrary_pattern(parser.string("abcdef")) &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.arbitrary_pattern(parser.string("abcdefg")) &
                            parser.arbitrary_pattern(parser.string("abcdef")) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match((parser.arbitrary_pattern(parser.string("abcdefg")) |
                         parser.arbitrary_pattern(parser.string("abcdef"))) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern(parser.string("abcdefg") |
                                                  parser.string("abcdefg")) &
                             parser.string("abcdefgh"),
                         "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("b") |
                                                 parser.string("bb") |
                                                 parser.string("bbb")) &
                            parser.string("a"),
                        "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.arbitrary_pattern(parser.string("bb") | parser.string("bbb")) &
            parser.string("c"),
        "bbbbbbbbbbbbbbbbbbbbbbba");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern(parser.string("bbb") |
                                                  parser.string("bbbbb") |
                                                  parser.string("bbba")) &
                             parser.string("c"),
                         "bbbbbbbbbbbbbbbbbbbbbbba");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.arbitrary_pattern(parser.string("bb") | parser.string("bbb")) &
            parser.string("a"),
        "bbbbbbbbbbbbbbbbbbbbbbba");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("bbbc") |
                                                 parser.string("bbbbbc") |
                                                 parser.string("b")) &
                            parser.string("a"),
                        "bbbbbbcbbbbbbbbcbbbbbbcbbbca");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern(parser.string("bbbc") |
                                                  parser.string("bbbbbc") |
                                                  parser.string("bb")) &
                             parser.string("a"),
                         "bbbbbbcbbbbbbbbcbbbbbbcbbbca");
  }());
  static_assert([]() {
    parser parser;
    return parser.match((parser.arbitrary_pattern(parser.string("bbbc") |
                                                  parser.string("bbbbbc") |
                                                  parser.string("bb")) &
                         parser.string("a")) |
                            (parser.span_set("bc") & parser.string("a")),
                        "bbbbbbcbbbbbbbbcbbbbbbcbbbca");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg") |
                                                 parser.string("abcdefg") |
                                                 parser.string("abcdef")) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg") |
                                                 parser.string("abcdefg") |
                                                 parser.arbitrary_character()) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(parser.string("abcdefg") |
                                                 parser.string("abcdefg") |
                                                 parser.arbitrary_character()) &
                            parser.string("abcdefgh"),
                        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.arbitrary_pattern(
            parser.string("abcdefg") | parser.string("abcdefg") |
            (parser.string("a") & parser.arbitrary_character())) &
            parser.string("abcdefgh"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.arbitrary_pattern(
            parser.string("abcdefg") | parser.string("abcdefg") |
            (parser.string("a") & parser.arbitrary_character())) &
            parser.string("abcdefghi"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern((
                             parser.string("a") & parser.arbitrary_character() &
                             parser.string("a"))) &
                             parser.string("abcdefghi"),
                         "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.arbitrary_pattern(
            parser.string("abcdefg") | parser.string("abcdefg") |
            (parser.string("a") &
             parser.arbitrary_pattern(parser.arbitrary_character()))) &
            parser.string("abcdefgh"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.arbitrary_pattern(
            parser.string("abcdefg") | parser.string("abcdefg") |
            (parser.string("a") &
             parser.arbitrary_pattern(parser.arbitrary_character()))) &
            parser.string("abcdefghi"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.arbitrary_pattern(
            parser.string("abcdefg") | parser.string("abcdefg") |
            parser.string("a") &
                parser.arbitrary_pattern(parser.arbitrary_character() &
                                         parser.length(1))) &
            parser.string("abcdefghi"),
        "abcdefabcdefabcdefabcdefabcdefabcdefghabcdef");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.arbitrary_pattern(
                            parser.arbitrary_pattern(parser.range('c', 'd')) |
                            parser.arbitrary_pattern(parser.range('a', 'b'))) &
                            parser.string("a") & parser.string("a") &
                            parser.string("e"),
                        "dacbddaaae");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_pattern(
                             parser.arbitrary_pattern(parser.range('c', 'd')) |
                             parser.arbitrary_pattern(parser.range('a', 'b'))) &
                             parser.string("a") & parser.string("a") &
                             parser.string("f"),
                         "dacbddaaae");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.length(10) & parser.string("a"), "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.length(10), "01234567");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.length(10) & parser.position(10) &
                            parser.string("a"),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.length(1) & parser.tab(4) & parser.position(4) &
                            parser.length(3) & parser.tab(8) &
                            parser.position(8) & parser.tab(10) &
                            parser.string("a"),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.length(1) & parser.tab(4) &
                            parser.position_reverse(8) & parser.length(3) &
                            parser.tab(8) & parser.position_reverse(4) &
                            parser.tab(10) & parser.string("a"),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.length(1) & parser.tab_reverse(8) &
                            parser.position_reverse(8) & parser.length(3) &
                            parser.tab_reverse(4) & parser.position_reverse(4) &
                            parser.tab(10) & parser.string("a"),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.remaining() & parser.position(12),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.remaining() & parser.position(11),
                         "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.remaining() & parser.position(13),
                         "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.remaining() & parser.position_reverse(0),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.remaining() & parser.position_reverse(1),
                         "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.remaining() & parser.position_reverse(-1),
                         "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.remaining() & parser.position(11) |
                            parser.remaining() & parser.position(12),
                        "0123456789a");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.string("abcde") & parser.quit(), "abcde");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match((parser.string("abcde") & parser.quit()) |
                             parser.string("abcde"),
                         "abcde");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("ab") & parser.found() & parser.string("e"), "abcde");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.string("abcde") & parser.backtrack(), "abcde");
  }());
  static_assert([]() {
    parser parser;
    return parser.match((parser.string("abcde") & parser.backtrack()) |
                            parser.string("abcde"),
                        "abcde");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.string("a") &
            parser.no_retry(parser.arbitrary_character() & parser.string("d")) &
            parser.string("f"),
        "abcdedf");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("a") &
                            (parser.no_retry(parser.arbitrary_character() &
                                             parser.string("d")) |
                             parser.string("bcded")) &
                            parser.string("f"),
                        "abcdedf");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("a") &
            (parser.no_retry(parser.range('b', 'c') | parser.string("d")) |
             parser.string("bcded")) &
            parser.string("f"),
        "abcdedf");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(
        parser.string("a") &
            (parser.no_retry(parser.arbitrary_pattern(parser.range('b', 'c') |
                                                      parser.range('d', 'e')) &
                             parser.string("f")) |
             parser.string("bcded")) &
            parser.string("f"),
        "abcdedf");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(parser.arbitrary_character() & parser.string("c") &
                             parser.no_backtrack() & parser.string("e"),
                         "abcde");
  }());
  static_assert([]() {
    parser parser;
    return !parser.match(
        parser.string("a") &
            (parser.arbitrary_pattern(
                 parser.range('b', 'c') |
                 (parser.range('d', 'e') & parser.no_backtrack())) |
             parser.string("bcded")) &
            parser.string("f"),
        "abcdedf");
  }());
  static_assert([]() {
    parser parser;
    return parser.match(parser.string("a") &
                            (parser.arbitrary_pattern(parser.range('b', 'c') |
                                                      (parser.range('d', 'e') &
                                                       parser.no_backtrack())) |
                             (parser.string("bcded") & parser.quit())) &
                            (parser.string("f") | parser.string("edf")),
                        "abcdedf");
  }());

  static_assert([]() {
    parser parser;
    auto p_string = parser.string("Hello  !");
    if (p_string.m_index.value() != 0)
      return false;
    auto p_fence = parser.no_retry(p_string);
    if (p_fence.m_index.value() != 1)
      return false;
    if (parser.m_top != 2)
      return false;

    bool rc = false;
    parser.m_patterns[0].m_type.each(
        [&rc](const pattern_base::string &) { rc = true; });
    if (!rc)
      return rc;
    rc = false;
    parser.m_patterns[1].m_type.each(
        [&rc](const pattern_base::no_retry &) { rc = true; });
    if (!rc)
      return rc;

    auto p_and = p_string & p_fence;
    if (p_and.m_index.value() != 2)
      return false;
    auto p_or = p_string | p_fence;
    if (p_or.m_index.value() != 3)
      return false;
    if (parser.m_top != 4)
      return false;

    rc = false;
    parser.m_patterns[2].m_type.each([&rc](const pattern_base::chain &p) {
      rc = p.m_pattern_index.value() == 0 && p.m_next.value() == 1;
    });
    if (!rc)
      return rc;
    rc = false;
    parser.m_patterns[3].m_type.each([&rc](const pattern_base::choice &p) {
      rc = p.m_pattern_index.value() == 0 && p.m_next.value() == 1;
    });
    if (!rc)
      return rc;

    return true;
  }());

  // LINKING TEST
  static_assert([]() {
    auto check_string = [](auto &parser,
                           decltype(pattern_trait::max_patterns()) index,
                           const char(&s)[2]) {
      bool rc = true;
      parser.m_patterns[index].m_type.each([&parser, &rc, &s](auto &p) {
        rc &=
            type_v<decltype(p)>.remove_cvref() == type_v<pattern_base::string>;
        if constexpr (type_v<decltype(p)>.remove_cvref() ==
                      type_v<pattern_base::string>) {
          decltype(pattern_trait::string_pool_length()) pool_index = 0;
          if (p.m_pool_index)
            pool_index = parser.m_string_indices[p.m_pool_index - 1];
          rc &= parser.m_string_pool[pool_index] == s[0];
        }
      });
      return rc;
    };

    auto check_three = [&check_string](
                           auto &parser,
                           decltype(pattern_trait::max_patterns()) index,
                           const char(&a)[2], const char(&b)[2],
                           const char(&c)[2]) {
      bool rc = false;
      parser.m_patterns[index].m_type.each([&check_string, &rc, &parser, &a, &b,
                                            &c](const pattern_base::choice &p) {
        rc = true;
        parser.m_patterns[p.m_pattern_index.value()].m_type.each(
            [&check_string, &rc, &parser, &a, &b](auto &p) {
              rc &= type_v<decltype(p)>.remove_cvref() ==
                    type_v<pattern_base::choice>;
              if constexpr (type_v<decltype(p)>.remove_cvref() ==
                            type_v<pattern_base::choice>) {
                rc &= check_string(parser, p.m_pattern_index.value(), a);
                rc &= check_string(parser, p.m_next.value(), b);
              }
            });
        rc &= check_string(parser, p.m_next.value(), c);
      });
      return rc;
    };

    auto check_abc =
        [&check_three](auto &parser,
                       decltype(pattern_trait::max_patterns()) index) {
          return check_three(parser, index, "a", "b", "c");
        };

    parser parser;
    pattern p_abc =
        parser.string("a") | parser.string("b") | parser.string("c");
    if (parser.m_string_top != 3)
      return false;
    parser.string("d") | parser.string("e") | parser.string("f") | p_abc;
    if (parser.m_string_top != 6)
      return false;
    (p_abc & parser.string("g")) | (parser.string("h") & parser.string("i"));
    if (parser.m_string_top != 9)
      return false;

    if (parser.m_top != 9 /* Individual patterns */ + 2 /* abc */ +
                            3 /* abcdef */ + 3 /* abcghi */)
      return false;

    p_abc | p_abc | p_abc;
    if (!check_abc(parser, 4))
      return false;

    bool rc = true;
    parser.m_patterns[10].m_type.each([&check_three, &check_abc, &rc,
                                       &parser](auto &&p) {
      rc &= type_v<decltype(p)>.remove_cvref() == type_v<pattern_base::choice>;
      if constexpr (type_v<decltype(p)>.remove_cvref() ==
                    type_v<pattern_base::choice>) {
        rc &= check_three(parser, p.m_pattern_index.value(), "d", "e", "f");
        rc &= check_abc(parser, p.m_next.value());
      }
    });
    if (!rc)
      return rc;

    parser.m_patterns[16].m_type.each([&check_string, &check_abc, &rc,
                                       &parser](const pattern_base::choice &p) {
      rc = true;
      parser.m_patterns[p.m_pattern_index.value()].m_type.each(
          [&check_string, &check_abc, &rc, &parser](auto &&p) {
            rc &= type_v<decltype(p)>.remove_cvref() ==
                  type_v<pattern_base::chain>;
            if constexpr (type_v<decltype(p)>.remove_cvref() ==
                          type_v<pattern_base::chain>) {
              rc &= check_abc(parser, p.m_pattern_index.value());
              rc &= check_string(parser, p.m_next.value(), "g");
            }
          });
      parser.m_patterns[p.m_next.value()].m_type.each([&check_string, &rc,
                                                       &parser](auto &&p) {
        rc &= type_v<decltype(p)>.remove_cvref() == type_v<pattern_base::chain>;
        if constexpr (type_v<decltype(p)>.remove_cvref() ==
                      type_v<pattern_base::chain>) {
          rc &= check_string(parser, p.m_pattern_index.value(), "h");
          rc &= check_string(parser, p.m_next.value(), "i");
        }
      });
    });
    if (!rc)
      return rc;

    return true;
  }());
};
} // namespace gut
