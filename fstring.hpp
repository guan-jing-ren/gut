#pragma once

#include "buffer.hpp"

namespace gut {
template <typename, count_t> struct fstring;

template <typename...>
inline constexpr constant is_fstring_v = constant_v<false>;
template <typename CharType, count_t Length>
inline constexpr constant is_fstring_v<fstring<CharType, Length>> =
    constant_v<true>;

template <typename CharType, count_t Length>
struct fstring : private buffer<CharType, Length + 1> {
  using char_type = CharType;
  constexpr static decltype(auto) char_type_v();
  constexpr static decltype(auto) count();
  using base_type = buffer<char_type, count() + 1>;
  constexpr static decltype(auto) base_type_v();

  constexpr fstring() = default;
  constexpr fstring(const fstring &) = default;
  constexpr fstring(fstring &&) = default;
  constexpr fstring &operator=(const fstring &) = default;
  constexpr fstring &operator=(fstring &&) = default;

  template <count_t StringLength>
  constexpr fstring(const char_type (&)[StringLength]);

  using base_type::operator[];

  constexpr buffer<CharType, Length + 1> to_buffer() const;

  template <index_t Start = 0, index_t End = count()>
  constexpr decltype(auto) substr(index<Start> = {}, index<End> = {}) const;

  template <typename, count_t> friend struct fstring;

  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator==(const fstring<OtherCharType, OtherLength> &,
                                   const OtherCharType (&)[OtherLength + 1]);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool
  operator==(const OtherCharType (&)[OtherLength],
             const fstring<OtherCharType, OtherLength - 1> &);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator==(const fstring<OtherCharType, OtherLength> &,
                                   const fstring<OtherCharType, OtherLength> &);

  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator!=(const fstring<OtherCharType, OtherLength> &,
                                   const OtherCharType (&)[OtherLength + 1]);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool
  operator!=(const OtherCharType (&)[OtherLength],
             const fstring<OtherCharType, OtherLength - 1> &);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator!=(const fstring<OtherCharType, OtherLength> &,
                                   const fstring<OtherCharType, OtherLength> &);

  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator<(const fstring<OtherCharType, OtherLength> &,
                                  const OtherCharType (&)[OtherLength + 1]);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool
  operator<(const OtherCharType (&)[OtherLength],
            const fstring<OtherCharType, OtherLength - 1> &);
  template <typename OtherCharType, count_t OtherLength>
  friend constexpr bool operator<(const fstring<OtherCharType, OtherLength> &,
                                  const fstring<OtherCharType, OtherLength> &);

  template <typename OtherCharType, count_t LeftLength, count_t RightLength>
  friend constexpr fstring<OtherCharType, LeftLength + RightLength - 1>
  operator+(const fstring<OtherCharType, LeftLength> &,
            const OtherCharType (&)[RightLength]);
  template <typename OtherCharType, count_t LeftLength, count_t RightLength>
  friend constexpr fstring<OtherCharType, LeftLength + RightLength - 1>
  operator+(const OtherCharType (&)[LeftLength],
            const fstring<OtherCharType, RightLength> &);
  template <typename OtherCharType, count_t LeftLength, count_t RightLength>
  friend constexpr fstring<OtherCharType, LeftLength + RightLength>
  operator+(const fstring<OtherCharType, LeftLength> &,
            const fstring<OtherCharType, RightLength> &);
};
template <count_t StringLength>
fstring(const char (&)[StringLength])->fstring<char, StringLength - 1>;
template <count_t StringLength>
fstring(const signed char (&)[StringLength])
    ->fstring<signed char, StringLength - 1>;
template <count_t StringLength>
fstring(const unsigned char (&)[StringLength])
    ->fstring<unsigned char, StringLength - 1>;
template <count_t StringLength>
fstring(const wchar_t (&)[StringLength])->fstring<wchar_t, StringLength - 1>;
template <count_t StringLength>
fstring(const char16_t (&)[StringLength])->fstring<char16_t, StringLength - 1>;
template <count_t StringLength>
fstring(const char32_t (&)[StringLength])->fstring<char32_t, StringLength - 1>;

template <typename CharType, count_t Length>
constexpr decltype(auto) fstring<CharType, Length>::char_type_v() {
  return type_v<char_type>;
}

template <typename CharType, count_t Length>
constexpr decltype(auto) fstring<CharType, Length>::count() {
  return count_v<Length>;
}

template <typename CharType, count_t Length>
constexpr decltype(auto) fstring<CharType, Length>::base_type_v() {
  return type_v<base_type>;
}

template <typename CharType, count_t Length>
template <count_t StringLength>
constexpr fstring<CharType, Length>::fstring(
    const char_type (&string)[StringLength])
    : base_type([&string]() mutable constexpr->decltype(auto) {
        static_assert(StringLength - 1 <= Length);
        base_type base{};
        for (count_t i = 0; i < StringLength; ++i)
          base[i] = string[i];
        return base;
      }()) {}

template <typename CharType, count_t Length>
constexpr buffer<CharType, Length + 1>
fstring<CharType, Length>::to_buffer() const {
  return *this;
}

template <typename CharType, count_t Length>
template <index_t Start, index_t End>
constexpr decltype(auto) fstring<CharType, Length>::substr(index<Start>,
                                                           index<End>) const {
  static_assert(Start >= 0);
  static_assert(End <= count());
  static_assert(End >= Start);
  fstring<CharType, End - Start> truncated{};
  for (count_t i = 0; i < truncated.count(); ++i)
    truncated[i] = this->operator[](i + Start);
  return truncated;
}

template <typename CharType, count_t Length>
constexpr bool operator==(const fstring<CharType, Length> &l,
                          const CharType (&r)[Length + 1]) {
  for (count_t i = 0; i < Length; ++i)
    if (l[i] != r[i])
      return false;
  return true;
}

template <typename CharType, count_t Length>
constexpr bool operator==(const CharType (&l)[Length],
                          const fstring<CharType, Length - 1> &r) {
  return r == l;
}

template <typename CharType, count_t Length>
constexpr bool operator==(const fstring<CharType, Length> &l,
                          const fstring<CharType, Length> &r) {
  return l == r.data();
}

template <typename CharType, count_t Length>
constexpr bool operator!=(const fstring<CharType, Length> &l,
                          const CharType (&r)[Length + 1]) {
  return !(l == r);
}

template <typename CharType, count_t Length>
constexpr bool operator!=(const CharType (&l)[Length],
                          const fstring<CharType, Length - 1> &r) {
  return r != l;
}

template <typename CharType, count_t Length>
constexpr bool operator!=(const fstring<CharType, Length> &l,
                          const fstring<CharType, Length> &r) {
  return l != r.data();
}

template <typename CharType, count_t Length>
constexpr bool operator<(const fstring<CharType, Length> &l,
                         const CharType (&r)[Length + 1]) {
  count_t i = 0;
  while (i < Length && l[i] == r[i])
    ++i;
  return i < Length && l[i] < r[i];
}

template <typename CharType, count_t Length>
constexpr bool operator<(const CharType (&l)[Length],
                         const fstring<CharType, Length - 1> &r) {
  count_t i = 0;
  while (i < Length - 1 && l[i] == r[i])
    ++i;
  return i < Length - 1 && l[i] < r[i];
}

template <typename CharType, count_t Length>
constexpr bool operator<(const fstring<CharType, Length> &l,
                         const fstring<CharType, Length> &r) {
  return l < r.data();
}

template <typename CharType, count_t LeftLength, count_t RightLength>
constexpr fstring<CharType, LeftLength + RightLength - 1>
operator+(const fstring<CharType, LeftLength> &l,
          const CharType (&r)[RightLength]) {
  fstring<CharType, LeftLength + RightLength - 1> appended{l.data()};
  for (count_t i = 0; i < RightLength - 1; ++i)
    appended[LeftLength + i] = r[i];
  return appended;
}

template <typename CharType, count_t LeftLength, count_t RightLength>
constexpr fstring<CharType, LeftLength + RightLength - 1>
operator+(const CharType (&l)[LeftLength],
          const fstring<CharType, RightLength> &r) {
  fstring<CharType, LeftLength + RightLength - 1> appended{l};
  for (count_t i = 0; i < RightLength; ++i)
    appended[LeftLength + i - 1] = r[i];
  return appended;
}

template <typename CharType, count_t LeftLength, count_t RightLength>
constexpr fstring<CharType, LeftLength + RightLength>
operator+(const fstring<CharType, LeftLength> &l,
          const fstring<CharType, RightLength> &r) {
  return l + r.data();
}
} // namespace gut
