#include "mangle.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(type_signature<int[]> == "[0,i5");
static_assert(type_signature<long double[47][37][5]> == "[2F,[25,[5,f7");
static_assert(type_signature<int, char, char16_t[], float> == "(i5i3[0,u4f5)");
static_assert(type_signature<type_list<int, bool, void>[7]> == "{7,i5b0}");
static_assert(type_signature<int, char, type_list<int, bool, void>, float> ==
              "(i5i3(i5b0)f5)");
enum class DummyEnum : int {};
static_assert(
    type_signature<int, char, type_list<DummyEnum, bool, void>[7], float> ==
    "(i5i3{7,i5b0}f5)");
} // namespace
