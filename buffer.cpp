#include "buffer.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
// static_assert(sizeof(buffer{{something{}, something{}, something{}}}) ==
// 3); // TODO: Implement optimization for array of empty objects.
constexpr buffer test = []() { return buffer{{0, 1, 2, 3, 4, 5}}; }();
constexpr buffer copied = []() {
  auto [a, b, c, d, e, f] = test;
  buffer buf{{a, b, c, d, e, f}};
  buf = test;
  return buffer{{a, b, c, d, e, f}};
}();
static_assert(!is_buffer_v<decltype(test)>,
              "Reference of buffer is not a buffer.");
static_assert(is_buffer_v<remove_cvref_t<decltype(test)>>,
              "buffer should be buffer.");
static_assert(test.count() == test.cend() - test.cbegin(),
              "Test buffer size not as specified.");
static_assert(test[3] == 3, "Test buffer element not as specified.");
static_assert(constant<test[5]>::value == 5,
              "Test buffer element not as specified.");
static_assert(test == copied,
              "Test buffer equality || structured binding failed.");
} // namespace
