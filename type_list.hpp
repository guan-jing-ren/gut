#pragma once

#include "type.hpp"

namespace gut {
template <index_t Index, typename Type>
struct indexed_type : index<Index>, type<Type> {
  static_assert(indexed_type::value() >= 0,
                "An indexed type must have a non-negative index.");

  constexpr indexed_type() = default;
  constexpr indexed_type(const indexed_type &that) = default;
  constexpr indexed_type(indexed_type &&that) = default;
  constexpr indexed_type &operator=(const indexed_type &that) = default;
  constexpr indexed_type &operator=(indexed_type &&that) = default;
};
template <typename Type> indexed_type(type<Type>)->indexed_type<0, Type>;

template <typename... IndexTypes> struct type_list;
template <typename... CandidateTypes>
inline constexpr constant is_type_list_v = constant_v<false>;
template <typename... IndexTypes>
inline constexpr constant is_type_list_v<type_list<IndexTypes...>> =
    constant_v<true>;

template <typename... IndexTypes> struct type_list : IndexTypes... {
  template <template <typename> typename Decorator>
  using decorated = type_list<Decorator<IndexTypes>...>;

  constexpr static decltype(auto) count();

  static_assert(count(), "Type list should not be empty.");

  template <typename Type> constexpr type_list(type<Type>);
  constexpr type_list() = default;
  constexpr type_list(const type_list &that) = default;
  constexpr type_list(type_list &&that) = default;
  constexpr type_list &operator=(const type_list &that) = default;
  constexpr type_list &operator=(type_list &&that) = default;

  template <
      typename... ThatTypes,
      typename = std::enable_if_t<
          (... + (std::is_constructible_v<IndexTypes, ThatTypes>)) == count()>>
  constexpr type_list(ThatTypes &&...);

  template <typename... Types>
  constexpr type_list(const type_list<Types...> &that);
  template <typename... Types> constexpr type_list(type_list<Types...> &that);
  template <typename... Types> constexpr type_list(type_list<Types...> &&that);
  template <typename... Types>
  constexpr decltype(auto) operator=(const type_list<Types...> &that);
  template <typename... Types>
  constexpr decltype(auto) operator=(type_list<Types...> &that);
  template <typename... Types>
  constexpr decltype(auto) operator=(type_list<Types...> &&that);

  template <typename Reduce> constexpr static decltype(auto) reduce(Reduce &&);
};
template <typename Type>
type_list(type<Type>)->type_list<indexed_type<0, Type>>;

template <typename... Types> constexpr decltype(auto) make_type_list();

template <template <typename> typename Decorator, typename... Types>
constexpr decltype(auto) make_type_list();

template <index_t Index, typename Type>
constexpr decltype(auto) get_type(indexed_type<Index, Type>);

template <typename Type, index_t Index>
constexpr decltype(auto) get_index(indexed_type<Index, Type>);

template <typename Type, index_t... Indices, typename... Types>
constexpr decltype(auto)
get_indices(type_list<indexed_type<Indices, Types>...>);

template <template <typename> typename Matcher, index_t... Indices,
          typename... Types>
constexpr decltype(auto)
match_indices(type_list<indexed_type<Indices, Types>...>);

template <auto... Constants> constexpr decltype(auto) make_constant_list();

template <index_t Index, typename TypeList>
constexpr decltype(auto) get_constant(TypeList);

template <auto Constant, typename TypeList>
constexpr decltype(auto) constant_index(TypeList);

template <index_t... Index, typename... Type1, typename Type2>
constexpr decltype(auto) operator+(type_list<indexed_type<Index, Type1>...>,
                                   type<Type2>);

template <typename Type1, typename Type2>
constexpr decltype(auto) operator+(type<Type1> t1, type<Type2> t2);

////////////////////
// IMPLEMENTATION //
////////////////////

template <typename... IndexTypes>
constexpr decltype(auto) type_list<IndexTypes...>::count() {
  return count_v<sizeof...(IndexTypes)>;
}

template <typename... IndexTypes>
template <typename Type>
constexpr type_list<IndexTypes...>::type_list(type<Type>) {}

template <typename... IndexTypes>
template <typename... ThatTypes, typename>
constexpr type_list<IndexTypes...>::type_list(ThatTypes &&... that)
    : IndexTypes(std::forward<ThatTypes>(that))... {}

template <typename... IndexTypes>
template <typename... Types>
constexpr type_list<IndexTypes...>::type_list(const type_list<Types...> &that)
    : IndexTypes(static_cast<const Types &>(that))... {}

template <typename... IndexTypes>
template <typename... Types>
constexpr type_list<IndexTypes...>::type_list(type_list<Types...> &that)
    : IndexTypes(static_cast<Types &>(that))... {}

template <typename... IndexTypes>
template <typename... Types>
constexpr type_list<IndexTypes...>::type_list(type_list<Types...> &&that)
    : IndexTypes(std::move(static_cast<Types &&>(that)))... {}

template <typename... IndexTypes>
template <typename... Types>
constexpr decltype(auto) type_list<IndexTypes...>::
operator=(const type_list<Types...> &that) {
  (..., IndexTypes::operator=(static_cast<const Types &>(that)));
  return *this;
}

template <typename... IndexTypes>
template <typename... Types>
constexpr decltype(auto) type_list<IndexTypes...>::
operator=(type_list<Types...> &that) {
  (..., IndexTypes::operator=(static_cast<Types &>(that)));
  return *this;
}

template <typename... IndexTypes>
template <typename... Types>
constexpr decltype(auto) type_list<IndexTypes...>::
operator=(type_list<Types...> &&that) {
  (..., IndexTypes::operator=(std::move(static_cast<Types &&>(that))));
  return *this;
}

template <typename... IndexTypes>
template <typename Reduce>
constexpr decltype(auto) type_list<IndexTypes...>::reduce(Reduce &&r) {
  return r(IndexTypes{}...);
}

template <index_t... Index, typename... Type1, typename Type2>
constexpr decltype(auto) operator+(type_list<indexed_type<Index, Type1>...>,
                                   type<Type2>) {
  return type_list<indexed_type<Index, Type1>...,
                   indexed_type<sizeof...(Index), Type2>>{};
}

template <typename Type1, typename Type2>
constexpr decltype(auto) operator+(type<Type1> t1, type<Type2> t2) {
  return type_list{t1} + t2;
}

template <typename... Types> constexpr decltype(auto) make_type_list() {
  static_assert(sizeof...(Types), "Type list must have at least an element.");
  if constexpr (sizeof...(Types) == 1)
    return type_list{type<Types>{}...};
  else
    return (... + type_v<Types>);
}

template <index_t Index, typename Type>
constexpr decltype(auto) get_type(indexed_type<Index, Type>) {
  return type_v<Type>;
}

template <typename Type, index_t Index>
constexpr decltype(auto) get_index(indexed_type<Index, Type>) {
  return Index;
}

template <typename Type, index_t... Indices, typename... Types>
constexpr decltype(auto)
get_indices(type_list<indexed_type<Indices, Types>...>) {
  return (indices<-1>{} + ... +
          index_v<(std::is_same_v<Type, Types> ? Indices : -1)>);
}

template <template <typename> typename Matcher, index_t... Indices,
          typename... Types>
constexpr decltype(auto)
match_indices(type_list<indexed_type<Indices, Types>...>) {
  return (indices<-1>{} + ... + index_v<(Matcher<Types>{} ? Indices : -1)>);
}

template <auto... Constants> constexpr decltype(auto) make_constant_list() {
  return make_type_list<constant<Constants>...>();
}

template <index_t Index, typename TypeList>
constexpr decltype(auto) get_constant(TypeList list) {
  return decltype(get_type<Index>(list))::raw_type::value;
}

template <auto Constant, typename TypeList>
constexpr decltype(auto) constant_index(TypeList list) {
  return get_index<constant<Constant>>(list);
}
} // namespace gut
