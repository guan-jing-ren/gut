/*
<!--
*/
#pragma once
/*
-->
<nav>
<p><a href="ntuple.html"><code>
*/
#include "ntuple.hpp"
/*
</code></a></p>
</nav>
<h1><code>
*/
namespace gut {
/*
</code></h1>
<table>
<tr>
<td>
<textarea readonly rows="40" cols="80">
*/
struct arg {
  arg() = default;
  arg(const arg &) = default;
  arg(arg &&) = default;
  template <typename... Arg> explicit constexpr arg(Arg &&...);
  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&...) const;
  constexpr decltype(auto) operator[](const arg &) const;
  constexpr decltype(auto) operator=(const arg &) const;
};

inline constexpr arg arg_v;

template <typename FunctionType, typename FunctionArity> struct function;
template <typename...>
inline constexpr constant is_function_v = constant_v<false>;
template <typename FunctionType, typename FunctionArity>
inline constexpr constant is_function_v<function<FunctionType, FunctionArity>> =
    constant_v<true>;

template <typename F, count_t MinArgs = 0, count_t MaxArgs = 1>
constexpr decltype(auto) deduce_arity() {
  static_assert(MaxArgs < config::function::arity_deduction_limit);
  if constexpr (is_function_v<F>)
    return F::arity();
  else if constexpr (is_callable_v<F>)
    return 0;
  else {
    constexpr auto arity = make_index_list<MinArgs, MaxArgs>().reduce([
    ](auto... i) mutable constexpr->decltype(auto) {
      return (index_v<-1> + ... +
              make_index_list<i + 1>()
                  .reduce([i](auto... j) mutable constexpr->decltype(auto) {
                    if constexpr (is_callable_v<F, decltype(something{j})...>)
                      return i;
                    else
                      return index_v<-1>;
                  }));
    })[index_v<0>] + 1;
    if constexpr (arity == 0)
      return deduce_arity<
          F, MaxArgs, MaxArgs * config::function::arity_deduction_expansion>();
    else
      return arity;
  }
}

template <typename F>
inline constexpr constant arity_v = constant_v<deduce_arity<F>()>;
template <auto F>
inline constexpr constant arity_v<constant<F>> = arity_v<decltype(F)>;
template <typename R, typename... Args>
inline constexpr constant arity_v<R(Args...)> =
    constant_v<int{sizeof...(Args)}>;
template <typename R, typename... Args>
inline constexpr constant arity_v<R (*)(Args...)> =
    constant_v<int{sizeof...(Args)}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...)> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) volatile> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const volatile> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) &> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const &> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) volatile &> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const volatile &> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) &&> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const &&> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) volatile &&> =
    constant_v<int{sizeof...(Args) + 1}>;
template <typename R, typename C, typename... Args>
inline constexpr constant arity_v<R (C::*)(Args...) const volatile &&> =
    constant_v<int{sizeof...(Args) + 1}>;

template <typename F = something, typename A = ntuple<>>
struct function : private thing<F>, private thing<A> {
  constexpr static decltype(auto) arity();

  constexpr function(F f);
  constexpr function(F f, A a);

  constexpr function() = default;
  constexpr function(const function &) = default;
  constexpr function(function &) = default;
  constexpr function(function &&) = default;

  constexpr function &operator=(const function &) = default;
  constexpr function &operator=(function &) = default;
  constexpr function &operator=(function &&) = default;

  constexpr decltype(auto) callable() const;
  constexpr decltype(auto) callable();
  constexpr decltype(auto) arguments() const;
  constexpr decltype(auto) arguments();

  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args) const;
  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args);

private:
  template <typename Functor, typename... Args>
  static constexpr decltype(auto) call_or_bind(Functor &&f, Args &&... args);
};
template <typename F> function(F)->function<remove_cvref_t<F>, ntuple<>>;

#define GUT_COMMA ,

#define GUT_DEFINE_BINOP_UNBOUND(op)                                           \
  constexpr decltype(auto) operator op(const arg &, const arg &)
#define GUT_DEFINE_PREFIX_UNBOUND(op)                                          \
  constexpr decltype(auto) operator op(const arg &)
#define GUT_DEFINE_POSTFIX_UNBOUND(op)                                         \
  constexpr decltype(auto) operator op(const arg &, int)
#define GUT_DEFINE_BINOP_LEFTBIND(op)                                          \
  template <typename Arg,                                                      \
            typename =                                                         \
                std::enable_if_t<!is_function_v<remove_cvref_t<Arg>> &&        \
                                 !std::is_same_v<arg, remove_cvref_t<Arg>>>>   \
  constexpr decltype(auto) operator op(Arg &&, const arg &)
#define GUT_DEFINE_BINOP_RIGHTBIND(op)                                         \
  template <typename Arg,                                                      \
            typename =                                                         \
                std::enable_if_t<!is_function_v<remove_cvref_t<Arg>> &&        \
                                 !std::is_same_v<arg, remove_cvref_t<Arg>>>>   \
  constexpr decltype(auto) operator op(const arg &, Arg &&)
#define GUT_DEFINE_BINOP_LEFTFUNCBIND(op)                                      \
  template <typename FunctionType, typename FunctionArity, typename Arg>       \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &, Arg &&)
#define GUT_DEFINE_BINOP_RIGHTFUNCBIND(op)                                     \
  template <typename Arg, typename FunctionType, typename FunctionArity>       \
  constexpr decltype(auto) operator op(                                        \
      const Arg &, const function<FunctionType, FunctionArity> &)
#define GUT_DEFINE_PREFIX_FUNCBIND(op)                                         \
  template <typename FunctionType, typename FunctionArity>                     \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &)
#define GUT_DEFINE_POSTFIX_FUNCBIND(op)                                        \
  template <typename FunctionType, typename FunctionArity>                     \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &, int)
#define GUT_DEFINE_BINOP(op)                                                   \
  GUT_DEFINE_BINOP_UNBOUND(op);                                                \
  GUT_DEFINE_BINOP_LEFTBIND(op);                                               \
  GUT_DEFINE_BINOP_RIGHTBIND(op);                                              \
  GUT_DEFINE_BINOP_LEFTFUNCBIND(op);                                           \
  GUT_DEFINE_BINOP_RIGHTFUNCBIND(op)
#define GUT_DEFINE_POSTFIX(op)                                                 \
  GUT_DEFINE_POSTFIX_UNBOUND(op);                                              \
  GUT_DEFINE_POSTFIX_FUNCBIND(op)
#define GUT_DEFINE_PREFIX(op)                                                  \
  GUT_DEFINE_PREFIX_UNBOUND(op);                                               \
  GUT_DEFINE_PREFIX_FUNCBIND(op)

GUT_DEFINE_POSTFIX(++);
GUT_DEFINE_POSTFIX(--);
GUT_DEFINE_BINOP(->*);
GUT_DEFINE_PREFIX(++);
GUT_DEFINE_PREFIX(--);
GUT_DEFINE_PREFIX(+);
GUT_DEFINE_PREFIX(-);
GUT_DEFINE_PREFIX(!);
GUT_DEFINE_PREFIX(~);
GUT_DEFINE_PREFIX(*);
GUT_DEFINE_PREFIX(&);
// new, arraynew, delete, arraydelete
GUT_DEFINE_BINOP(*);
GUT_DEFINE_BINOP(/);
GUT_DEFINE_BINOP(%);
GUT_DEFINE_BINOP(+);
GUT_DEFINE_BINOP(-);
GUT_DEFINE_BINOP(<<);
GUT_DEFINE_BINOP(>>);
GUT_DEFINE_BINOP(<);
GUT_DEFINE_BINOP(<=);
GUT_DEFINE_BINOP(>);
GUT_DEFINE_BINOP(>=);
GUT_DEFINE_BINOP(==);
GUT_DEFINE_BINOP(!=);
GUT_DEFINE_BINOP(&);
GUT_DEFINE_BINOP(^);
GUT_DEFINE_BINOP(|);
GUT_DEFINE_BINOP(&&);
GUT_DEFINE_BINOP(||);
GUT_DEFINE_BINOP(*=);
GUT_DEFINE_BINOP(/=);
GUT_DEFINE_BINOP(%=);
GUT_DEFINE_BINOP(+=);
GUT_DEFINE_BINOP(-=);
GUT_DEFINE_BINOP(<<=);
GUT_DEFINE_BINOP(>>=);
GUT_DEFINE_BINOP(&=);
GUT_DEFINE_BINOP(^=);
GUT_DEFINE_BINOP(|=);
GUT_DEFINE_BINOP_UNBOUND(GUT_COMMA);
GUT_DEFINE_BINOP_LEFTBIND(GUT_COMMA);
GUT_DEFINE_BINOP_RIGHTBIND(GUT_COMMA);
GUT_DEFINE_BINOP_LEFTFUNCBIND(GUT_COMMA);
GUT_DEFINE_BINOP_RIGHTFUNCBIND(GUT_COMMA);

#undef GUT_DEFINE_BINOP
#undef GUT_DEFINE_PREFIX
#undef GUT_DEFINE_POSTFIX

#undef GUT_DEFINE_BINOP_UNBOUND
#undef GUT_DEFINE_PREFIX_UNBOUND
#undef GUT_DEFINE_POSTFIX_UNBOUND

#undef GUT_DEFINE_BINOP_LEFTBIND
#undef GUT_DEFINE_BINOP_RIGHTBIND
#undef GUT_DEFINE_BINOP_LEFTFUNCBIND
#undef GUT_DEFINE_BINOP_RIGHTFUNCBIND
#undef GUT_DEFINE_PREFIX_FUNCBIND
#undef GUT_DEFINE_POSTFIX_FUNCBIND

#undef GUT_COMMA
/*
</textarea>
<details>
<summary><h2>Function adapters for control flow constructs
(UNUSED)</h2></summary>
<textarea readonly rows="40" cols="80">
*/
template <typename Body> struct felse : private thing<Body> {
  using body_base = thing<Body>;
  using body_type = Body;

private:
  template <typename, typename> friend struct fif;

  constexpr felse(Body body) : body_base(body) {}
};

template <typename Initializer, typename Condition, typename Body>
struct fif : private thing<Initializer>,
             private thing<Condition>,
             private thing<Body> {
  using initializer_base = thing<Initializer>;
  using initializer_type = Initializer;
  using condition_base = thing<Condition>;
  using condition_type = Condition;
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr fif(Initializer initializer, Condition condition, Body body)
      : initializer_base(initializer), condition_base(condition),
        body_base(body) {}

  constexpr fif(Condition condition, Body body)
      : initializer_base(something{}), condition_base(condition),
        body_base(body) {}

  template <typename ElseCondition, typename ElseBody>
  constexpr decltype(auto) felse_if(ElseCondition &&condition,
                                    ElseBody &&body) const {
    fif{std::forward<ElseCondition>(condition), std::forward<ElseBody>(body)};
    return *this;
  }

  template <typename ElseBody>
  constexpr decltype(auto) felse(ElseBody &&body) const {
    return gut::felse{std::forward<ElseBody>(body)};
  }
};

template <typename Initializer, typename Condition, typename Next,
          typename Body>
struct ffor : private thing<Initializer>,
              private thing<Condition>,
              private thing<Next>,
              private thing<Body> {
  using initializer_base = thing<Initializer>;
  using initializer_type = Initializer;
  using condition_base = thing<Condition>;
  using condition_type = Condition;
  using next_base = thing<Next>;
  using next_type = Next;
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr ffor(Initializer initializer, Condition condition, Next next,
                 Body body)
      : initializer_base(initializer), condition_base(condition),
        next_base(next), body_base(body) {}

  constexpr ffor(Condition condition, Next next, Body body)
      : initializer_base(something{}), condition_base(condition),
        next_base(next), body_base(body) {}

  constexpr ffor(Condition condition, Body body)
      : initializer_base(something{}), condition_base(condition),
        next_base(something{}), body_base(body) {}
};

template <typename Initializer, typename Condition, typename Body>
struct fwhile : private thing<Initializer>,
                private thing<Condition>,
                private thing<Body> {
  using initializer_base = thing<Initializer>;
  using initializer_type = Initializer;
  using condition_base = thing<Condition>;
  using condition_type = Condition;
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr fwhile(Initializer initializer, Condition condition, Body body)
      : initializer_base(initializer), condition_base(condition),
        body_base(body) {}

  constexpr fwhile(Condition condition, Body body)
      : initializer_base(something{}), condition_base(condition),
        body_base(body) {}
};

template <typename Body, typename Initializer, typename Condition>
struct fdo : private thing<Body>,
             private thing<Initializer>,
             private thing<Condition> {
  using initializer_base = thing<Initializer>;
  using initializer_type = Initializer;
  using condition_base = thing<Condition>;
  using condition_type = Condition;
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr fdo(Body body, Initializer initializer, Condition condition)
      : body_base(body), initializer_base(initializer),
        condition_base(condition) {}

  constexpr fdo(Body body, Condition condition)
      : body_base(body), initializer_base(something{}),
        condition_base(condition) {}
};

template <typename Match, typename Body>
struct fcase : private thing<Match>, private thing<Body> {
  using match_base = thing<Match>;
  using match_type = Match;
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr fcase(Match match, Body body)
      : match_base(match), body_base(body) {}
};

template <typename Body> struct fdefault : private thing<Body> {
  using body_base = thing<Body>;
  using body_type = Body;

  constexpr fdefault(Body body) : body_base(body) {}
};

template <typename Initializer, typename Condition>
struct fswitch : private thing<Initializer>, private thing<Condition> {
  using initializer_base = thing<Initializer>;
  using initializer_type = Initializer;
  using condition_base = thing<Condition>;
  using condition_type = Condition;

  constexpr fswitch(Initializer initializer, Condition condition)
      : initializer_base(initializer), condition_base(condition) {}

  constexpr fswitch(Condition condition)
      : initializer_base(something{}), condition_base(condition) {}

  template <typename Match, typename Body>
  constexpr decltype(auto) fcase(Match match, Body body) const {
    gut::fcase{match, body};
    return *this;
  }

  template <typename Body> constexpr decltype(auto) fdefault(Body body) const {
    gut::fdefault{body};
    return *this;
  }
};
/*
</textarea>
</details>
<details>
<summary><h2>IMPLEMENTATION</h2></summary>
<textarea readonly rows="40" cols="80">
*/
template <typename F, typename A>
constexpr decltype(auto) function<F, A>::arity() {
  if constexpr (type_v<ntuple<>> == remove_cvref_v<A>)
    return arity_v<F>;
  else
    return make_index_list<remove_cvref_t<A>::count()>().reduce([
    ](auto... i) mutable constexpr->decltype(auto) {
      return (0 + ... +
              (type_v<arg> == remove_cvref_v<decltype(std::declval<A>().get(i))>
                   ? 1
                   : 0));
    });
}

template <typename F, typename A>
constexpr function<F, A>::function(F f) : thing<F>(std::move(f)) {}

template <typename F, typename A>
constexpr function<F, A>::function(F f, A a)
    : thing<F>(std::move(f)), thing<A>(std::move(a)) {}

template <typename F, typename A>
constexpr decltype(auto) function<F, A>::callable() const {
  return thing<F>::get();
}

template <typename F, typename A>
constexpr decltype(auto) function<F, A>::callable() {
  return thing<F>::get();
}

template <auto A> constexpr decltype(auto) argument_binder(constant<A>) {
  if constexpr (A == 0)
    return ntuple<>{};
  else
    return make_index_list<A>().reduce(
        [](auto... i) { return ntuple{arg{i}...}; });
}

template <typename Bound, typename Unbound>
constexpr decltype(auto) woven_arguments(Bound &&bound, Unbound &&unbound) {
  return weave_arguments(
      std::forward<Bound>(bound), std::forward<Unbound>(unbound),
      [](auto &&... args) mutable constexpr->decltype(auto) {
        constexpr constant all_args = constant_v<(
            true && ... && (type_v<arg> == remove_cvref_v<decltype(args)>))>;
        if constexpr (all_args)
          return ntuple<>{};
        else
          return ntuple{std::forward<decltype(args)>(args)...};
      });
}

template <typename F, typename A>
constexpr decltype(auto) function<F, A>::arguments() const {
  if constexpr (remove_cvref_t<A>::count() == 0)
    return argument_binder(arity());
  else
    return thing<A>::get();
}

template <typename F, typename A>
constexpr decltype(auto) function<F, A>::arguments() {
  if constexpr (remove_cvref_t<A>::count() == 0)
    return argument_binder(arity());
  else
    return thing<A>::get();
}

template <typename F, typename A>
template <typename... Args>
constexpr decltype(auto) function<F, A>::operator()(Args &&... args) const {
  return call_or_bind(*this, std::forward<Args>(args)...);
}

template <typename F, typename A>
template <typename... Args>
constexpr decltype(auto) function<F, A>::operator()(Args &&... args) {
  return call_or_bind(*this, std::forward<Args>(args)...);
}

template <typename Unbound, typename Forwarder>
constexpr decltype(auto) weave_arguments(ntuple<>, Unbound &&unbound,
                                         Forwarder &&forwarder) {
  return unbound.reduce(forwarder);
}

template <typename Bound, typename Unbound, typename Forwarder>
constexpr decltype(auto) weave_arguments(Bound &&bound, Unbound &&unbound,
                                         Forwarder &&forwarder) {
  return make_index_list<remove_cvref_t<Bound>::count()>().reduce([
    &bound, &unbound, &forwarder
  ](auto... i) mutable constexpr->decltype(auto) {
    return forwarder([&bound, &
                      unbound ](auto i) mutable constexpr->decltype(auto) {
      (void)bound, (void)unbound;
      constexpr constant is_bound_arg =
          type_v<arg> == remove_cvref_v<decltype(bound.get(i))>;
      constexpr auto unbound_count = make_index_list<i + 1>().reduce(
          [](auto... j) mutable constexpr->decltype(auto) {
            return (... + (type_v<arg> == remove_cvref_v<decltype(bound.get(j))>
                               ? 1
                               : 0));
          });
      if constexpr (unbound_count <= remove_cvref_t<Unbound>::count() &&
                    is_bound_arg)
        return unbound.get(index_v<unbound_count - 1>);
      else
        return bound.get(i);
    }(i)...);
  });
}

template <typename F, typename A>
template <typename Functor, typename... Args>
constexpr decltype(auto) function<F, A>::call_or_bind(Functor &&f,
                                                      Args &&... args) {
  constexpr constant to_bind = []() mutable constexpr->decltype(auto) {
    using woven_type = decltype(woven_arguments(
        f.arguments(), forwarding_ntuple{std::forward<Args>(args)...}));
    if constexpr (woven_type::count() == 0)
      return constant_v<(false || ... ||
                         (type_v<arg> == remove_cvref_v<Args>))>;
    else
      return make_index_list<woven_type::count()>().reduce([](auto... i) {
        return constant_v<(
            false || ... ||
            (type_v<arg> ==
             remove_cvref_v<decltype(std::declval<woven_type>().get(i))>))>;
      });
  }
  ();
  if constexpr (to_bind)
    return gut::function{
        f.callable(),
        woven_arguments(f.arguments(),
                        forwarding_ntuple{std::forward<Args>(args)...})};
  else if constexpr (std::is_member_function_pointer_v<F>)
    return weave_arguments(
        f.arguments(), forwarding_ntuple{std::forward<Args>(args)...},
        ([&f](auto &&object,
              auto &&... args) mutable constexpr->decltype(auto) {
          return (object.*f.callable())(std::forward<decltype(args)>(args)...);
        }));
  else if constexpr (std::is_member_pointer_v<F>)
    return weave_arguments(f.arguments(),
                           forwarding_ntuple{std::forward<Args>(args)...},
                           ([&f](auto &&object) mutable constexpr->decltype(
                               auto) { return object.*f.callable(); }));
  else if constexpr (is_constant_v<F>) {
    if constexpr (std::is_member_function_pointer_v<decltype(F::value)>)
      return weave_arguments(
          f.arguments(), forwarding_ntuple{std::forward<Args>(args)...}, ([
          ](auto &&object, auto &&... args) mutable constexpr->decltype(auto) {
            return (object.*F::value)(std::forward<decltype(args)>(args)...);
          }));
    else if constexpr (std::is_member_pointer_v<decltype(F::value)>)
      return weave_arguments(f.arguments(),
                             forwarding_ntuple{std::forward<Args>(args)...},
                             ([&f](auto &&object) mutable constexpr->decltype(
                                 auto) { return object.*F::value; }));
    else
      return weave_arguments(
          f.arguments(), forwarding_ntuple{std::forward<Args>(args)...},
          ([&f](auto &&... args) mutable constexpr->decltype(auto) {
            return f.callable()(std::forward<decltype(args)>(args)...);
          }));
  } else
    return weave_arguments(
        f.arguments(), forwarding_ntuple{std::forward<Args>(args)...},
        ([&f](auto &&... args) mutable constexpr->decltype(auto) {
          return f.callable()(std::forward<decltype(args)>(args)...);
        }));
}

template <typename... Arg> constexpr arg::arg(Arg &&...) {}

template <typename... Args>
constexpr decltype(auto) arg::operator()(Args &&... args) const {
  return function([](auto &&bound, Args... unbound) constexpr->decltype(auto) {
    return std::forward<decltype(bound)>(bound)(std::forward<Args>(unbound)...);
  },
                  ntuple{arg_v, std::forward<Args>(args)...});
}

constexpr decltype(auto) arg::operator[](const arg &) const {
  return function([](auto &&bound, auto &&unbound) constexpr->decltype(auto) {
    return std::forward<decltype(bound)>(
        bound)[std::forward<decltype(unbound)>(unbound)];
  });
}

constexpr decltype(auto) arg::operator=(const arg &) const {
  return function([](auto &&bound, auto &&unbound) constexpr->decltype(auto) {
    return std::forward<decltype(bound)>(bound) =
               std::forward<decltype(unbound)>(unbound);
  });
}

template <count_t Start, count_t End, typename Func, typename... Args>
constexpr decltype(auto) partition_arguments(Func &&func, Args &&... args) {
  if constexpr (Start == End)
    return func();
  else
    return make_index_list<Start, End>().reduce([
      &func, forwarded = forwarding_ntuple{std::forward<Args>(args)...}
    ](auto... i) mutable constexpr->decltype(auto) {
      return func(forwarded.get(i)...);
    });
}

template <count_t Start, typename... Args>
constexpr decltype(auto) partition_arguments(Args &&... args) {
  return partition_arguments<Start, Start + 1>(
      [](auto &&a) mutable constexpr->decltype(auto) {
        return std::forward<decltype(a)>(a);
      },
      std::forward<Args>(args)...);
}

template <typename Left, typename Right>
constexpr decltype(auto) concatenate_arguments(Left &&l, Right &&r) {
  if constexpr (is_ntuple_v<remove_cvref_t<Left>> &&
                is_ntuple_v<remove_cvref_t<Right>>) {
    return l.reduce([&r](auto &&... l) mutable constexpr->decltype(auto) {
      return r.reduce([&l...](auto &&... r) mutable constexpr->decltype(auto) {
        return ntuple{l..., r...};
      });
    });
  } else if constexpr (is_ntuple_v<remove_cvref_t<Left>>) {
    if constexpr (type_v<something> == remove_cvref_v<Right>)
      return l;
    else
      return l.reduce([&r](auto &&... l) mutable constexpr->decltype(auto) {
        return ntuple{l..., std::forward<Right>(r)};
      });
  } else if constexpr (is_ntuple_v<remove_cvref_t<Right>>) {
    if constexpr (type_v<something> == remove_cvref_v<Left>)
      return r;
    else
      return r.reduce([&l](auto &&... r) mutable constexpr->decltype(auto) {
        return ntuple{std::forward<Left>(l), r...};
      });
  }
}

template <typename Combiner, typename UnboundLeft, typename UnboundRight,
          typename LeftArguments, typename RightArguments>
constexpr decltype(auto)
combine_function(Combiner combiner, [[maybe_unused]] UnboundLeft unbound_left,
                 [[maybe_unused]] UnboundRight unbound_right, LeftArguments l,
                 RightArguments r) {
  constexpr constant is_singular =
      type_v<something> == remove_cvref_v<RightArguments>;
  constexpr constant is_left_binding =
      type_v<something> != remove_cvref_v<UnboundLeft>;
  constexpr constant is_right_binding =
      type_v<something> != remove_cvref_v<UnboundRight>;
  static_assert(!is_left_binding ||
                type_v<something> != remove_cvref_v<LeftArguments>);
  static_assert(!is_right_binding ||
                type_v<something> != remove_cvref_v<RightArguments>);
  static_assert(!is_singular || (is_left_binding && !is_right_binding));

  return gut::function(
      [=](auto &&... args) constexpr->decltype(auto) {
        if constexpr (decltype(is_singular)::value)
          return combiner(unbound_left(std::forward<decltype(args)>(args)...));
        else if constexpr (decltype(is_left_binding)::value &&decltype(
                               is_right_binding)::value)
          return combiner(
              partition_arguments<0, remove_cvref_t<LeftArguments>::count()>(
                  unbound_left, std::forward<decltype(args)>(args)...),
              partition_arguments<remove_cvref_t<LeftArguments>::count(),
                                  sizeof...(args)>(
                  unbound_right, std::forward<decltype(args)>(args)...));
        else if constexpr (decltype(is_left_binding)::value)
          return combiner(
              partition_arguments<0, remove_cvref_t<LeftArguments>::count()>(
                  unbound_left, std::forward<decltype(args)>(args)...),
              partition_arguments<remove_cvref_t<LeftArguments>::count()>(
                  std::forward<decltype(args)>(args)...));
        else if constexpr (decltype(is_right_binding)::value)
          return combiner(
              partition_arguments<0>(std::forward<decltype(args)>(args)...),
              partition_arguments<1, sizeof...(args)>(
                  unbound_right, std::forward<decltype(args)>(args)...));
      },
      concatenate_arguments(std::forward<LeftArguments>(l),
                            std::forward<RightArguments>(r)));
}

#define GUT_COMMA ,

#define GUT_IMPL_BINOP_UNBOUND(op)                                             \
  constexpr decltype(auto) operator op(const arg &, const arg &) {             \
    return function([](auto &&left, auto &&right) constexpr->decltype(auto) {  \
      return std::forward<decltype(left)>(left)                                \
          op std::forward<decltype(right)>(right);                             \
    });                                                                        \
  }
#define GUT_IMPL_PREFIX_UNBOUND(op)                                            \
  constexpr decltype(auto) operator op(const arg &) {                          \
    return function([](auto &&bound) constexpr->decltype(auto) {               \
      return op std::forward<decltype(bound)>(bound);                          \
    });                                                                        \
  }
#define GUT_IMPL_POSTFIX_UNBOUND(op)                                           \
  constexpr decltype(auto) operator op(const arg &, int) {                     \
    return function([](auto &&bound) constexpr->decltype(auto) {               \
      return std::forward<decltype(bound)>(bound) op;                          \
    });                                                                        \
  }
#define GUT_IMPL_BINOP_LEFTBIND(op)                                            \
  template <typename Arg, typename>                                            \
  constexpr decltype(auto) operator op(Arg &&left, const arg &) {              \
    return (arg_v op arg_v)(std::forward<Arg>(left), arg_v);                   \
  }
#define GUT_IMPL_BINOP_RIGHTBIND(op)                                           \
  template <typename Arg, typename>                                            \
  constexpr decltype(auto) operator op(const arg &, Arg &&right) {             \
    return (arg_v op arg_v)(arg_v, std::forward<Arg>(right));                  \
  }
#define GUT_IMPL_BINOP_LEFTFUNCBIND(op)                                        \
  template <typename FunctionType, typename FunctionArity, typename Arg>       \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &func, Arg &&arg) {          \
    if constexpr (is_function_v<remove_cvref_t<Arg>>)                          \
      return combine_function((arg_v op arg_v), func.callable(),               \
                              arg.callable(), func.arguments(),                \
                              arg.arguments());                                \
    else                                                                       \
      return combine_function((arg_v op arg_v), func.callable(), something{},  \
                              func.arguments(), std::forward<Arg>(arg));       \
  }
#define GUT_IMPL_BINOP_RIGHTFUNCBIND(op)                                       \
  template <typename Arg, typename FunctionType, typename FunctionArity>       \
  constexpr decltype(auto) operator op(                                        \
      const Arg &arg, const function<FunctionType, FunctionArity> &func) {     \
    return combine_function((arg_v op arg_v), something{}, func.callable(),    \
                            arg, func.arguments());                            \
  }
#define GUT_IMPL_PREFIX_FUNCBIND(op)                                           \
  template <typename FunctionType, typename FunctionArity>                     \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &func) {                     \
    return combine_function(op arg_v, func, func.callable(), something{},      \
                            func.arguments(), something{});                    \
  }
#define GUT_IMPL_POSTFIX_FUNCBIND(op)                                          \
  template <typename FunctionType, typename FunctionArity>                     \
  constexpr decltype(auto) operator op(                                        \
      const function<FunctionType, FunctionArity> &func, int) {                \
    return combine_function(arg_v op, func, func.callable(), something{},      \
                            func.arguments(), something{});                    \
  }
#define GUT_IMPL_BINOP(op)                                                     \
  GUT_IMPL_BINOP_UNBOUND(op)                                                   \
  GUT_IMPL_BINOP_LEFTBIND(op)                                                  \
  GUT_IMPL_BINOP_RIGHTBIND(op)                                                 \
  GUT_IMPL_BINOP_LEFTFUNCBIND(op)                                              \
  GUT_IMPL_BINOP_RIGHTFUNCBIND(op)
#define GUT_IMPL_PREFIX(op)                                                    \
  GUT_IMPL_PREFIX_UNBOUND(op)                                                  \
  GUT_IMPL_PREFIX_FUNCBIND(op)
#define GUT_IMPL_POSTFIX(op)                                                   \
  GUT_IMPL_POSTFIX_UNBOUND(op)                                                 \
  GUT_IMPL_POSTFIX_FUNCBIND(op)

GUT_IMPL_POSTFIX(++)
GUT_IMPL_POSTFIX(--)
GUT_IMPL_BINOP(->*)
GUT_IMPL_PREFIX(++)
GUT_IMPL_PREFIX(--)
GUT_IMPL_PREFIX(+)
GUT_IMPL_PREFIX(-)
GUT_IMPL_PREFIX(!)
GUT_IMPL_PREFIX(~)
GUT_IMPL_PREFIX(*)
GUT_IMPL_PREFIX(&)
// new, arraynew, delete, arraydelete
GUT_IMPL_BINOP(*)
GUT_IMPL_BINOP(/)
GUT_IMPL_BINOP(%)
GUT_IMPL_BINOP(+)
GUT_IMPL_BINOP(-)
GUT_IMPL_BINOP(<<)
GUT_IMPL_BINOP(>>)
GUT_IMPL_BINOP(<)
GUT_IMPL_BINOP(<=)
GUT_IMPL_BINOP(>)
GUT_IMPL_BINOP(>=)
GUT_IMPL_BINOP(==)
GUT_IMPL_BINOP(!=)
GUT_IMPL_BINOP(&)
GUT_IMPL_BINOP(^)
GUT_IMPL_BINOP(|)
GUT_IMPL_BINOP(&&)
GUT_IMPL_BINOP(||)
GUT_IMPL_BINOP(*=)
GUT_IMPL_BINOP(/=)
GUT_IMPL_BINOP(%=)
GUT_IMPL_BINOP(+=)
GUT_IMPL_BINOP(-=)
GUT_IMPL_BINOP(<<=)
GUT_IMPL_BINOP(>>=)
GUT_IMPL_BINOP(&=)
GUT_IMPL_BINOP(^=)
GUT_IMPL_BINOP(|=)
GUT_IMPL_BINOP_UNBOUND(GUT_COMMA)
GUT_IMPL_BINOP_LEFTBIND(GUT_COMMA)
GUT_IMPL_BINOP_RIGHTBIND(GUT_COMMA)
GUT_IMPL_BINOP_LEFTFUNCBIND(GUT_COMMA)
GUT_IMPL_BINOP_RIGHTFUNCBIND(GUT_COMMA)

#undef GUT_IMPL_BINOP
#undef GUT_IMPL_PREFIX
#undef GUT_IMPL_POSTFIX

#undef GUT_IMPL_BINOP_UNBOUND
#undef GUT_IMPL_PREFIX_UNBOUND
#undef GUT_IMPL_POSTFIX_UNBOUND

#undef GUT_IMPL_BINOP_LEFTBIND
#undef GUT_IMPL_BINOP_RIGHTBIND
#undef GUT_IMPL_BINOP_LEFTFUNCBIND
#undef GUT_IMPL_BINOP_RIGHTFUNCBIND
#undef GUT_IMPL_PREFIX_FUNCBIND
#undef GUT_IMPL_POSTFIX_FUNCBIND

#undef GUT_COMMA
} // namespace gut

// Choice by type
// Auxiliary variables
/*
</textarea>
</details>
</td>
*/
