#pragma once

#include <climits>
#if __GNUG__
#include <cstddef>
#include <cstdint>
#endif
#include <limits>
#include <new>
#include <utility>

namespace gut {
namespace config {

inline constexpr bool std_supported = __cplusplus >= 201703L;
inline constexpr bool freestanding = __STDC_HOSTED__;

inline constexpr auto has_rtti =
#ifdef __cpp_rtti
    __cpp_rtti;
#else
    false;
#endif

inline constexpr auto has_exceptions =
#ifdef __cpp_exceptions
    __cpp_exceptions;
#else
    false;
#endif

namespace function {
inline constexpr std::intmax_t arity_deduction_limit =
#ifdef GUT_CONFIG_FUNCTION_ARITY_DEDUCTION_LIMIT
    GUT_CONFIG_FUNCTION_ARITY_DEDUCTION_LIMIT
#else
    256
#endif
    ;

inline constexpr std::intmax_t arity_deduction_expansion =
#ifdef GUT_CONFIG_FUNCTION_ARITY_DEDUCTION_EXPANSION
    GUT_CONFIG_FUNCTION_ARITY_DEDUCTION_EXPANSION
#else
    3
#endif
    ;
} // namespace function

namespace pattern {
inline constexpr std::intmax_t chain_max =
#ifdef GUT_CONFIG_PATTERN_CHAIN_MAX
    GUT_CONFIG_PATTERN_CHAIN_MAX
#else
    10
#endif
    ;

inline constexpr std::intmax_t choice_max =
#ifdef GUT_CONFIG_PATTERN_CHOICE_MAX
    GUT_CONFIG_PATTERN_CHOICE_MAX
#else
    chain_max
#endif
    ;

} // namespace pattern

} // namespace config

} // namespace gut
