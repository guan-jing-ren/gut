#include "type.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(type_v<decltype(type_v<int[1][2][3][4][5]>.get_extents())> ==
              type_v<indices<1, 2, 3, 4, 5>>);
static_assert(type_v<decltype(type_v<int[5]>.get_extents())> ==
              type_v<indices<5>>);
static_assert(type_v<decltype(type_v<int>.get_extents())> ==
              type_v<indices<-1>>);

static_assert(type_v<something> == type_v<return_type_t<int(), int>>);
static_assert(type_v<int> == type_v<return_type_t<int(int), int>>);
static_assert(type_v<something> == type_v<return_type_t<void(int), int>>);
} // namespace
