#include "ntuple.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

#include "buffer.hpp"

using namespace gut;

namespace {
static_assert(
    std::is_empty_v<ntuple<something, something, something, something>>);
static_assert(sizeof(ntuple<something, something, something, something>) ==
              4); // TODO: Investigate why size != 1;
static_assert(
    type_v<ntuple<something, something>>.is_derived_from<thing_empty<something>>());
static_assert(std::is_trivially_default_constructible_v<
                  ntuple<int, int, long, long long>>,
              "`ntuple` of trivially default constructible things should be "
              "trivially default constructible.");
static_assert(
    std::is_trivially_move_constructible_v<ntuple<int, int, long, long long>>,
    "`ntuple` of trivially move constructible things should be trivially move "
    "constructible.");
static_assert(
    std::is_trivially_copy_constructible_v<ntuple<int, int, long, long long>>,
    "`ntuple` of trivially copy constructible things should be trivially copy "
    "constructible.");
static_assert(
    std::is_trivially_move_assignable_v<ntuple<int, int, long, long long>>,
    "`ntuple` of trivially move assignable things should be trivially move "
    "assignable.");
static_assert(
    std::is_trivially_copy_assignable_v<ntuple<int, int, long, long long>>,
    "`ntuple` of trivially copy assignable things should be trivially copy "
    "assignable.");
static_assert(!std::is_trivially_default_constructible_v<
                  ntuple<int, int, long, long long &>>,
              "`ntuple` with reference members should not be trivially default "
              "constructible.");
static_assert(
    std::is_trivially_move_constructible_v<ntuple<int, int, long, long long &>>,
    "`ntuple` of references and trivially move constructible things  should be "
    "trivially move constructible.");
static_assert(
    std::is_trivially_copy_constructible_v<ntuple<int, int, long, long long &>>,
    "`ntuple` of references and trivially copy constructible things  should be "
    "trivially copy constructible.");
static_assert(
    std::is_trivially_move_assignable_v<ntuple<int, int, long, long long &>>,
    "`ntuple` of references and trivially move assignable things  should be "
    "trivially move assignable.");
static_assert(
    std::is_trivially_copy_assignable_v<ntuple<int, int, long, long long &>>,
    "`ntuple` of references and trivially copy assignable things  should be "
    "trivially copy assignable.");

constexpr ntuple test = []() constexpr {
  auto [x, y, z] = ntuple{1, 2, 3};
  buffer<int, 5> f = {4, 5, 6};
  ntuple test{x, y, z, f};
  ntuple<long, long, long, buffer<int, 5>> test_copy = std::move(test);
  test = ntuple{x, y, z, f};
  test_copy = test;
  test = ntuple{x, y, z, f};
  test_copy = std::move(test);
  ntuple<const int &, const int &, const int &> test_ref{x, y, z};
  test = ntuple{x, y, z, f};
  return test;
}
();
constexpr ntuple<long, long, long, buffer<int, 5>> test_copy = test;
static_assert(!is_ntuple_v<decltype(test)>,
              "Reference of ntuple is not an ntuple.");
static_assert(is_ntuple_v<std::decay_t<decltype(test)>>,
              "ntuple should be ntuple.");
static_assert(test.get<int>() == 1, "Unexpected value as specified.");
static_assert(test.get<0>() == 1, "Unexpected value as specified.");
static_assert(test.get<1>() == 2, "Unexpected value as specified.");
static_assert(test.get<2>() == 3, "Unexpected value as specified.");
static_assert(test.get<3>()[0] == 4, "Unexpected value as specified.");
static_assert(test.get<3>()[1] == 5, "Unexpected value as specified.");
static_assert(test.get<3>()[2] == 6, "Unexpected value as specified.");
static_assert(test.get<3>()[3] == 0, "Unexpected value as specified.");
static_assert(test.get<3>()[4] == 0, "Unexpected value as specified.");
static_assert(type_v<const int &> == type{test.get<0>()},
              "Expected const int & type for tuple member.");
static_assert(
    !std::is_default_constructible_v<ntuple<int, int &>>,
    "Expected tuple with reference member not to be default constructible.");
static_assert(
    std::is_rvalue_reference_v<
        decltype(ntuple<int &&>{std::declval<int &&>()}.get<0>())>,
    "Tuple member of rvalue-reference expected to be rvalue reference.");
static_assert(
    std::is_lvalue_reference_v<
        decltype(ntuple<int &>{std::declval<int &>()}.get<0>())>,
    "Tuple member of lreference-tuple expected to be lvalue reference.");
static_assert(type_v<const long &> == type{test_copy.get<0>()});
static_assert(test_copy.get<long>() == 1, "Unexpected value as specified.");
static_assert(test_copy.get<0>() == 1, "Unexpected value as specified.");
static_assert(test_copy.get<1>() == 2, "Unexpected value as specified.");
static_assert(test_copy.get<2>() == 3, "Unexpected value as specified.");
static_assert(test_copy.get<3>()[0] == 4, "Unexpected value as specified.");
static_assert(test_copy.get<3>()[1] == 5, "Unexpected value as specified.");
static_assert(test_copy.get<3>()[2] == 6, "Unexpected value as specified.");
static_assert(test_copy.get<3>()[3] == 0, "Unexpected value as specified.");
static_assert(test_copy.get<3>()[4] == 0, "Unexpected value as specified.");

constexpr ntuple mapped_tuple =
    []() {
      auto mapped =
          ntuple<int, buffer<int, 5>, long>{42, buffer{{97, 98, 99, 100, 101}},
                                            0b11'1111'1111}
              .map([](const auto &t) {
                if constexpr (type_v<decltype(t)> == type_v<const long &>) {
                  if (t == 0b11'1111'1111)
                    return 'z';
                  return 'a';
                }
              });
      mapped.each([](auto &&t) {
        if constexpr (type_v<decltype(t)> == type_v<char &>) {
          if (t == 'z')
            t = 'm';
        }
      });
      return mapped;
    } // namespace
();
static_assert(type_v<const something &> ==
              type_v<decltype(mapped_tuple.get(index_v<0>))>);
static_assert(type_v<const something &> ==
              type_v<decltype(mapped_tuple.get(index_v<1>))>);
static_assert(type_v<const char &> ==
              type_v<decltype(mapped_tuple.get(index_v<2>))>);
static_assert(mapped_tuple.get(index_v<2>) == 'm');
constexpr ntuple filtered_tuple = []() {
  ntuple<std::int8_t, std::uint8_t, std::int16_t, std::uint16_t, std::int32_t,
         std::uint32_t, std::int64_t, std::uint64_t>
      ints{};
  return ints.filter<std::is_signed>().map([](auto &&i) { return i; });
}();
static_assert(filtered_tuple.count() == 4);
static_assert(type_v<const std::int8_t &> ==
              type_v<decltype(filtered_tuple.get(index_v<0>))>);
static_assert(type_v<const std::int16_t &> ==
              type_v<decltype(filtered_tuple.get(index_v<1>))>);
static_assert(type_v<const std::int32_t &> ==
              type_v<decltype(filtered_tuple.get(index_v<2>))>);
static_assert(type_v<const std::int64_t &> ==
              type_v<decltype(filtered_tuple.get(index_v<3>))>);
constexpr ntuple func_tuple{
    0,
    [](auto &&t, auto &&) {
      return std::numeric_limits<remove_cvref_t<decltype(t)>>::max();
    },
    [](auto &&, auto &&u) {
      return std::numeric_limits<remove_cvref_t<decltype(u)>>::min();
    },
    '!'};
constexpr ntuple funcresult_tuple = func_tuple(short{0}, 0l);
static_assert(type_v<const something &> ==
              type_v<decltype(funcresult_tuple.get(index_v<0>))>);
static_assert(type_v<const something &> ==
              type_v<decltype(funcresult_tuple.get(index_v<3>))>);
static_assert(funcresult_tuple.get(index_v<1>) ==
              std::numeric_limits<short>::max());
static_assert(funcresult_tuple.get(index_v<2>) ==
              std::numeric_limits<long>::min());
constexpr ntuple iota{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
static_assert(iota.reduce([](auto &&... t) { return (... + t); }) ==
              (iota.back() + 1) * iota.back() / 2);
} // namespace
