#include "config.hpp"

static_assert(gut::config::std_supported, "GUT only supports C++17 or above.");
static_assert(!gut::config::has_rtti, "GUT must not use RTTI.");
static_assert(!gut::config::has_exceptions, "GUT must not use exceptions.");
