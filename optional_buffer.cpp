#include "optional_buffer.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
constexpr optional_buffer array_opt{type_v<int[200]>};
static_assert(!array_opt.value(index_v<0>));
static_assert([]() {
  optional_buffer array_opt{type_v<int[200]>};
  if (array_opt.value(50))
    return 1;
  array_opt.value(50, 42);
  if (!array_opt.value(50))
    return 1;
  if (array_opt.value(50).value() != 42)
    return 1;
  array_opt.value(50, 43);
  if (array_opt.value(50).value() != 43)
    return 1;
  array_opt.reset(50);
  if (array_opt.value(50))
    return 1;
  return 0;
}() == 0);
static_assert(std::is_trivially_copy_constructible_v<
              remove_cvref_t<decltype(array_opt)>>);
static_assert(std::is_trivially_move_constructible_v<
              remove_cvref_t<decltype(array_opt)>>);
static_assert(
    std::is_trivially_copy_assignable_v<remove_cvref_t<decltype(array_opt)>>);
static_assert(
    std::is_trivially_move_assignable_v<remove_cvref_t<decltype(array_opt)>>);
static_assert(
    std::is_trivially_destructible_v<remove_cvref_t<decltype(array_opt)>>);
} // namespace
