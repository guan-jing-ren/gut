#pragma once

#include "type.hpp"

namespace gut {
template <typename ValueType> struct optional;
template <typename... NonOptionalTypes>
inline constexpr constant is_optional_v = constant_v<false>;
template <typename ValueType>
inline constexpr constant is_optional_v<optional<ValueType>> = constant_v<true>;

struct optional_bool_base {
  constexpr optional_bool_base();
  constexpr optional_bool_base(const optional_bool_base &that) = default;
  constexpr optional_bool_base(optional_bool_base &&that) = default;
  constexpr optional_bool_base &
  operator=(const optional_bool_base &that) = default;
  constexpr optional_bool_base &operator=(optional_bool_base &&that) = default;

  constexpr optional_bool_base(bool that);
  constexpr decltype(auto) operator=(bool that);

  constexpr operator bool() const;

  constexpr bool value() const;

  constexpr void reset();

private:
  std::int8_t m_val = 0;
};

template <typename RefValueType> struct optional_ref_base {
  using value_type = RefValueType;
  using storage_type = std::remove_reference_t<RefValueType> *;

  constexpr optional_ref_base();
  constexpr optional_ref_base(const optional_ref_base &that) = default;
  constexpr optional_ref_base(optional_ref_base &&that) = default;
  constexpr optional_ref_base &
  operator=(const optional_ref_base &that) = default;
  constexpr optional_ref_base &operator=(optional_ref_base &&that) = default;

  constexpr optional_ref_base(value_type that);
  constexpr decltype(auto) operator=(value_type that);

  constexpr operator bool() const;

  constexpr value_type value() const;
  constexpr value_type value();

  constexpr void reset();

private:
  storage_type m_val = nullptr;
};

template <bool, typename ValueType> struct optional_val_destructor {
  using value_type = ValueType;

  constexpr optional_val_destructor() : m_something(), m_valid(false) {}
  constexpr optional_val_destructor(const optional_val_destructor &) = default;
  constexpr optional_val_destructor(optional_val_destructor &&) = default;
  constexpr optional_val_destructor &
  operator=(const optional_val_destructor &) = default;
  constexpr optional_val_destructor &
  operator=(optional_val_destructor &&) = default;

  constexpr optional_val_destructor(const remove_cvref_t<value_type> &that)
      : m_val(that), m_valid(true) {}
  constexpr optional_val_destructor(remove_cvref_t<value_type> &that)
      : m_val(that), m_valid(true) {}
  constexpr optional_val_destructor(remove_cvref_t<value_type> &&that)
      : m_val(std::move(that)), m_valid(true) {}

  constexpr void reset();

  union {
    something m_something = {};
    value_type m_val;
  };
  bool m_valid = false;
};
template <typename ValueType> struct optional_val_destructor<false, ValueType> {
  using value_type = ValueType;
  constexpr optional_val_destructor() : m_something(), m_valid(false) {}
  constexpr optional_val_destructor(const optional_val_destructor &) = default;
  constexpr optional_val_destructor(optional_val_destructor &&) = default;
  constexpr optional_val_destructor &
  operator=(const optional_val_destructor &) = default;
  constexpr optional_val_destructor &
  operator=(optional_val_destructor &&) = default;
  ~optional_val_destructor();

  constexpr optional_val_destructor(const remove_cvref_t<value_type> &that)
      : m_val(that), m_valid(true) {}
  constexpr optional_val_destructor(remove_cvref_t<value_type> &that)
      : m_val(that), m_valid(true) {}
  constexpr optional_val_destructor(remove_cvref_t<value_type> &&that)
      : m_val(std::move(that)), m_valid(true) {}

  constexpr void reset();

  union {
    something m_something = {};
    value_type m_val;
  };
  bool m_valid = false;
};
template <bool, bool TriviallyDestructible, typename ValueType>
struct optional_val_move_constructor
    : optional_val_destructor<TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type = optional_val_destructor<TriviallyDestructible, ValueType>;

  constexpr optional_val_move_constructor() = default;
  constexpr optional_val_move_constructor(
      const optional_val_move_constructor &) = default;
  constexpr optional_val_move_constructor(optional_val_move_constructor &&) =
      default;
  constexpr optional_val_move_constructor &
  operator=(const optional_val_move_constructor &) = default;
  constexpr optional_val_move_constructor &
  operator=(optional_val_move_constructor &&) = default;

  constexpr optional_val_move_constructor(
      const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_constructor(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_constructor(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool TriviallyDestructible, typename ValueType>
struct optional_val_move_constructor<false, TriviallyDestructible, ValueType>
    : optional_val_destructor<TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type = optional_val_destructor<TriviallyDestructible, ValueType>;

  constexpr optional_val_move_constructor() = default;
  constexpr optional_val_move_constructor(
      const optional_val_move_constructor &) = default;
  constexpr optional_val_move_constructor(
      optional_val_move_constructor &&that) {
    if (that.m_valid) {
      using construct_type = remove_cvref_t<value_type>;
      ::new (std::addressof(base_type::m_val))
          construct_type{std::move(that.m_val)};
      base_type::m_valid = true;
      that.m_val.~construct_type();
      that.m_valid = false;
    }
  }
  constexpr optional_val_move_constructor &
  operator=(const optional_val_move_constructor &) = default;
  constexpr optional_val_move_constructor &
  operator=(optional_val_move_constructor &&) = default;

  constexpr optional_val_move_constructor(
      const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_constructor(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_constructor(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool, bool TriviallyMoveConstructible, bool TriviallyDestructible,
          typename ValueType>
struct optional_val_move_assignment
    : optional_val_move_constructor<TriviallyMoveConstructible,
                                    TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type =
      optional_val_move_constructor<TriviallyMoveConstructible,
                                    TriviallyDestructible, ValueType>;

  constexpr optional_val_move_assignment() = default;
  constexpr optional_val_move_assignment(const optional_val_move_assignment &) =
      default;
  constexpr optional_val_move_assignment(optional_val_move_assignment &&) =
      default;
  constexpr optional_val_move_assignment &
  operator=(const optional_val_move_assignment &) = default;
  constexpr optional_val_move_assignment &
  operator=(optional_val_move_assignment &&) = default;

  constexpr optional_val_move_assignment(const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_assignment(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_assignment(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool TriviallyMoveConstructible, bool TriviallyDestructible,
          typename ValueType>
struct optional_val_move_assignment<false, TriviallyMoveConstructible,
                                    TriviallyDestructible, ValueType>
    : optional_val_move_constructor<TriviallyMoveConstructible,
                                    TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type =
      optional_val_move_constructor<TriviallyMoveConstructible,
                                    TriviallyDestructible, ValueType>;

  constexpr optional_val_move_assignment() = default;
  constexpr optional_val_move_assignment(const optional_val_move_assignment &) =
      default;
  constexpr optional_val_move_assignment(optional_val_move_assignment &&) =
      default;
  constexpr optional_val_move_assignment &
  operator=(const optional_val_move_assignment &) = default;
  constexpr optional_val_move_assignment &
  operator=(optional_val_move_assignment &&that) {
    using construct_type = remove_cvref_t<value_type>;
    if (base_type::m_valid) {
      if (that.m_valid) {
        base_type::m_val = std::move(that.m_val);
        base_type::m_valid = true;
      } else {
        base_type::m_val.~construct_type();
        base_type::m_valid = false;
      }
    } else if (that.m_valid) {
      ::new (std::addressof(base_type::m_val))
          construct_type{std::move(that.m_val)};
      base_type::m_valid = true;
    }
    return *this;
  }

  constexpr optional_val_move_assignment(const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_assignment(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_move_assignment(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool, bool TriviallyMoveAssignable, bool TriviallyMoveConstructible,
          bool TriviallyDestructible, typename ValueType>
struct optional_val_copy_constructor
    : optional_val_move_assignment<TriviallyMoveAssignable,
                                   TriviallyMoveConstructible,
                                   TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type =
      optional_val_move_assignment<TriviallyMoveAssignable,
                                   TriviallyMoveConstructible,
                                   TriviallyDestructible, ValueType>;

  constexpr optional_val_copy_constructor() = default;
  constexpr optional_val_copy_constructor(
      const optional_val_copy_constructor &) = default;
  constexpr optional_val_copy_constructor(optional_val_copy_constructor &&) =
      default;
  constexpr optional_val_copy_constructor &
  operator=(const optional_val_copy_constructor &) = default;
  constexpr optional_val_copy_constructor &
  operator=(optional_val_copy_constructor &&) = default;

  constexpr optional_val_copy_constructor(
      const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_constructor(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_constructor(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool TriviallyMoveAssignable, bool TriviallyMoveConstructible,
          bool TriviallyDestructible, typename ValueType>
struct optional_val_copy_constructor<false, TriviallyMoveAssignable,
                                     TriviallyMoveConstructible,
                                     TriviallyDestructible, ValueType>
    : optional_val_move_assignment<TriviallyMoveAssignable,
                                   TriviallyMoveConstructible,
                                   TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type =
      optional_val_move_assignment<TriviallyMoveAssignable,
                                   TriviallyMoveConstructible,
                                   TriviallyDestructible, ValueType>;

  constexpr optional_val_copy_constructor() = default;
  constexpr optional_val_copy_constructor(
      const optional_val_copy_constructor &that) {
    if (that.m_valid) {
      using construct_type = remove_cvref_t<value_type>;
      ::new (std::addressof(base_type::m_val)) construct_type{that.m_val};
      base_type::m_valid = true;
    }
  }
  constexpr optional_val_copy_constructor(optional_val_copy_constructor &&) =
      default;
  constexpr optional_val_copy_constructor &
  operator=(const optional_val_copy_constructor &) = default;
  constexpr optional_val_copy_constructor &
  operator=(optional_val_copy_constructor &&) = default;

  constexpr optional_val_copy_constructor(
      const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_constructor(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_constructor(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool, bool TriviallyCopyConstructible, bool TriviallyMoveAssignable,
          bool TriviallyMoveConstructible, bool TriviallyDestructible,
          typename ValueType>
struct optional_val_copy_assignment
    : optional_val_copy_constructor<
          TriviallyCopyConstructible, TriviallyMoveAssignable,
          TriviallyMoveConstructible, TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type = optional_val_copy_constructor<
      TriviallyCopyConstructible, TriviallyMoveAssignable,
      TriviallyMoveConstructible, TriviallyDestructible, ValueType>;

  constexpr optional_val_copy_assignment() = default;
  constexpr optional_val_copy_assignment(const optional_val_copy_assignment &) =
      default;
  constexpr optional_val_copy_assignment(optional_val_copy_assignment &&) =
      default;
  constexpr optional_val_copy_assignment &
  operator=(const optional_val_copy_assignment &) = default;
  constexpr optional_val_copy_assignment &
  operator=(optional_val_copy_assignment &&) = default;

  constexpr optional_val_copy_assignment(const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_assignment(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_assignment(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};
template <bool TriviallyCopyConstructible, bool TriviallyMoveAssignable,
          bool TriviallyMoveConstructible, bool TriviallyDestructible,
          typename ValueType>
struct optional_val_copy_assignment<
    false, TriviallyCopyConstructible, TriviallyMoveAssignable,
    TriviallyMoveConstructible, TriviallyDestructible, ValueType>
    : optional_val_copy_constructor<
          TriviallyCopyConstructible, TriviallyMoveAssignable,
          TriviallyMoveConstructible, TriviallyDestructible, ValueType> {
  using value_type = ValueType;
  using base_type = optional_val_copy_constructor<
      TriviallyCopyConstructible, TriviallyMoveAssignable,
      TriviallyMoveConstructible, TriviallyDestructible, ValueType>;

  constexpr optional_val_copy_assignment() = default;
  constexpr optional_val_copy_assignment(const optional_val_copy_assignment &) =
      default;
  constexpr optional_val_copy_assignment(optional_val_copy_assignment &&) =
      default;
  constexpr optional_val_copy_assignment &
  operator=(const optional_val_copy_assignment &that) {
    using construct_type = remove_cvref_t<value_type>;
    if (base_type::m_valid) {
      if (that.m_valid) {
        base_type::m_val = that.m_val;
        base_type::m_valid = true;
      } else {
        base_type::m_val.~construct_type();
        base_type::m_valid = false;
      }
    } else if (that.m_valid) {
      ::new (std::addressof(base_type::m_val)) construct_type{that.m_val};
      base_type::m_valid = true;
    }
    return *this;
  }
  constexpr optional_val_copy_assignment &
  operator=(optional_val_copy_assignment &&) = default;

  constexpr optional_val_copy_assignment(const remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_assignment(remove_cvref_t<value_type> &that)
      : base_type(that) {}
  constexpr optional_val_copy_assignment(remove_cvref_t<value_type> &&that)
      : base_type(std::move(that)) {}
};

template <typename ValueType>
struct optional_val_base
    : optional_val_copy_assignment<
          std::is_trivially_copy_assignable_v<ValueType>,
          std::is_trivially_copy_constructible_v<ValueType>,
          std::is_trivially_move_assignable_v<ValueType>,
          std::is_trivially_move_constructible_v<ValueType>,
          std::is_trivially_destructible_v<ValueType>, ValueType> {
  using value_type = ValueType;
  using base_type = optional_val_copy_assignment<
      std::is_trivially_copy_assignable_v<ValueType>,
      std::is_trivially_copy_constructible_v<ValueType>,
      std::is_trivially_move_assignable_v<ValueType>,
      std::is_trivially_move_constructible_v<ValueType>,
      std::is_trivially_destructible_v<ValueType>, ValueType>;

  constexpr optional_val_base() = default;
  constexpr optional_val_base(const optional_val_base &that) = default;
  constexpr optional_val_base(optional_val_base &&that) = default;
  optional_val_base &operator=(const optional_val_base &that) = default;
  optional_val_base &operator=(optional_val_base &&that) = default;

  template <typename ThatValueType>
  constexpr optional_val_base(const optional_val_base<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr optional_val_base(optional_val_base<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr optional_val_base(optional_val_base<ThatValueType> &&that);
  template <typename ThatValueType>
  constexpr decltype(auto)
  operator=(const optional_val_base<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(optional_val_base<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(optional_val_base<ThatValueType> &&that);

  constexpr optional_val_base(const value_type &that);
  constexpr optional_val_base(value_type &&that);
  constexpr decltype(auto) operator=(const value_type &that);
  constexpr decltype(auto) operator=(value_type &&that);

  constexpr operator bool() const;

  constexpr const value_type &value() const;
  constexpr value_type &value();

private:
  template <typename ThatType>
  constexpr void assign(bool that_valid, ThatType &&that);
};

template <typename ValueType>
using optional_base =
    std::conditional_t<type_v<bool> == type_v<ValueType>, optional_bool_base,
                       std::conditional_t<std::is_reference_v<ValueType>,
                                          optional_ref_base<ValueType>,
                                          optional_val_base<ValueType>>>;

template <typename ValueType> struct optional : optional_base<ValueType> {
  using value_type = ValueType;
  using optional_base = gut::optional_base<ValueType>;

  constexpr static decltype(auto) value_type_v();

  constexpr optional() = default;
  constexpr optional(const optional &that) = default;
  constexpr optional(optional &&that) = default;
  constexpr optional &operator=(const optional &that) = default;
  constexpr optional &operator=(optional &&that) = default;

  using optional_base::optional_base;
  using optional_base::operator=;

  using optional_base::operator bool;

  using optional_base::value;

  template <index_t Index>
  constexpr decltype(auto) get(index<Index> = {}) const;
  template <index_t Index> constexpr decltype(auto) get(index<Index> = {});

  using optional_base::reset;

  template <typename Map> constexpr decltype(auto) map(Map &&m) const;
  template <typename Map> constexpr decltype(auto) map(Map &&m);

  template <typename Each> constexpr decltype(auto) each(Each &&e) const;
  template <typename Each> constexpr decltype(auto) each(Each &&e);

  template <template <typename> typename Filter>
  constexpr decltype(auto) filter() const;
  template <template <typename> typename Filter>
  constexpr decltype(auto) filter();

  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args) const;
  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args);

  template <typename Reduce> constexpr decltype(auto) reduce(Reduce &&r) const;
  template <typename Reduce> constexpr decltype(auto) reduce(Reduce &&r);
};

template <typename ValueType,
          typename = std::enable_if_t<!is_optional_v<
              typename decltype(type_v<ValueType>.remove_cvref())::raw_type>>>
optional(ValueType &&)->optional<remove_cvref_t<ValueType>>;

template <typename ValueType>
optional(const optional<ValueType> &)->optional<ValueType>;
template <typename ValueType>
optional(optional<ValueType> &&)->optional<ValueType>;

////////////////////
// IMPLEMENTATION //
////////////////////

constexpr optional_bool_base::optional_bool_base() : m_val(0) {}

constexpr optional_bool_base::optional_bool_base(bool that)
    : m_val(that ? 1 : -1) {}

constexpr decltype(auto) optional_bool_base::operator=(bool that) {
  m_val = that ? 1 : -1;
  return *this;
}

constexpr optional_bool_base::operator bool() const { return m_val != 0; }

constexpr bool optional_bool_base::value() const { return m_val > 0; }

constexpr void optional_bool_base::reset() { m_val = 0; }

template <typename RefValueType>
constexpr optional_ref_base<RefValueType>::optional_ref_base()
    : m_val(nullptr) {}

template <typename RefValueType>
constexpr optional_ref_base<RefValueType>::optional_ref_base(value_type that)
    : m_val(std::addressof(that)) {}

template <typename RefValueType>
constexpr decltype(auto)
optional_ref_base<RefValueType>::operator=(value_type that) {
  m_val = &that;
  return *this;
}

template <typename RefValueType>
constexpr optional_ref_base<RefValueType>::operator bool() const {
  return m_val;
}

template <typename RefValueType>
constexpr auto optional_ref_base<RefValueType>::value() const -> value_type {
  return static_cast<const value_type>(*m_val);
}

template <typename RefValueType>
constexpr auto optional_ref_base<RefValueType>::value() -> value_type {
  return static_cast<value_type>(*m_val);
}

template <typename RefValueType>
constexpr void optional_ref_base<RefValueType>::reset() {
  m_val = nullptr;
}

template <typename ValueType>
optional_val_destructor<false, ValueType>::~optional_val_destructor() {
  reset();
}

template <typename ValueType>
template <typename ThatValueType>
constexpr optional_val_base<ValueType>::optional_val_base(
    const optional_val_base<ThatValueType> &that)
    : base_type(that ? that.value() : value_type{}, true) {
  if (!that)
    base_type::reset();
}

template <typename ValueType>
template <typename ThatValueType>
constexpr optional_val_base<ValueType>::optional_val_base(
    optional_val_base<ThatValueType> &that)
    : base_type(that ? that.value() : value_type{}, true) {
  if (!that)
    base_type::reset();
}

template <typename ValueType>
template <typename ThatValueType>
constexpr optional_val_base<ValueType>::optional_val_base(
    optional_val_base<ThatValueType> &&that)
    : base_type(that ? that.value() : value_type{}, true) {
  if (!that)
    base_type::reset();
}

template <typename ValueType>
template <typename ThatType>
constexpr void optional_val_base<ValueType>::assign(bool that_valid,
                                                    ThatType &&that) {
  if (!that_valid) {
    base_type::reset();
    return;
  }

  bool reconstruct = false;
  if constexpr (std::is_trivially_copy_assignable_v<optional_val_base> ||
                std::is_trivially_move_assignable_v<optional_val_base>)
    operator=(optional_val_base{std::forward<ThatType>(that)});
  else if constexpr (std::is_assignable_v<value_type &, ThatType>) {
    if (operator bool())
      base_type::m_val = std::forward<ThatType>(that);
    else
      reconstruct = true;
  } else
    reconstruct = true;

  if (reconstruct) {
    base_type::reset();
    new (std::addressof(base_type::m_val))
        value_type{std::forward<ThatType>(that)};
  }

  base_type::m_valid = true;
}

template <typename ValueType>
template <typename ThatValueType>
constexpr decltype(auto) optional_val_base<ValueType>::operator=(
    const optional_val_base<ThatValueType> &that) {
  assign(that, that.value());
  return *this;
}

template <typename ValueType>
template <typename ThatValueType>
constexpr decltype(auto) optional_val_base<ValueType>::operator=(
    optional_val_base<ThatValueType> &that) {
  assign(that, that.value());
  return *this;
}

template <typename ValueType>
template <typename ThatValueType>
constexpr decltype(auto) optional_val_base<ValueType>::operator=(
    optional_val_base<ThatValueType> &&that) {
  assign(that, std::move(that.value()));
  return *this;
}

template <typename ValueType>
constexpr optional_val_base<ValueType>::optional_val_base(
    const value_type &that)
    : base_type(that) {
  base_type::m_valid = true;
}

template <typename ValueType>
constexpr optional_val_base<ValueType>::optional_val_base(value_type &&that)
    : base_type(std::move(that)) {
  base_type::m_valid = true;
}

template <typename ValueType>
constexpr decltype(auto)
optional_val_base<ValueType>::operator=(const value_type &that) {
  assign(true, that);
  return *this;
}

template <typename ValueType>
constexpr decltype(auto)
optional_val_base<ValueType>::operator=(value_type &&that) {
  assign(true, std::move(that));
  return *this;
}

template <typename ValueType>
constexpr optional_val_base<ValueType>::operator bool() const {
  return base_type::m_valid;
}

template <typename ValueType>
constexpr auto optional_val_base<ValueType>::value() const
    -> const value_type & {
  return base_type::m_val;
}

template <typename ValueType>
constexpr auto optional_val_base<ValueType>::value() -> value_type & {
  return base_type::m_val;
}

template <bool TriviallyDestructible, typename ValueType>
constexpr void
optional_val_destructor<TriviallyDestructible, ValueType>::reset() {
  if (m_valid) {
    *this = {};
  }
}

template <typename ValueType>
constexpr void optional_val_destructor<false, ValueType>::reset() {
  if (m_valid) {
    m_val.~value_type();
    m_something = {};
    m_valid = false;
  }
}

template <typename ValueType>
constexpr decltype(auto) optional<ValueType>::value_type_v() {
  return type_v<value_type>;
}

template <typename ValueType>
template <index_t Index>
constexpr decltype(auto) optional<ValueType>::get(index<Index>) const {
  if constexpr (Index == 0)
    return this->operator bool();
  else if constexpr (type_v<bool> == type_v<ValueType>)
    return optional_base::value();
  else
    return this->operator bool() ? &optional_base::value() : nullptr;
}

template <typename ValueType>
template <index_t Index>
constexpr decltype(auto) optional<ValueType>::get(index<Index>) {
  if constexpr (Index == 0)
    return this->operator bool();
  else if constexpr (type_v<bool> == type_v<ValueType>)
    return optional_base::value();
  else
    return this->operator bool() ? &optional_base::value() : nullptr;
}

template <typename ValueType>
template <typename Map>
constexpr decltype(auto) optional<ValueType>::map(Map &&m) const {
  using return_type = gut::optional<return_type_t<Map, value_type>>;
  if (this->operator bool())
    return return_type{call(m, value())};
  else
    return return_type{};
}

template <typename ValueType>
template <typename Map>
constexpr decltype(auto) optional<ValueType>::map(Map &&m) {
  using return_type = gut::optional<return_type_t<Map, value_type>>;
  if (this->operator bool())
    return return_type{call(m, value())};
  else
    return return_type{};
}

template <typename ValueType>
template <typename Each>
constexpr decltype(auto) optional<ValueType>::each(Each &&e) const {
  if (this->operator bool())
    call(e, value());
  return *this;
}

template <typename ValueType>
template <typename Each>
constexpr decltype(auto) optional<ValueType>::each(Each &&e) {
  if (this->operator bool())
    call(e, value());
  return *this;
}

template <typename ValueType>
template <template <typename> typename>
constexpr decltype(auto) optional<ValueType>::filter() const {
  return *this;
}

template <typename ValueType>
template <template <typename> typename>
constexpr decltype(auto) optional<ValueType>::filter() {
  return *this;
}

template <typename ValueType>
template <typename... Args>
constexpr decltype(auto)
optional<ValueType>::operator()(Args &&... args) const {
  if constexpr (is_callable_v<decltype(value()), Args...>) {
    return map(
        [&args...](auto &&o) { return call(o, std::forward<Args>(args)...); });
  } else
    return something{};
}

template <typename ValueType>
template <typename... Args>
constexpr decltype(auto) optional<ValueType>::operator()(Args &&... args) {
  if constexpr (is_callable_v<decltype(value()), Args...>) {
    return map(
        [&args...](auto &&o) { return call(o, std::forward<Args>(args)...); });
  } else
    return something{};
}

template <typename ValueType>
template <typename Reduce>
constexpr decltype(auto) optional<ValueType>::reduce(Reduce &&r) const {
  return map(r);
}

template <typename ValueType>
template <typename Reduce>
constexpr decltype(auto) optional<ValueType>::reduce(Reduce &&r) {
  return map(r);
}
} // namespace gut

template <typename ValueType>
class std::tuple_size<gut::optional<ValueType>> : public gut::constant<2> {};

template <typename ValueType>
class std::tuple_element<0, gut::optional<ValueType>> {
public:
  using type = bool;
};

template <typename ValueType>
class std::tuple_element<1, gut::optional<ValueType>> {
public:
  using type =
      std::conditional_t<gut::type_v<bool> == gut::type_v<ValueType>, bool,
                         decltype(&gut::optional<ValueType>{}.value())>;
};
