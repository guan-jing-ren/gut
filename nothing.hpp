#pragma once

#include "constant.hpp"

namespace gut {
struct nothing;

template <typename Nothing, typename... Tail>
inline constexpr constant is_nothing_v =
    constant_v<std::is_same_v<nothing, Nothing> && sizeof...(Tail) == 0>;

struct nothing final {
  nothing() = delete;
  nothing(const nothing &) = delete;
  nothing(nothing &&) = delete;
  nothing &operator=(const nothing &) = delete;
  nothing &operator=(nothing &&) = delete;
  ~nothing() = delete;
};
} // namespace gut
