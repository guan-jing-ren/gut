#include "nothing.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(!std::is_default_constructible_v<nothing>,
              "Special `nothing` must not be created.");
static_assert(!std::is_copy_constructible_v<nothing>,
              "Special `nothing` must not be copied.");
static_assert(!std::is_move_constructible_v<nothing>,
              "Special `nothing` must not be moved.");
static_assert(!std::is_copy_assignable_v<nothing>,
              "Special `nothing` must not be copy assigned.");
static_assert(!std::is_move_assignable_v<nothing>,
              "Special `nothing` must not be move assigned.");
static_assert(!std::is_destructible_v<nothing>,
              "Special `nothing` must not be deleted.");
} // namespace
