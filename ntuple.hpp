#pragma once

#include "thing.hpp"
#include "type_list.hpp"

namespace gut {
template <typename IndexedMemberType>
struct ntuple_member : private IndexedMemberType,
                       private thing<typename IndexedMemberType::raw_type> {
  using raw_type = typename IndexedMemberType::raw_type;
  using tuple_member_base = thing<raw_type>;
  using tuple_member_base::tuple_member_base;
  using tuple_member_base::operator=;
  using tuple_member_base::get;
  constexpr ntuple_member() = default;
  constexpr ntuple_member(const ntuple_member &that) = default;
  constexpr ntuple_member(ntuple_member &&that) = default;
  constexpr ntuple_member &operator=(const ntuple_member &that) = default;
  constexpr ntuple_member &operator=(ntuple_member &&that) = default;

private:
  template <typename...> friend struct type_list;
};

template <typename... MemberTypes>
using tuple_base = typename decltype(
    make_type_list<MemberTypes...>())::template decorated<ntuple_member>;

template <typename... MemberTypes> struct ntuple;
template <typename... CandidateTypes>
inline constexpr constant is_ntuple_v = constant_v<false>;
template <typename... MemberTypes>
inline constexpr constant is_ntuple_v<ntuple<MemberTypes...>> =
    constant_v<true>;

template <typename... MemberTypes> struct ntuple : tuple_base<MemberTypes...> {
  using tuple_base = gut::tuple_base<MemberTypes...>;
  constexpr static decltype(auto) member_types_v();
  using member_types = decltype(member_types_v());
  template <index_t Index>
  using type_at =
      typename decltype(get_type<Index>(member_types_v()))::raw_type;

  using tuple_base::tuple_base;
  using tuple_base::operator=;
  using tuple_base::count;

  constexpr ntuple() = default;
  constexpr ntuple(const ntuple &that) = default;
  constexpr ntuple(ntuple &&that) = default;
  constexpr ntuple &operator=(const ntuple &that) = default;
  constexpr ntuple &operator=(ntuple &&that) = default;

  template <index_t MemberIndex>
  constexpr decltype(auto) get(index<MemberIndex> = {}) const;
  template <index_t MemberIndex>
  constexpr decltype(auto) get(index<MemberIndex> = {});
  template <typename MemberType>
  constexpr decltype(auto) get(type<MemberType> = {}) const;
  template <typename MemberType>
  constexpr decltype(auto) get(type<MemberType> = {});

  constexpr decltype(auto) front() const;
  constexpr decltype(auto) front();
  constexpr decltype(auto) back() const;
  constexpr decltype(auto) back();

  template <typename Map> constexpr decltype(auto) map(Map &&m) const;
  template <typename Map> constexpr decltype(auto) map(Map &&m);

  template <typename Each> constexpr decltype(auto) each(Each &&e) const;
  template <typename Each> constexpr decltype(auto) each(Each &&e);

  template <template <typename> typename Filter>
  constexpr decltype(auto) filter() const;
  template <template <typename> typename Filter>
  constexpr decltype(auto) filter();

  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args) const;
  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args);

  template <typename Reduce> constexpr decltype(auto) reduce(Reduce &&r) const;
  template <typename Reduce> constexpr decltype(auto) reduce(Reduce &&r);
};

template <typename... MemberTypes,
          std::enable_if_t<
              !is_ntuple_v<std::remove_reference_t<MemberTypes>...>, int> = 0>
ntuple(MemberTypes &&...)->ntuple<remove_cvref_t<MemberTypes>...>;

template <typename... MemberTypes>
ntuple(const ntuple<MemberTypes...> &)->ntuple<MemberTypes...>;
template <typename... MemberTypes>
ntuple(ntuple<MemberTypes...> &&)->ntuple<MemberTypes...>;

template <> struct ntuple<> {
  constexpr static decltype(auto) count();

  template <index_t MemberIndex>
  constexpr decltype(auto) get(index<MemberIndex> = {}) const;
  template <typename MemberType>
  constexpr decltype(auto) get(type<MemberType> = {}) const;

  constexpr decltype(auto) front() const;
  constexpr decltype(auto) back() const;

  template <typename Map> constexpr decltype(auto) map(Map &&m) const;

  template <typename Each> constexpr decltype(auto) each(Each &&e) const;

  template <template <typename> typename Filter>
  constexpr decltype(auto) filter() const;

  template <typename... Args>
  constexpr decltype(auto) operator()(Args &&... args) const;

  template <typename Reduce> constexpr decltype(auto) reduce(Reduce &&r) const;
};

template <typename... ForwardedMemberTypes>
struct forwarding_ntuple : ntuple<ForwardedMemberTypes...> {
  using ntuple_base = ntuple<ForwardedMemberTypes...>;
  using ntuple_base::ntuple_base;
};
template <typename... ForwardedMemberTypes>
forwarding_ntuple(ForwardedMemberTypes &&...)
    ->forwarding_ntuple<ForwardedMemberTypes...>;

////////////////////
// IMPLEMENTATION //
////////////////////

template <typename... MemberTypes>
constexpr decltype(auto) ntuple<MemberTypes...>::member_types_v() {
  return make_type_list<MemberTypes...>();
}

template <typename... MemberTypes>
template <index_t MemberIndex>
constexpr decltype(auto) ntuple<MemberTypes...>::get(index<MemberIndex>) const {
  return static_cast<const ntuple_member<
      indexed_type<MemberIndex, type_at<MemberIndex>>> &>(*this)
      .get();
}

template <typename... MemberTypes>
template <index_t MemberIndex>
constexpr decltype(auto) ntuple<MemberTypes...>::get(index<MemberIndex>) {
  return static_cast<
             ntuple_member<indexed_type<MemberIndex, type_at<MemberIndex>>> &>(
             *this)
      .get();
}

template <typename... MemberTypes>
template <typename MemberType>
constexpr decltype(auto) ntuple<MemberTypes...>::get(type<MemberType>) const {
  return static_cast<const ntuple_member<indexed_type<
      get_indices<MemberType>(make_type_list<MemberTypes...>())[index_v<0>],
      MemberType>> &>(*this)
      .get();
}

template <typename... MemberTypes>
template <typename MemberType>
constexpr decltype(auto) ntuple<MemberTypes...>::get(type<MemberType>) {
  return static_cast<ntuple_member<indexed_type<
      get_indices<MemberType>(make_type_list<MemberTypes...>())[index_v<0>],
      MemberType>> &>(*this)
      .get();
}

template <typename... MemberTypes>
constexpr decltype(auto) ntuple<MemberTypes...>::front() const {
  return get(index_v<0>);
}

template <typename... MemberTypes>
constexpr decltype(auto) ntuple<MemberTypes...>::front() {
  return get(index_v<0>);
}

template <typename... MemberTypes>
constexpr decltype(auto) ntuple<MemberTypes...>::back() const {
  return get(index_v<count() - 1>);
}

template <typename... MemberTypes>
constexpr decltype(auto) ntuple<MemberTypes...>::back() {
  return get(index_v<count() - 1>);
}

template <typename... MemberTypes>
template <typename Map>
constexpr decltype(auto) ntuple<MemberTypes...>::map(Map &&m) const {
  return make_index_list<count()>().reduce(
      [ this, &m ](auto... i) mutable constexpr->decltype(auto) {
        return gut::ntuple{call(m, this->get(i))...};
      });
}

template <typename... MemberTypes>
template <typename Map>
constexpr decltype(auto) ntuple<MemberTypes...>::map(Map &&m) {
  return make_index_list<count()>().reduce(
      [ this, &m ](auto... i) mutable constexpr->decltype(auto) {
        return gut::ntuple{call(m, this->get(i))...};
      });
}

template <typename... MemberTypes>
template <typename Each>
constexpr decltype(auto) ntuple<MemberTypes...>::each(Each &&e) const {
  map(e);
  return *this;
}

template <typename... MemberTypes>
template <typename Each>
constexpr decltype(auto) ntuple<MemberTypes...>::each(Each &&e) {
  map(e);
  return *this;
}

template <typename... MemberTypes>
template <template <typename> typename Filter>
constexpr decltype(auto) ntuple<MemberTypes...>::filter() const {
  return match_indices<Filter>(make_type_list<MemberTypes...>())
      .reduce([this](auto... i) mutable constexpr->decltype(auto) {
        return gut::ntuple<decltype(this->get(i))...>{this->get(i)...};
      });
}

template <typename... MemberTypes>
template <template <typename> typename Filter>
constexpr decltype(auto) ntuple<MemberTypes...>::filter() {
  return match_indices<Filter>(make_type_list<MemberTypes...>())
      .reduce([this](auto... i) mutable constexpr->decltype(auto) {
        return gut::ntuple<decltype(this->get(i))...>{this->get(i)...};
      });
}

template <typename... MemberTypes>
template <typename... Args>
constexpr decltype(auto)
ntuple<MemberTypes...>::operator()(Args &&... args) const {
  return map([&args...](auto &&t) mutable constexpr->decltype(auto) {
    if constexpr (is_callable_v<decltype(t), decltype(args)...>)
      return t(args...);
  });
}

template <typename... MemberTypes>
template <typename... Args>
constexpr decltype(auto) ntuple<MemberTypes...>::operator()(Args &&... args) {
  return map([&args...](auto &&t) mutable constexpr->decltype(auto) {
    if constexpr (is_callable_v<decltype(t), decltype(args)...>)
      return t(args...);
  });
}

template <typename... MemberTypes>
template <typename Reduce>
constexpr decltype(auto) ntuple<MemberTypes...>::reduce(Reduce &&r) const {
  return make_index_list<count()>().reduce(
      [ this, &r ](auto... i) mutable constexpr->decltype(auto) {
        return r(this->get(i)...);
      });
}

template <typename... MemberTypes>
template <typename Reduce>
constexpr decltype(auto) ntuple<MemberTypes...>::reduce(Reduce &&r) {
  return make_index_list<count()>().reduce(
      [ this, &r ](auto... i) mutable constexpr->decltype(auto) {
        return r(this->get(i)...);
      });
}

constexpr decltype(auto) ntuple<>::count() { return count_v<0>; }

template <index_t MemberIndex>
constexpr decltype(auto) ntuple<>::get(index<MemberIndex>) const {
  return something{};
}

template <typename MemberType>
constexpr decltype(auto) ntuple<>::get(type<MemberType>) const {
  return something{};
}

constexpr decltype(auto) ntuple<>::front() const { return get(index_v<0>); }

constexpr decltype(auto) ntuple<>::back() const {
  return get(index_v<count() - 1>);
}

template <typename Map> constexpr decltype(auto) ntuple<>::map(Map &&m) const {
  return m();
}

template <typename Each>
constexpr decltype(auto) ntuple<>::each(Each &&e) const {
  map(e);
  return *this;
}

template <template <typename> typename Filter>
constexpr decltype(auto) ntuple<>::filter() const {
  return *this;
}

template <typename... Args>
constexpr decltype(auto) ntuple<>::operator()(Args &&...) const {
  return *this;
}

template <typename Reduce>
constexpr decltype(auto) ntuple<>::reduce(Reduce &&r) const {
  return r();
}
} // namespace gut

template <typename... MemberTypes>
class std::tuple_size<gut::ntuple<MemberTypes...>>
    : public gut::constant<sizeof...(MemberTypes)> {};

template <std::size_t MemberIndex, typename... MemberTypes>
class std::tuple_element<MemberIndex, gut::ntuple<MemberTypes...>> {
public:
  using type =
      typename gut::ntuple<MemberTypes...>::template type_at<MemberIndex>;
};
