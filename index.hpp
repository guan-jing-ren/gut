#pragma once

#include "constant.hpp"

namespace gut {
using index_t = std::intmax_t;

template <index_t> struct index;

template <typename...> inline constexpr constant is_index_v = constant_v<false>;
template <index_t Index>
inline constexpr constant is_index_v<index<Index>> = constant_v<true>;

template <index_t Index = 0> struct index {
  constexpr static index_t value();

  static_assert(value() >= -1, "Indexes should be non-negative, `-1` only for "
                               "fold expression purposes.");

  constexpr index() = default;
  constexpr index(const index &that) = default;
  constexpr index(index &&that) = default;
  constexpr index &operator=(const index &that) = default;
  constexpr index &operator=(index &&that) = default;

  constexpr operator index_t() const { return Index; }
};

template <index_t Index> constexpr index<Index> index_v = index<Index>{};

template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator==(index<IndexL> l, index<IndexR> r);
template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator!=(index<IndexL> l, index<IndexR> r);
template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator<(index<IndexL> l, index<IndexR> r);

template <index_t Index> constexpr index_t index<Index>::value() {
  return Index;
}

template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator==(index<IndexL> l, index<IndexR> r) {
  return constant_v<l.value() == r.value()>;
}

template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator!=(index<IndexL> l, index<IndexR> r) {
  return constant_v<!(l == r)>;
}

template <index_t IndexL, index_t IndexR>
constexpr decltype(auto) operator<(index<IndexL> l, index<IndexR> r) {
  return constant_v<l.value() < r.value()>;
}
} // namespace gut
