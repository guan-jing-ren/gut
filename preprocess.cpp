#include "preprocess.hpp"

#define BOOL_FALSE false
#define BOOL_TRUE true

static_assert(GUT_PP_CONCAT(BOOL_, GUT_PP_ISEMPTY()));
static_assert(!GUT_PP_CONCAT(BOOL_, GUT_PP_ISEMPTY(a)));
static_assert(GUT_PP_CONCAT(BOOL_, GUT_PP_ISEMPTY()));
static_assert(!GUT_PP_CONCAT(BOOL_, GUT_PP_ISEMPTY(a, , , , )));

#undef BOOL_FALSE
#undef BOOL_TRUE

static_assert(GUT_PP_IF(GUT_PP_ISEMPTY(), true, false));
static_assert(!GUT_PP_IF(GUT_PP_ISEMPTY(a), true, false));
static_assert(!GUT_PP_IF(GUT_PP_ISEMPTY(a, b), true, false));

static_assert(GUT_PP_ARG_COUNT() == 0);
static_assert(GUT_PP_ARG_COUNT(a) == 1);
static_assert(GUT_PP_ARG_COUNT(a, b) == 2);
