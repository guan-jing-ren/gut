#include "bitfield.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
constexpr bitfield<8, 5, 2, 0> bits{};
static_assert(
    std::is_trivially_default_constructible_v<remove_cvref_t<decltype(bits)>>);
static_assert(
    std::is_trivially_copy_constructible_v<remove_cvref_t<decltype(bits)>>);
static_assert(
    std::is_trivially_move_constructible_v<remove_cvref_t<decltype(bits)>>);
static_assert(
    std::is_trivially_copy_assignable_v<remove_cvref_t<decltype(bits)>>);
static_assert(
    std::is_trivially_move_assignable_v<remove_cvref_t<decltype(bits)>>);
static_assert(std::is_trivially_destructible_v<remove_cvref_t<decltype(bits)>>);
static_assert(bits.count() == 3);
static_assert(bits.min_offset() == 0);
static_assert(bits.max_offset() == 8);
static_assert(sizeof(bits) == 1);
static_assert(type_v<std::uint8_t> ==
              type_v<decltype(bits[index_v<0>].value())>);
static_assert([]() {
  auto new_bits = bits;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<1>] = 0b0010;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<0>] = 0b1;
  return new_bits;
}()[index_v<1>] == 0b101);
static_assert([]() {
  auto new_bits = bits;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<1>] = 0b0010;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<0>] = 0b1u;
  return new_bits;
}()[index_v<0>] == 1u);
static_assert([]() {
  auto new_bits = bits;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<1>] = 0b0010;
  new_bits[index_v<1>] = 0b1101;
  new_bits[index_v<2>] = 0b1;
  if (new_bits[index_v<1>] == 0b101 && new_bits[index_v<0>] == 1)
    new_bits[index_v<2>] = 0;
  return new_bits;
}()[index_v<2>] == 1);
static_assert([]() {
  bitfield<16, 10, 3, 0> new_bits{};
  static_assert(sizeof(new_bits) == 2);
  new_bits[index_v<1>] = 0b1010101;
  return new_bits;
}()[index_v<1>] == 0b1010101);
static_assert([]() {
  bitfield<31, 21, 8, 0> new_bits{};
  static_assert(sizeof(new_bits) == 4);
  new_bits[index_v<1>] = 0b101010101010;
  return new_bits;
}()[index_v<1>] == 0b101010101010);
static_assert([]() {
  bitfield<31, 21, 8, 0> new_bits{};
  static_assert(sizeof(new_bits) == 4);
  new_bits[index_v<2>] = 0b10101010;
  new_bits[index_v<1>] = 0b101010101010;
  return new_bits;
}()[index_v<2>] == 0b10101010);
static_assert([]() {
  bitfield<31, 21, 8, 0> new_bits{};
  static_assert(sizeof(new_bits) == 4);
  new_bits[index_v<0>] = 0b1000000000;
  new_bits[index_v<1>] = 0b101010101010;
  return new_bits;
}()[index_v<0>] == 0b1000000000);
static_assert([]() {
  bitfield<31, 21, 9, 0> new_bits{};
  static_assert(sizeof(new_bits) == 4);
  new_bits[index_v<0>] = 0b0010'0000'0000;
  new_bits[index_v<1>] = 0b0101'0101'0101;
  new_bits[index_v<2>] = 0b0001'1010'1101;
  static_assert(sizeof(new_bits.cast<32, 0>()) == sizeof(std::uint32_t *));
  return new_bits;
}()
                  .cast<32, 0>()[index_v<0>] ==
              0b0100'0000'0000'1010'1010'1011'1010'1101);
constexpr bitfield<8, 5, 2, 0> more_bits{1, 2, 3};
static_assert(more_bits[index_v<0>] == 1);
static_assert(more_bits[index_v<1>] == 2);
static_assert(more_bits[index_v<2>] == 3);
constexpr bitfield bitset = []() {
  bitfield<15, 0> bitset{};
  bitset[8] = true;
  bitset[9] = true;
  bitset[9] = false;
  return bitset;
}();
static_assert(!bitset[7]);
static_assert(!bitset[10]);
static_assert(bitset[8]);
static_assert(!bitset[9]);
} // namespace
