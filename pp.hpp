#pragma once
#include "config.hpp"
#define GUT_PP_WHILE_64(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 65, __VA_ARGS__)      \
                GUT_PP_WHILE_65(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_63(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 64, __VA_ARGS__)      \
                GUT_PP_WHILE_64(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_62(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 63, __VA_ARGS__)      \
                GUT_PP_WHILE_63(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_61(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 62, __VA_ARGS__)      \
                GUT_PP_WHILE_62(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_60(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 61, __VA_ARGS__)      \
                GUT_PP_WHILE_61(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_59(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 60, __VA_ARGS__)      \
                GUT_PP_WHILE_60(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_58(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 59, __VA_ARGS__)      \
                GUT_PP_WHILE_59(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_57(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 58, __VA_ARGS__)      \
                GUT_PP_WHILE_58(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_56(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 57, __VA_ARGS__)      \
                GUT_PP_WHILE_57(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_55(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 56, __VA_ARGS__)      \
                GUT_PP_WHILE_56(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_54(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 55, __VA_ARGS__)      \
                GUT_PP_WHILE_55(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_53(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 54, __VA_ARGS__)      \
                GUT_PP_WHILE_54(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_52(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 53, __VA_ARGS__)      \
                GUT_PP_WHILE_53(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_51(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 52, __VA_ARGS__)      \
                GUT_PP_WHILE_52(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_50(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 51, __VA_ARGS__)      \
                GUT_PP_WHILE_51(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_49(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 50, __VA_ARGS__)      \
                GUT_PP_WHILE_50(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_48(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 49, __VA_ARGS__)      \
                GUT_PP_WHILE_49(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_47(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 48, __VA_ARGS__)      \
                GUT_PP_WHILE_48(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_46(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 47, __VA_ARGS__)      \
                GUT_PP_WHILE_47(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_45(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 46, __VA_ARGS__)      \
                GUT_PP_WHILE_46(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_44(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 45, __VA_ARGS__)      \
                GUT_PP_WHILE_45(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_43(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 44, __VA_ARGS__)      \
                GUT_PP_WHILE_44(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_42(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 43, __VA_ARGS__)      \
                GUT_PP_WHILE_43(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_41(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 42, __VA_ARGS__)      \
                GUT_PP_WHILE_42(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_40(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 41, __VA_ARGS__)      \
                GUT_PP_WHILE_41(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_39(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 40, __VA_ARGS__)      \
                GUT_PP_WHILE_40(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_38(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 39, __VA_ARGS__)      \
                GUT_PP_WHILE_39(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_37(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 38, __VA_ARGS__)      \
                GUT_PP_WHILE_38(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_36(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 37, __VA_ARGS__)      \
                GUT_PP_WHILE_37(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_35(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 36, __VA_ARGS__)      \
                GUT_PP_WHILE_36(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_34(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 35, __VA_ARGS__)      \
                GUT_PP_WHILE_35(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_33(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 34, __VA_ARGS__)      \
                GUT_PP_WHILE_34(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_32(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 33, __VA_ARGS__)      \
                GUT_PP_WHILE_33(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_31(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 32, __VA_ARGS__)      \
                GUT_PP_WHILE_32(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_30(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 31, __VA_ARGS__)      \
                GUT_PP_WHILE_31(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_29(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 30, __VA_ARGS__)      \
                GUT_PP_WHILE_30(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_28(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 29, __VA_ARGS__)      \
                GUT_PP_WHILE_29(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_27(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 28, __VA_ARGS__)      \
                GUT_PP_WHILE_28(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_26(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 27, __VA_ARGS__)      \
                GUT_PP_WHILE_27(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_25(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 26, __VA_ARGS__)      \
                GUT_PP_WHILE_26(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_24(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 25, __VA_ARGS__)      \
                GUT_PP_WHILE_25(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_23(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 24, __VA_ARGS__)      \
                GUT_PP_WHILE_24(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_22(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 23, __VA_ARGS__)      \
                GUT_PP_WHILE_23(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_21(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 22, __VA_ARGS__)      \
                GUT_PP_WHILE_22(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_20(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 21, __VA_ARGS__)      \
                GUT_PP_WHILE_21(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_19(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 20, __VA_ARGS__)      \
                GUT_PP_WHILE_20(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_18(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 19, __VA_ARGS__)      \
                GUT_PP_WHILE_19(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_17(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 18, __VA_ARGS__)      \
                GUT_PP_WHILE_18(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_16(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 17, __VA_ARGS__)      \
                GUT_PP_WHILE_17(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_15(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 16, __VA_ARGS__)      \
                GUT_PP_WHILE_16(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_14(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 15, __VA_ARGS__)      \
                GUT_PP_WHILE_15(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_13(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 14, __VA_ARGS__)      \
                GUT_PP_WHILE_14(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_12(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 13, __VA_ARGS__)      \
                GUT_PP_WHILE_13(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_11(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 12, __VA_ARGS__)      \
                GUT_PP_WHILE_12(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_10(FUNC, ...)                                             \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 11, __VA_ARGS__)      \
                GUT_PP_WHILE_11(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_9(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 10, __VA_ARGS__)      \
                GUT_PP_WHILE_10(FUNC, GUT_PP_HEAD(__VA_ARGS__),                \
                                GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_8(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 9, __VA_ARGS__)       \
                GUT_PP_WHILE_9(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_7(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 8, __VA_ARGS__)       \
                GUT_PP_WHILE_8(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_6(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 7, __VA_ARGS__)       \
                GUT_PP_WHILE_7(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_5(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 6, __VA_ARGS__)       \
                GUT_PP_WHILE_6(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_4(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 5, __VA_ARGS__)       \
                GUT_PP_WHILE_5(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_3(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 4, __VA_ARGS__)       \
                GUT_PP_WHILE_4(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_2(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 3, __VA_ARGS__)       \
                GUT_PP_WHILE_3(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_1(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_NOTFIRST, 2, __VA_ARGS__)       \
                GUT_PP_WHILE_2(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
#define GUT_PP_WHILE_0(FUNC, ...)                                              \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_SHIFTLEFT(__VA_ARGS__)), ,                   \
            GUT_PP_WHILE_CALLFUNC(FUNC, GUT_PP_FIRST, 1, __VA_ARGS__)          \
                GUT_PP_WHILE_1(FUNC, GUT_PP_HEAD(__VA_ARGS__),                 \
                               GUT_PP_WHILE_ARGS(__VA_ARGS__)))
