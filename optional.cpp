#include "optional.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(std::is_trivially_copy_constructible_v<optional<int>>,
              "optional of trivially copy constructible type should be "
              "trivially copy constructible.");
static_assert(std::is_trivially_move_constructible_v<optional<int>>,
              "optional of trivially move constructible type should be "
              "trivially move constructible.");
static_assert(std::is_trivially_copy_assignable_v<optional<int>>,
              "optional of trivially copy assignable type should be trivially "
              "copy assignable.");
static_assert(std::is_trivially_move_assignable_v<optional<int>>,
              "optional of trivially move assignable type should be trivially "
              "move assignable.");
static_assert(std::is_trivially_destructible_v<optional<int>>,
              "optional of trivially destructible type should be "
              "trivially destructible.");
static_assert(
    std::is_trivially_copy_constructible_v<optional<int &>>,
    "optional of reference type should be trivially copy constructible.");
static_assert(
    std::is_trivially_move_constructible_v<optional<int &>>,
    "optional of reference type should be trivially move constructible.");
static_assert(
    std::is_trivially_copy_assignable_v<optional<int &>>,
    "optional of reference type should be trivially copy assignable.");
static_assert(
    std::is_trivially_move_assignable_v<optional<int &>>,
    "optional of reference type should be trivially move assignable.");
static_assert(std::is_trivially_destructible_v<optional<int &>>,
              "optional of reference type should be trivially destructible.");
static_assert(sizeof(optional<something>) ==
              2); // TODO: Make empty base specialization.
constexpr optional<int> int_opt;
static_assert(!int_opt,
              "Expected invalid value optional when default constructed.");
constexpr optional opt = 99;
constexpr optional opt_copy = []() {
  optional<long> opt_copy;
  opt_copy = opt;
  opt_copy = 42;
  if (auto [valid, value] = opt_copy; valid)
    opt_copy = *value;
  return opt_copy;
}();
static_assert(
    std::is_trivially_destructible_v<decltype(opt_copy)>,
    "Expected trivially destructible optional of trivially destructible type.");
static_assert(!is_optional_v<decltype(opt)>,
              "Reference to optional is not an optional.");
static_assert(is_optional_v<std::decay_t<decltype(opt)>>,
              "optional should be optional.");
static_assert(opt.value() == 99, "Unexpected optional value as specified.");
static_assert(type_v<const int &> == type_v<decltype(opt.value())>,
              "Expected lvalue reference member.");
static_assert(opt_copy.value() == 42,
              "Expected reassigned constexpr optional value.");

constexpr optional<bool> bool_opt;
static_assert(!bool_opt, "Expected invalid tribool.");
constexpr optional true_opt = true;
static_assert(true_opt, "Expected valid tribool.");
static_assert(true_opt.value(), "Expected true tribool.");
constexpr optional false_opt = false;
static_assert(false_opt, "Expected valid tribool.");
static_assert(!false_opt.value(), "Expected false tribool.");
constexpr optional bool_copy = []() constexpr {
  optional<bool> bool_copy = true_opt;
  bool_copy = false_opt;
  return bool_copy;
}
();
static_assert(!bool_copy.value(),
              "Expected reassigned constexpr tribool value.");

struct NonTrivial {
  int m = 0;
  NonTrivial() = default;
  NonTrivial(const NonTrivial &) = default;
  NonTrivial(NonTrivial &&) = default;
  NonTrivial &operator=(const NonTrivial &) = default;
  NonTrivial &operator=(NonTrivial &&) = default;
  ~NonTrivial() { (void)0; }
};
static_assert(!std::is_trivially_destructible_v<optional<NonTrivial>>,
              "Expected non-trivially destructible optional of non-trivially "
              "destructible type.");

static_assert(
    type_v<int &&> == type_v<decltype(optional<int &&>{}.value())>,
    "Expected rvalue reference optional's rvalue-ness to be preserved.");
} // namespace
