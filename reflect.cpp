#include "reflect.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace reflect_test {
enum E { a, b, c };

constexpr reflect_enum re = GUT_REFLECT_ENUM(E, GUT_REFLECT_MEMBERS(a, b, c));
static_assert(re.members().count() == 3);
static_assert(sizeof(re.type_v()) == 1);
static_assert(sizeof(re.members()) == 1);
static_assert(sizeof(re) == sizeof(re.names()) + sizeof(re.name()));
static_assert(re.name() == "E");
static_assert(re.name(index_v<0>) == "a");
static_assert(re.name(index_v<1>) == "b");
static_assert(re.name(index_v<2>) == "c");
static_assert(re.member(index_v<0>) == E::a);
static_assert(re.member(index_v<1>) == E::b);
static_assert(re.member(index_v<2>) == E::c);
constexpr reflect_enum re2 = GUT_REFLECT_ENUM(E, GUT_REFLECT_MEMBERS());
static_assert(re2.members().count() == 0);
static_assert(sizeof(re2.type_v()) == 1);
static_assert(sizeof(re2.members()) == 1);
static_assert(sizeof(re2) == sizeof(re2.name()));
static_assert(re2.name() == "E");
constexpr reflect_enum re3 = GUT_REFLECT_ENUM(E, GUT_REFLECT_MEMBERS(a));
static_assert(re3.members().count() == 1);
static_assert(sizeof(re3.type_v()) == 1);
static_assert(sizeof(re3.members()) == 1);
static_assert(sizeof(re3) == sizeof(re3.names()) + sizeof(re3.name()));
static_assert(re3.name() == "E");
static_assert(re3.name(index_v<0>) == "a");
static_assert(re3.member(index_v<0>) == E::a);

struct S {
  char a;
  short b;
  int c;
};

constexpr reflect_struct rs =
    GUT_REFLECT_STRUCT(S, GUT_REFLECT_MEMBERS(a, b, c), GUT_REFLECT_BASES());
static_assert(rs.members().count() == 3);
static_assert(rs.bases().count() == 0);
static_assert(sizeof(rs.type_v()) == 1);
static_assert(sizeof(rs.members()) == 1);
static_assert(sizeof(rs.bases()) == 1);
static_assert(sizeof(rs) == sizeof(rs.names()) + sizeof(rs.name()));
static_assert(rs.name() == "S");
static_assert(rs.name(index_v<0>) == "a");
static_assert(rs.name(index_v<1>) == "b");
static_assert(rs.name(index_v<2>) == "c");
static_assert(rs.member(index_v<0>) == &S::a);
static_assert(rs.member(index_v<1>) == &S::b);
static_assert(rs.member(index_v<2>) == &S::c);
constexpr reflect_struct rs2 = GUT_REFLECT_STRUCT(
    S, GUT_REFLECT_MEMBERS(), GUT_REFLECT_BASES(int, char, long));
static_assert(rs2.members().count() == 0);
static_assert(rs2.bases().count() == 3);
static_assert(sizeof(rs2.type_v()) == 1);
static_assert(sizeof(rs2.members()) == 1);
static_assert(sizeof(rs2.bases()) == 1);
static_assert(sizeof(rs2) == sizeof(rs2.name()));
static_assert(rs2.name() == "S");
static_assert(rs2.base(index_v<0>) == type_v<int>);
static_assert(rs2.base(index_v<1>) == type_v<char>);
static_assert(rs2.base(index_v<2>) == type_v<long>);
constexpr reflect_struct rs3 =
    GUT_REFLECT_STRUCT(S, GUT_REFLECT_MEMBERS(a), GUT_REFLECT_BASES(int));
static_assert(rs3.members().count() == 1);
static_assert(rs3.bases().count() == 1);
static_assert(sizeof(rs3.type_v()) == 1);
static_assert(sizeof(rs3.members()) == 1);
static_assert(sizeof(rs3.bases()) == 1);
static_assert(sizeof(rs3) == sizeof(rs3.names()) + sizeof(rs3.name()));
static_assert(rs3.name() == "S");
static_assert(rs3.name(index_v<0>) == "a");
static_assert(rs2.base(index_v<0>) == type_v<int>);
constexpr reflect_struct rs4 =
    GUT_REFLECT_STRUCT(S, GUT_REFLECT_MEMBERS(), GUT_REFLECT_BASES());
static_assert(rs4.members().count() == 0);
static_assert(rs4.bases().count() == 0);
static_assert(sizeof(rs4.type_v()) == 1);
static_assert(sizeof(rs4.members()) == 1);
static_assert(sizeof(rs4.bases()) == 1);
static_assert(sizeof(rs4) == sizeof(rs4.name()));
static_assert(rs4.name() == "S");

char overloads(char) { return {}; }
short overloads(short) { return {}; }
int overloads(int) { return {}; }
long overloads(long) { return {}; }
long long overloads(long long) { return {}; }

constexpr reflect_overloads ro = GUT_REFLECT_OVERLOADS(
    overloads,
    GUT_REFLECT_MEMBERS(char (*)(char), short (*)(short), int (*)(int),
                        long (*)(long), long long (*)(long long)));
static_assert(ro.members().count() == 5);
static_assert(sizeof(ro.members()) == 1);
static_assert(sizeof(ro) == sizeof(ro.name()));
static_assert(ro.name() == "overloads");
static_assert(ro.member(index_v<0>) == static_cast<char (*)(char)>(&overloads));
static_assert(ro.member(index_v<1>) ==
              static_cast<short (*)(short)>(&overloads));
static_assert(ro.member(index_v<2>) == static_cast<int (*)(int)>(&overloads));
static_assert(ro.member(index_v<3>) == static_cast<long (*)(long)>(&overloads));
static_assert(ro.member(index_v<4>) ==
              static_cast<long long (*)(long long)>(&overloads));

struct O {
  char overloads(char) { return {}; }
  short overloads(short) { return {}; }
  int overloads(int) { return {}; }
  long overloads(long) { return {}; }
  long long overloads(long long) { return {}; }
};
constexpr reflect_overloads ro2 = GUT_REFLECT_OVERLOADS(
    O::overloads,
    GUT_REFLECT_MEMBERS(char (O::*)(char), short (O::*)(short), int (O::*)(int),
                        long (O::*)(long), long long (O::*)(long long)));
static_assert(ro2.members().count() == 5);
static_assert(sizeof(ro2.members()) == 1);
static_assert(sizeof(ro2) == sizeof(ro2.name()));
static_assert(ro2.name() == "O::overloads");
static_assert(ro2.member(index_v<0>) ==
              static_cast<char (O::*)(char)>(&O::overloads));
static_assert(ro2.member(index_v<1>) ==
              static_cast<short (O::*)(short)>(&O::overloads));
static_assert(ro2.member(index_v<2>) ==
              static_cast<int (O::*)(int)>(&O::overloads));
static_assert(ro2.member(index_v<3>) ==
              static_cast<long (O::*)(long)>(&O::overloads));
static_assert(ro2.member(index_v<4>) ==
              static_cast<long long (O::*)(long long)>(&O::overloads));
constexpr reflect_struct rO =
    GUT_REFLECT_STRUCT(O, GUT_REFLECT_MEMBERS(), GUT_REFLECT_BASES(S));

constexpr reflect_namespace rn = GUT_REFLECT_NAMESPACE(
    ::reflect_test, GUT_REFLECT_MEMBERS(rs, re, ro, ro2, rO));
static_assert(rn.members().count() == 5);
static_assert(sizeof(rn.members()) == sizeof(rs) + sizeof(re) + sizeof(ro) +
                                          sizeof(ro2) + sizeof(rO) + 1);
static_assert(sizeof(rn) == sizeof(rn.members()) + sizeof(rn.name()) + 1);
static_assert(rn.name() == "::reflect_test");
static_assert(rn.member(index_v<0>).type_v() == type_v<S>);
static_assert(rn.member(index_v<1>).type_v() == type_v<E>);
static_assert(rn.member(index_v<0>).name() == "S");
static_assert(rn.name(index_v<0>) == "::reflect_test::S");
static_assert(rn.member(index_v<0>).member(index_v<0>) == &S::a);
static_assert(rn.member(index_v<1>).name() == "E");
static_assert(rn.name(index_v<1>) == "::reflect_test::E");
static_assert(rn.member(index_v<1>).member(index_v<1>) == E::b);
static_assert(rn.member(index_v<2>).name() == "overloads");
static_assert(rn.name(index_v<2>) == "::reflect_test::overloads");
static_assert(rn.member(index_v<2>).member(index_v<1>) ==
              constant_v<static_cast<short (*)(short)>(overloads)>);
static_assert(rn.member(index_v<3>).name() == "O::overloads");
static_assert(rn.name(index_v<3>) == "::reflect_test::O::overloads");
static_assert(rn.member(index_v<3>).member(index_v<1>) ==
              constant_v<static_cast<short (O::*)(short)>(&O::overloads)>);
static_assert(rn.member(type_v<S>).name() == "S");
static_assert(rn.name(type_v<S>) == "::reflect_test::S");
static_assert(rn.member(type_v<S>).member(index_v<0>) == &S::a);
static_assert(rn.member(type_v<E>).name() == "E");
static_assert(rn.name(type_v<E>) == "::reflect_test::E");
static_assert(rn.member(type_v<E>).member(index_v<1>) == E::b);
} // namespace reflect_test
