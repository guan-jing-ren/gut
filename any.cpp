#include "any.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert([]() {
  enum class Answer { ANSWER = 42 };
  concrete ai{[](constant<Answer::ANSWER>) {
    return static_cast<int>(Answer::ANSWER);
  }};
  auto ai_func = ai.func<Answer::ANSWER, int>();
  auto ai_ref = ai.base();
  static_assert(type_v<decltype(ai_func)>.remove_cvref() ==
                type_v<function<int (any::*)(constant<Answer::ANSWER>)>>);
  static_assert(type_v<decltype(ai_ref)>.remove_cvref() == type_v<ref<any &>>);
  return ai_func(ai_ref.get(), constant_v<Answer::ANSWER>);
}() == 42);
static_assert([]() {
  enum class Answer { ANSWER = 42 };
  const concrete ai{[](constant<Answer::ANSWER>) {
    return static_cast<int>(Answer::ANSWER);
  }};
  auto ai_func = ai.func<Answer::ANSWER, int>();
  auto ai_ref = ai.base();
  static_assert(type_v<decltype(ai_func)>.remove_cvref() ==
                type_v<function<int (any::*)(constant<Answer::ANSWER>) const>>);
  static_assert(type_v<decltype(ai_ref)>.remove_cvref() ==
                type_v<ref<const any &>>);
  return ai_func(ai_ref.get(), constant_v<Answer::ANSWER>);
}() == 42);
} // namespace
