#pragma once

#include "buffer.hpp"
#include "thing.hpp"

namespace gut {
template <index_t BigEndianOffset, index_t... BigEndianOffsets> struct bitfield;
template <typename...>
inline constexpr constant is_bitfield_v = constant_v<false>;
template <index_t... BigEndianOffsets>
inline constexpr constant is_bitfield_v<bitfield<BigEndianOffsets...>> =
    constant_v<true>;

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
struct bitfield {
  constexpr static decltype(auto) count();
  constexpr static decltype(auto) offsets_v();
  constexpr static auto max_offset();
  constexpr static auto min_offset();

  constexpr bitfield() = default;
  constexpr bitfield(const bitfield &) = default;
  constexpr bitfield(bitfield &&) = default;
  constexpr bitfield &operator=(const bitfield &) = default;
  constexpr bitfield &operator=(bitfield &&) = default;
  constexpr bitfield(decltype(BigEndianOffsets)...);

  template <index_t Field>
  constexpr decltype(auto) operator[](index<Field>) const;
  template <index_t Field> constexpr decltype(auto) operator[](index<Field>);
  constexpr decltype(auto)
  operator[](integer<max_bits(BigEndianOffset) + 1>) const;
  constexpr decltype(auto) operator[](integer<max_bits(BigEndianOffset) + 1>);

  template <index_t CastBigEndianOffset, index_t... CastBigEndianOffsets>
  constexpr decltype(auto) cast() const;
  template <index_t CastBigEndianOffset, index_t... CastBigEndianOffsets>
  constexpr decltype(auto) cast();

  template <index_t... Offsets>
  friend constexpr bool operator==(const bitfield<Offsets...> &l,
                                   const bitfield<Offsets...> &r);

private:
  template <index_t, index_t...> friend struct bitfield;
  template <typename, index_t> friend struct field_proxy;
  template <typename> friend struct bit_proxy;

  constexpr static bool is_cast();
  constexpr static bool is_descending();
  constexpr static decltype(auto) offsets();
  constexpr static decltype(auto) size();

  using representation_type = buffer<std::byte, size()>;
  thing<
      std::conditional_t<is_cast(), representation_type &, representation_type>>
      m_field;

  template <count_t Size>
  constexpr bitfield(const buffer<std::byte, Size> &field);
  template <count_t Size> constexpr bitfield(buffer<std::byte, Size> &field);

  static_assert(is_descending());
  static_assert(min_offset() == 0);
};

template <index_t FieldSize>
using field_type = std::conditional_t<
    FieldSize <= 8, std::uint8_t,
    std::conditional_t<
        FieldSize <= 16, std::uint16_t,
        std::conditional_t<FieldSize <= 32, std::uint32_t, std::uint64_t>>>;

template <typename BitFieldType, index_t Field> struct field_proxy {
private:
  constexpr static decltype(auto) lo();
  constexpr static decltype(auto) hi();
  constexpr static decltype(auto) span();

public:
  using bitfield_type = remove_cvref_t<BitFieldType>;
  friend bitfield_type;
  using field_type = gut::field_type<field_proxy::span()>;

  constexpr field_type value() const;
  constexpr operator field_type() const;

  constexpr field_proxy &operator=(field_type value);

  constexpr field_proxy() = delete;
  constexpr field_proxy(const field_proxy &) = delete;
  constexpr field_proxy(field_proxy &&) = delete;
  constexpr field_proxy &operator=(const field_proxy &) = delete;
  constexpr field_proxy &operator=(field_proxy &&) = delete;

private:
  ref<BitFieldType> m_bitfield;

  constexpr field_proxy(BitFieldType source);
  constexpr static decltype(auto) hi_byte();
  constexpr static decltype(auto) lo_byte();
  constexpr static std::byte hi_mask();
  constexpr static std::byte lo_mask();
  constexpr static decltype(auto) mask();
};

template <typename BitFieldType> struct bit_proxy {
  using bitfield_type = remove_cvref_t<BitFieldType>;
  friend bitfield_type;

  constexpr bool value() const;
  constexpr operator bool() const;
  constexpr bit_proxy &operator=(bool value);

  constexpr bit_proxy() = delete;
  constexpr bit_proxy(const bit_proxy &) = delete;
  constexpr bit_proxy(bit_proxy &&) = delete;
  constexpr bit_proxy &operator=(const bit_proxy &) = delete;
  constexpr bit_proxy &operator=(bit_proxy &&) = delete;

private:
  ref<BitFieldType> m_bitfield;
  integer<max_bits(bitfield_type::max_offset()) + 1> m_bit;

  constexpr bit_proxy(BitFieldType,
                      integer<max_bits(bitfield_type::max_offset()) + 1>);
};

template <typename ArrayType> constexpr auto make_span_type();

////////////////////
// IMPLEMENTATION //
////////////////////

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::count() {
  return count_v<sizeof...(BigEndianOffsets)>;
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::offsets_v() {
  return (
      index_v<(BigEndianOffset < 0 ? -BigEndianOffset : BigEndianOffset)> +
      ... +
      index_v<(BigEndianOffsets < 0 ? -BigEndianOffsets : BigEndianOffsets)>);
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr auto bitfield<BigEndianOffset, BigEndianOffsets...>::max_offset() {
  return offsets().front();
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr auto bitfield<BigEndianOffset, BigEndianOffsets...>::min_offset() {
  return offsets().back();
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr bitfield<BigEndianOffset, BigEndianOffsets...>::bitfield(
    decltype(BigEndianOffsets)... field_values)
    : m_field() {
  make_index_list<sizeof...(BigEndianOffsets)>().reduce(
      [ this, &field_values... ](auto... i) mutable constexpr {
        return (... + field_values) +
               (... + ((this->operator[](i) = field_values), 0));
      });
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <index_t Field>
constexpr decltype(auto)
    bitfield<BigEndianOffset, BigEndianOffsets...>::operator[](
        index<Field>) const {
  return field_proxy<const bitfield &, Field>{*this};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <index_t Field>
constexpr decltype(auto)
    bitfield<BigEndianOffset, BigEndianOffsets...>::operator[](index<Field>) {
  return field_proxy<bitfield &, Field>{*this};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
    bitfield<BigEndianOffset, BigEndianOffsets...>::operator[](
        integer<max_bits(BigEndianOffset) + 1> bit) const {
  return bit_proxy<const bitfield &>{*this, bit};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
    bitfield<BigEndianOffset, BigEndianOffsets...>::operator[](
        integer<max_bits(BigEndianOffset) + 1> bit) {
  return bit_proxy<bitfield &>{*this, bit};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <index_t CastBigEndianOffset, index_t... CastBigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::cast() const {
  return gut::bitfield<-CastBigEndianOffset, -CastBigEndianOffsets...>{
      m_field.get()};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <index_t CastBigEndianOffset, index_t... CastBigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::cast() {
  return gut::bitfield<-CastBigEndianOffset, -CastBigEndianOffsets...>{
      m_field.get()};
}

template <index_t... BigEndianOffsets>
constexpr bool operator==(const bitfield<BigEndianOffsets...> &l,
                          const bitfield<BigEndianOffsets...> &r) {
  return l.m_field.get() == r.m_field.get();
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr bool bitfield<BigEndianOffset, BigEndianOffsets...>::is_cast() {
  return ((BigEndianOffset < 0) || ... || (BigEndianOffsets < 0));
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr bool bitfield<BigEndianOffset, BigEndianOffsets...>::is_descending() {
  auto prev = std::numeric_limits<count_t>::max();
  for (auto o : offsets())
    if (o < prev)
      prev = o;
    else
      return false;
  return true;
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::size() {
  return count_v<max_offset() / CHAR_BIT + (max_offset() % CHAR_BIT ? 1 : 0)>;
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
constexpr decltype(auto)
bitfield<BigEndianOffset, BigEndianOffsets...>::offsets() {
  return buffer{
      {(BigEndianOffset < 0 ? -BigEndianOffset : BigEndianOffset),
       (BigEndianOffsets < 0 ? -BigEndianOffsets : BigEndianOffsets)...}};
}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <count_t Size>
constexpr bitfield<BigEndianOffset, BigEndianOffsets...>::bitfield(
    const buffer<std::byte, Size> &field)
    : m_field(field) {}

template <index_t BigEndianOffset, index_t... BigEndianOffsets>
template <count_t Size>
constexpr bitfield<BigEndianOffset, BigEndianOffsets...>::bitfield(
    buffer<std::byte, Size> &field)
    : m_field(field) {}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::lo() {
  return index_v<bitfield_type::offsets()[Field + 1]>;
}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::hi() {
  return index_v<bitfield_type::offsets()[Field]>;
}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::span() {
  return count_v<hi() - lo()>;
}

template <typename BitFieldType, index_t Field>
constexpr auto field_proxy<BitFieldType, Field>::value() const -> field_type {
  constexpr buffer m = mask();
  gut::field_type<(lo_byte() - hi_byte()) *CHAR_BIT> field = 0;
  for (index_t first = hi_byte(), last = lo_byte(); first != last; ++first) {
    field |= static_cast<decltype(field)>(
        ~m[first - hi_byte()] & m_bitfield.get().m_field.get()[first]);
    if constexpr (sizeof(decltype(field)) > 1)
      if (last - first != 1)
        field <<= CHAR_BIT;
  }
  field >>= (lo() % CHAR_BIT);
  return static_cast<field_type>(field);
}

template <typename BitFieldType, index_t Field>
constexpr field_proxy<BitFieldType, Field>::operator field_type() const {
  return value();
}

template <typename BitFieldType, index_t Field>
constexpr auto field_proxy<BitFieldType, Field>::operator=(field_type value)
    -> field_proxy & {
  constexpr buffer m = mask();
  gut::field_type<(lo_byte() - hi_byte()) *CHAR_BIT> field = value;
  field <<= (lo() % CHAR_BIT);
  for (index_t first = hi_byte(), last = lo_byte(); first != last; ++first) {
    m_bitfield.get().m_field.get()[first] &= m[first - hi_byte()];
    m_bitfield.get().m_field.get()[first] |=
        std::byte((field >> ((last - first - 1) * CHAR_BIT)) & 0b1111'1111) &
        ~m[first - hi_byte()];
  }
  return *this;
}

template <typename BitFieldType, index_t Field>
constexpr field_proxy<BitFieldType, Field>::field_proxy(BitFieldType source)
    : m_bitfield(source) {}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::lo_byte() {
  return index_v<bitfield_type::size() - lo() / CHAR_BIT>;
}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::hi_byte() {
  return index_v<bitfield_type::size() - (hi() - 1) / CHAR_BIT - 1>;
}

template <typename BitFieldType, index_t Field>
constexpr std::byte field_proxy<BitFieldType, Field>::hi_mask() {
  return hi() % CHAR_BIT ? ~std::byte{} << (hi() % CHAR_BIT) : std::byte{};
}

template <typename BitFieldType, index_t Field>
constexpr std::byte field_proxy<BitFieldType, Field>::lo_mask() {
  return lo() % CHAR_BIT ? ~std::byte{} >> (CHAR_BIT - (lo() % CHAR_BIT))
                         : std::byte{};
}

template <typename BitFieldType, index_t Field>
constexpr decltype(auto) field_proxy<BitFieldType, Field>::mask() {
  buffer<std::byte, lo_byte() - hi_byte()> mask{};
  mask.front() |= hi_mask();
  mask.back() |= lo_mask();
  return mask;
}

template <typename ArrayType> constexpr auto make_span_type() {
  return type_v<ArrayType>.get_extents().reduce([
  ](auto... extents) constexpr mutable->decltype(auto) {
    return make_index_list<sizeof...(extents)>()
        .reduce([extents...](auto... i) constexpr->decltype(auto) {
          auto partial_sum = [extents...](auto i) constexpr {
            constexpr buffer field_array{{(max_bits(extents.value()) + 1)...}};
            count_t sum = 0;
            for (index_t index = i; index < field_array.count(); ++index)
              sum += field_array[index];
            return sum;
          };
          return bitfield<partial_sum(i)..., 0>{extents...};
        });
  });
}

template <typename BitFieldType>
constexpr bool bit_proxy<BitFieldType>::value() const {
  return static_cast<bool>(m_bitfield.get().m_field.get()[m_bit / CHAR_BIT] &
                           static_cast<std::byte>(1 << (m_bit % CHAR_BIT)));
}

template <typename BitFieldType>
constexpr bit_proxy<BitFieldType>::operator bool() const {
  return value();
}

template <typename BitFieldType>
constexpr auto bit_proxy<BitFieldType>::operator=(bool value) -> bit_proxy & {
  auto bit = static_cast<std::byte>(1 << (m_bit % CHAR_BIT));
  if (value)
    m_bitfield.get().m_field.get()[m_bit / CHAR_BIT] |= bit;
  else
    m_bitfield.get().m_field.get()[m_bit / CHAR_BIT] &= ~bit;
  return *this;
}

template <typename BitFieldType>
constexpr bit_proxy<BitFieldType>::bit_proxy(
    BitFieldType source, integer<max_bits(bitfield_type::max_offset()) + 1> bit)
    : m_bitfield(source), m_bit(bit) {}
} // namespace gut
