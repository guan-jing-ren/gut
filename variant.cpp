#include "variant.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
/*
-->
<h2>Compile time conformance tests</h2>
<!--
*/
static_assert(
    std::is_default_constructible_v<variant<char, int, char>>,
    "Variant of  default constructible types should be default constructible.");
static_assert(std::is_trivially_copy_constructible_v<variant<char, int, char>>,
              "Variant of trivially copy constructible types should be "
              "trivially copy constructible.");
static_assert(std::is_trivially_move_constructible_v<variant<char, int, char>>,
              "Variant of trivially move constructible types should be "
              "trivially move constructible.");
static_assert(std::is_trivially_copy_assignable_v<variant<char, int, char>>,
              "Variant of trivially copy assignable types should be trivially "
              "copy assignable.");
static_assert(std::is_trivially_move_assignable_v<variant<char, int, char>>,
              "Variant of trivially move assignable types should be trivially "
              "move assignable.");
static_assert(std::is_trivially_destructible_v<variant<char, int, char>>,
              "Variant of trivially destructible types should be trivially "
              "copy constructible.");
static_assert(std::is_default_constructible_v<variant<char, int &, char>>,
              "Variant of trivially default constructible types should be "
              "trivially default constructible.");
static_assert(
    std::is_trivially_copy_constructible_v<variant<char, int &, char>>,
    "Variant of trivially copy constructible types should be trivially copy "
    "constructible.");
static_assert(
    std::is_trivially_move_constructible_v<variant<char, int &, char>>,
    "Variant of trivially move constructible types should be trivially move "
    "constructible.");
static_assert(std::is_trivially_copy_assignable_v<variant<char, int &, char>>,
              "Variant of trivially copy assignable types should be trivially "
              "copy assignable.");
static_assert(std::is_trivially_move_assignable_v<variant<char, int &, char>>,
              "Variant of trivially move assignable types should be trivially "
              "move assignable.");
static_assert(std::is_trivially_destructible_v<variant<char, int &, char>>,
              "Variant of trivially destructible types should be trivially "
              "destructible.");
static_assert(
    !std::is_default_constructible_v<variant<char &, int &, char &>>,
    "Variant of reference types should not be default constructible.");
/*
-->
<pre>
*/
static_assert(sizeof(variant<something, something>) == 2);
/*
</pre>
<p class="comment todo">Make empty member optimization so that
<code>sizeof(...)</code> is <code>1</code>. The rationale behind this is that if
a member type is an <em>empty</em> type, it would be cheap to construct one if
needed, instead of storing it. All that is needed to represent the choice is for
the <code>index</code> to work as designed.</p>
<!--
*/
template <typename MemberType>
struct not_long : decltype(type_v<MemberType> != type_v<long>) {};
constexpr variant cic_var = []() {
  variant<char, int, char> cic_var, cic_var2{index_v<2>, 127};
  cic_var = cic_var2;
  variant<char, char, int> cci_var = cic_var.value(index_v<2>);
  cic_var = std::move(cic_var2);
  cic_var = {index_v<2>, 'z'};
  cci_var = cic_var.value(index_v<2>);
  cci_var = cic_var.value(type_v<char>);
  cic_var = 'z';
  cci_var = cic_var.value(index_v<0>);
  cci_var = cic_var.value(type_v<char>);
  cic_var = variant<char, int, char>{'z'};
  cci_var = cic_var.value(index_v<0>);
  cic_var = variant<char, int, char>{127};
  cci_var = cic_var.value(index_v<1>);
  auto mapped_cic_var = cic_var.each([](auto &&v) { v -= 4; })
                            .map([](auto &&v) -> remove_cvref_t<decltype(v)> {
                              return v + remove_cvref_t<decltype(v)>{2};
                            })
                            .value()
                            .filter<not_long>()
                            .value();
  switch (auto [idx, a, b, c] = cic_var; idx) {
  default:
    break;
  case 0:
    cic_var = {index_v<0>, *a};
    break;
  case 1:
    cic_var = {index_v<1>, mapped_cic_var.value().value(index_v<1>) + 3};
    break;
  case 2:
    cic_var = {index_v<2>, *c};
    break;
  };
  return cic_var;
}();
static_assert(cic_var.value(index_v<1>) == 128,
              "Expected value at variant index as specified.");
static_assert(
    !std::is_default_constructible_v<variant<bool &>>,
    "Single ref membered variant should not be default constructible.");
static_assert(sizeof(cic_var.index()) == 1,
              "Expected size of 1 for single membered variant.");
constexpr int answer = variant<char, short, int, long>{40}
                           .reduce([](auto &&i) { return i + 2; })
                           .value();
static_assert(answer == 42);
constexpr auto max_lambda = [](auto v) {
  if constexpr (type_v<int> == type_v<decltype(v)>)
    return std::numeric_limits<decltype(v)>::max();
};
constexpr variant<void (*)(char), void (*)(short), int (*)(int), void (*)(long),
                  void (*)(long long), int &(*)(int)>
    maxfunc_variant = {index_v<2>, max_lambda};
constexpr variant max_variant = maxfunc_variant(42).value();
static_assert(max_variant.value(index_v<2>) == std::numeric_limits<int>::max());
static_assert(type_v<const something &> ==
              type_v<decltype(max_variant.value(index_v<0>))>);
static_assert(type_v<const something &> ==
              type_v<decltype(max_variant.value(index_v<1>))>);
static_assert(type_v<const something &> ==
              type_v<decltype(max_variant.value(index_v<3>))>);
static_assert(type_v<const something &> ==
              type_v<decltype(max_variant.value(index_v<4>))>);
static_assert(type_v<int &> == type_v<decltype(max_variant.value(index_v<5>))>);
static_assert(sizeof(variant<bool>) == 1,
              "Expected size of 1 for single membered bool variant.");
static_assert(sizeof(variant<bool, bool>) == 2,
              "Expected size of 2 for two byte-sized membered variant.");
static_assert(sizeof(variant<bool &>) == sizeof(std::intptr_t),
              "Expected size of single ref membered variant to be the same "
              "size as a pointer.");
constexpr variant b_var = []() {
  variant<bool> b_var;
  static_assert(sizeof(b_var) == sizeof(bool));
  if (auto [idx, b] = b_var; idx == 0)
    b_var = {index_v<0>, 1};
  return b_var;
}();
static_assert(b_var.index() == 0, "Expected valid bool variant as specified.");
static_assert(b_var.value(index_v<0>),
              "Expected bool variant value as specified.");
static_assert(b_var.index() == 0, "Expected bool variant index as specified.");
constexpr variant i_var = []() {
  variant<int> i_var = variant<long>{index_v<0>, 42}.value(index_v<0>);
  static_assert(sizeof(i_var) == sizeof(int));
  if (auto [idx, i] = i_var; idx == 0)
    i_var = {index_v<0>, *i};
  return i_var;
}();
static_assert(i_var.index() == 0,
              "Expected valid single int variant as specified.");
static_assert(i_var.value(index_v<0>) == 42,
              "Expected single int variant value as specified.");
static_assert(i_var.index() == 0,
              "Expected single int variant index as specified.");
struct NonTrivialDestructor {
  NonTrivialDestructor(const NonTrivialDestructor &) = default;
  NonTrivialDestructor(NonTrivialDestructor &&) = default;
  NonTrivialDestructor &operator=(const NonTrivialDestructor &) = default;
  NonTrivialDestructor &operator=(NonTrivialDestructor &&) = default;
  ~NonTrivialDestructor() { (void)0; }
};
static_assert(!std::is_trivially_destructible_v<NonTrivialDestructor>);
static_assert(
    !std::is_trivially_destructible_v<variant<int, NonTrivialDestructor>>);
} // namespace
/*
-->
*/
