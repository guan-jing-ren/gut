/*
<!--
*/
#pragma once

#include "any.hpp"
#include "fstring.hpp"
#include "space.hpp"
#include "variant.hpp"

namespace gut {
struct pattern_trait;
struct match_config;
struct pattern;
struct parser;

struct pattern_trait {
  using value_type = char;
  constexpr static decltype(auto) value_type_v() { return type<value_type>{}; }

  constexpr static decltype(auto) string_pool_length() {
    return integer{count_v<4096>};
  }
  constexpr static decltype(auto) string_pool_average() {
    return integer{count_v<8>};
  }
  constexpr static decltype(auto) span_length() { return integer{count_v<8>}; }
  constexpr static decltype(auto) max_patterns() {
    return integer{count_v<4096>};
  }
  constexpr static decltype(auto) max_position() {
    return integer{count_v<(1 << 20)>};
  }

  constexpr static decltype(auto) stack_size() {
    return integer{count_v<8192>};
  }
  constexpr static decltype(auto) stack_depth() { return stack_size() / 4; }
  constexpr static decltype(auto) retry_size() { return stack_size(); }
  constexpr static decltype(auto) retry_depth() { return retry_size() / 4; }
};

struct pattern_base {
  using trait_type = pattern_trait;
  constexpr static decltype(auto) trait_type_v();

  constexpr pattern_base() = default;

  template <typename TagType> constexpr pattern_base(type<TagType>);

  template <typename TagType> constexpr pattern_base(type<TagType>, index_t p);

  template <typename TagType>
  constexpr pattern_base(type<TagType>,
                         optional<decltype(trait_type::max_patterns())> p);

  template <typename TagType>
  constexpr pattern_base(type<TagType>,
                         optional<decltype(trait_type::max_patterns())> pattern,
                         optional<decltype(trait_type::max_patterns())> n);

  template <typename TagType, count_t N>
  constexpr pattern_base(type<TagType>,
                         decltype(integer{
                             count_v<trait_type::string_pool_length() /
                                     trait_type::string_pool_average()>}) s);

  template <typename TagType>
  constexpr pattern_base(type<TagType>, trait_type::value_type first,
                         trait_type::value_type last);

  struct string;
  struct range;
  struct not_range;
  struct span_range;
  struct not_span_range;
  struct not_span_range_greedy;
  struct set;
  struct not_set;
  struct span_set;
  struct not_span_set;
  struct not_span_set_greedy;
  struct chain;
  struct choice;
  /*
  <dt>
  */
  struct trace;
  /*
  </dt>
  <dd>
  <details open>
  <summary>Decides whether to proceed with the matching for the current
  pattern.</summary>

  Function decides based on either the current match, or some other state.
  </details>
  </dd>
  */
  struct remaining;
  struct arbitrary_character;
  /*
  <dd>
  <details open>

  The equivalent of <em>SPITBOL</em>'s <code>ARB</code> pattern.
  </details>
  </dd>
  <dt>
  */
  struct arbitrary_pattern;
  /*
  <dd>
  <details open>

  The equivalent of <em>SPITBOL</em>'s <code>ARBNO</code> pattern.
  </details>
  </dd>
  <dt>
  */
  struct length;
  struct position;
  struct position_reverse;
  struct tab;
  struct tab_reverse;
  /*
  <dd>
  <details open>

  The equivalent of <em>SPITBOL</em>'s <code>BREAKX</code> pattern.
  </details>
  </dd>
  <dt>
  */
  struct quit;
  /*
  </dt>
  <dd>
  <details open>
  <summary>Fails the entire match.</summary>

  The equivalent of <em>SPITBOL</em>'s <code>ABORT</code> pattern.
  </details>
  </dd>
  */
  struct found;
  /*
  <dt>
  */
  struct backtrack;
  /*
  </dt>
  <dd>
  <details open>
  <summary>Tries next available alternative match.</summary>

  The equivalent of <em>SPITBOL</em>'s <code>FAIL</code> pattern.
  </details>
  </dd>
  */
  struct no_backtrack;
  /*
  <dt>
  */
  struct no_retry;
  /*
  </dt>
  <dd>
  <details open>
  <summary>Once the pattern is matched, any subsequent backtracking does not
  trigger the alternatives in the pattern.</summary>

  The equivalent of <em>SPITBOL</em>'s <code>FENCE(pattern)</code> pattern.
  </details>.
  */
  struct char_range {
    trait_type::value_type m_first, m_last;
  };
  struct char_set {
    decltype(integer{count_v<trait_type::string_pool_length() /
                             trait_type::string_pool_average()>}) m_pool_index;
  };
  struct adapter {
    optional<decltype(trait_type::max_patterns())> m_pattern_index;
  };
  struct linking : adapter {
    optional<decltype(trait_type::max_patterns())> m_next;
  };
  struct positional {
    decltype(trait_type::max_position()) m_value;
  };
  struct retry {};

  struct string : char_set {};
  struct range : char_range {};
  struct not_range : char_range {};
  struct span_range : char_range {};
  struct not_span_range : char_range {};
  struct not_span_range_greedy : char_range, retry {};
  struct set : char_set {};
  struct not_set : char_set {};
  struct span_set : char_set {};
  struct not_span_set : char_set {};
  struct not_span_set_greedy : char_set, retry {};
  struct arbitrary_character : retry {};
  struct arbitrary_pattern : adapter, retry {};
  struct remaining {};
  struct chain : linking {};
  struct choice : linking, retry {};
  struct trace : adapter {};
  struct length : positional {};
  struct position : positional {};
  struct position_reverse : positional {};
  struct tab : positional {};
  struct tab_reverse : positional {};
  struct quit {};
  struct found {};
  struct backtrack {};
  struct no_backtrack {};
  struct no_retry : adapter {};

  variant<string /* 0 */, range /* 1 */, not_range /* 2 */, span_range /* 3 */,
          not_span_range /* 4 */, not_span_range_greedy /* 5 */, set /* 6 */,
          not_set /* 7 */, span_set /* 8 */, not_span_set /* 9 */,
          not_span_set_greedy /* 10 */, arbitrary_character /* 11 */,
          arbitrary_pattern /* 12 */, remaining /* 13 */, chain /* 14 */,
          choice /* 15 */, trace /* 16 */, length /* 17 */, position /* 18 */,
          position_reverse /* 19 */, tab /* 20 */, tab_reverse /* 21 */,
          quit /* 22 */, found /* 23 */, backtrack /* 24 */,
          no_backtrack /* 25 */, no_retry /* 26 */>
      m_type;
};

struct pattern {
  using base_type = pattern_base;
  constexpr decltype(auto) base_type_v();
  using trait_type = pattern_trait;
  constexpr decltype(auto) trait_type_v();

  friend constexpr pattern operator|(pattern, pattern);
  friend constexpr pattern operator&(pattern, pattern);

private:
  friend struct parser;
  friend struct pattern_test;

  ref<parser &> m_parser;
  optional<decltype(trait_type::max_patterns())> m_index;

  constexpr pattern(parser &parser,
                    optional<decltype(trait_type::max_patterns())> index = {});
};

struct parser {
  using trait_type = pattern_trait;
  constexpr static decltype(auto) trait_type_v();

  template <count_t N>
  constexpr decltype(auto)
  string(const pattern_base::trait_type::value_type (&s)[N]);
  constexpr decltype(auto) range(pattern_base::trait_type::value_type first,
                                 pattern_base::trait_type::value_type last);
  constexpr decltype(auto) not_range(pattern_base::trait_type::value_type first,
                                     pattern_base::trait_type::value_type last);
  constexpr decltype(auto)
  span_range(pattern_base::trait_type::value_type first,
             pattern_base::trait_type::value_type last);
  constexpr decltype(auto)
  not_span_range(pattern_base::trait_type::value_type first,
                 pattern_base::trait_type::value_type last);
  constexpr decltype(auto)
  not_span_range_greedy(pattern_base::trait_type::value_type first,
                        pattern_base::trait_type::value_type last);
  template <count_t N>
  constexpr decltype(auto)
  set(const pattern_base::trait_type::value_type (&c)[N]);
  template <count_t N>
  constexpr decltype(auto)
  not_set(const pattern_base::trait_type::value_type (&c)[N]);
  template <count_t N>
  constexpr decltype(auto)
  span_set(const pattern_base::trait_type::value_type (&c)[N]);
  template <count_t N>
  constexpr decltype(auto)
  not_span_set(const pattern_base::trait_type::value_type (&c)[N]);
  template <count_t N>
  constexpr decltype(auto)
  not_span_set_greedy(const pattern_base::trait_type::value_type (&c)[N]);
  constexpr decltype(auto) arbitrary_character();
  constexpr decltype(auto) arbitrary_pattern(pattern p);
  constexpr decltype(auto) remaining();
  constexpr decltype(auto) trace(pattern p);
  constexpr decltype(auto) length(count_t length);
  constexpr decltype(auto) position(index_t index);
  constexpr decltype(auto) position_reverse(index_t index);
  constexpr decltype(auto) tab(index_t index);
  constexpr decltype(auto) tab_reverse(index_t index);
  constexpr decltype(auto) quit();
  constexpr decltype(auto) found();
  constexpr decltype(auto) backtrack();
  constexpr decltype(auto) no_backtrack();
  constexpr decltype(auto) no_retry(pattern p);

  template <typename Tracer>
  constexpr static decltype(auto) convert_to_tracer(concrete<Tracer> &tracer);
  enum match_op { MATCH, FAIL };
  constexpr decltype(auto)
  match(pattern, space<const trait_type::value_type *, integer<12>> str,
        optional<
            ntuple<ref<any &>,
                   function<pattern (any::*)(constant<match_op::MATCH>, pattern,
                                             space<const char *, integer<12>>,
                                             integer<12>, integer<12>)>,
                   function<void (any::*)(constant<match_op::FAIL>, pattern,
                                          space<const char *, integer<12>>,
                                          integer<12>, integer<12>)>>> = {});

private:
  friend struct pattern;

  buffer<trait_type::value_type, trait_type::string_pool_length()>
      m_string_pool{};
  buffer<decltype(trait_type::string_pool_length()),
         trait_type::string_pool_length() / trait_type::string_pool_average()>
      m_string_indices{};
  decltype(integer{m_string_indices.count()}) m_string_top = 0;
  buffer<pattern_base, trait_type::max_patterns()> m_patterns{};
  decltype(integer{m_patterns.count()}) m_top = 0;

  template <count_t N>
  constexpr decltype(auto) push_string(const trait_type::value_type (&s)[N]);
  constexpr decltype(auto) push_pattern(pattern_base);
  constexpr decltype(auto) get_string(const pattern_base::char_set &cs) const;

  constexpr decltype(auto) chain(pattern p);
  constexpr decltype(auto) choice(pattern p);
  constexpr decltype(auto) representation(const pattern &p) const;
  constexpr decltype(auto) representation(const pattern &p);
  template <typename PatternType>
  constexpr decltype(auto) is_type(decltype(trait_type::max_patterns()) index,
                                   type<PatternType>) const;

  friend constexpr pattern operator|(pattern, pattern);
  friend constexpr pattern operator&(pattern, pattern);
  friend struct pattern_test;
};

////////////////////
// IMPLEMENTATION //
////////////////////

constexpr decltype(auto) pattern_base::trait_type_v() {
  return type_v<trait_type>;
}

template <typename TagType>
constexpr pattern_base::pattern_base(type<TagType>) : m_type(TagType{}) {}

template <typename TagType>
constexpr pattern_base::pattern_base(type<TagType>, index_t p)
    : m_type(TagType{{p}}) {}

template <typename TagType>
constexpr pattern_base::pattern_base(
    type<TagType>, optional<decltype(trait_type::max_patterns())> p)
    : m_type([&p]() mutable constexpr->decltype(auto) {
        if constexpr (
            type_v<TagType>.template is_derived_from<pattern_base::retry>())
          return TagType{adapter{p}, {}};
        else
          return TagType{adapter{p}};
      }()) {}

template <typename TagType>
constexpr pattern_base::pattern_base(
    type<TagType>, optional<decltype(trait_type::max_patterns())> pattern,
    optional<decltype(trait_type::max_patterns())> n)
    : m_type([&pattern, &n ]() mutable constexpr->decltype(auto) {
        if constexpr (
            type_v<TagType>.template is_derived_from<pattern_base::retry>())
          return TagType{linking{adapter{pattern}, n}, {}};
        else
          return TagType{linking{adapter{pattern}, n}};
      }()) {}

template <typename TagType, count_t N>
constexpr pattern_base::pattern_base(
    type<TagType>,
    decltype(integer{count_v<trait_type::string_pool_length() /
                             trait_type::string_pool_average()>}) s)
    : m_type(TagType{char_set{s}}) {}

template <typename TagType>
constexpr pattern_base::pattern_base(type<TagType>,
                                     trait_type::value_type first,
                                     trait_type::value_type last)
    : m_type(TagType{char_range{first, last}}) {}

constexpr decltype(auto) pattern::base_type_v() { return type_v<base_type>; }

constexpr decltype(auto) pattern::trait_type_v() { return type_v<trait_type>; }

constexpr pattern::pattern(parser &parser,
                           optional<decltype(trait_type::max_patterns())> index)
    : m_parser(parser), m_index(index) {}

constexpr decltype(auto) parser::trait_type_v() { return type_v<trait_type>; }

template <count_t N>
constexpr decltype(auto)
parser::push_string(const trait_type::value_type (&s)[N]) {
  decltype(integer{m_string_pool.count()}) string_top_index = 0;
  if (m_string_top)
    string_top_index = m_string_indices[m_string_top - 1];
  for (count_t n = 0; n < N - 1; ++n)
    m_string_pool[n + string_top_index] = s[n];
  m_string_indices[m_string_top] = N - 1 + string_top_index;
  auto string_top = m_string_top;
  ++m_string_top;
  return string_top;
}

constexpr decltype(auto) parser::push_pattern(pattern_base p) {
  m_patterns[m_top] = p;
  auto top = m_top;
  ++m_top;
  return pattern{*this, top};
}

template <count_t N>
constexpr decltype(auto) parser::string(const trait_type::value_type (&s)[N]) {
  return push_pattern({type_v<pattern_base::string>, push_string(s)});
}

constexpr decltype(auto) parser::range(trait_type::value_type first,
                                       trait_type::value_type last) {
  return push_pattern({type_v<pattern_base::range>, first, last});
}

constexpr decltype(auto)
parser::not_range(pattern_base::trait_type::value_type first,
                  pattern_base::trait_type::value_type last) {
  return push_pattern({type_v<pattern_base::not_range>, first, last});
}

constexpr decltype(auto)
parser::span_range(pattern_base::trait_type::value_type first,
                   pattern_base::trait_type::value_type last) {
  return push_pattern({type_v<pattern_base::span_range>, first, last});
}

constexpr decltype(auto)
parser::not_span_range(pattern_base::trait_type::value_type first,
                       pattern_base::trait_type::value_type last) {
  return push_pattern({type_v<pattern_base::not_span_range>, first, last});
}

constexpr decltype(auto)
parser::not_span_range_greedy(pattern_base::trait_type::value_type first,
                              pattern_base::trait_type::value_type last) {
  return push_pattern(
      {type_v<pattern_base::not_span_range_greedy>, first, last});
}

template <count_t N>
constexpr decltype(auto)
parser::set(const pattern_base::trait_type::value_type (&c)[N]) {
  return push_pattern({type_v<pattern_base::set>, push_string(c)});
}

template <count_t N>
constexpr decltype(auto)
parser::not_set(const pattern_base::trait_type::value_type (&c)[N]) {
  return push_pattern({type_v<pattern_base::not_set>, push_string(c)});
}

template <count_t N>
constexpr decltype(auto)
parser::span_set(const pattern_base::trait_type::value_type (&c)[N]) {
  return push_pattern({type_v<pattern_base::span_set>, push_string(c)});
}

template <count_t N>
constexpr decltype(auto)
parser::not_span_set(const pattern_base::trait_type::value_type (&c)[N]) {
  return push_pattern({type_v<pattern_base::not_span_set>, push_string(c)});
}

template <count_t N>
constexpr decltype(auto) parser::not_span_set_greedy(
    const pattern_base::trait_type::value_type (&c)[N]) {
  return push_pattern(
      {type_v<pattern_base::not_span_set_greedy>, push_string(c)});
}

constexpr decltype(auto) parser::arbitrary_character() {
  return push_pattern({type_v<pattern_base::arbitrary_character>});
}

constexpr decltype(auto) parser::arbitrary_pattern(pattern p) {
  return push_pattern({type_v<pattern_base::arbitrary_pattern>, p.m_index});
}

constexpr decltype(auto) parser::remaining() {
  return push_pattern({type_v<pattern_base::remaining>});
}

constexpr decltype(auto) parser::trace(pattern p) {
  return push_pattern({type_v<pattern_base::trace>, p.m_index});
}

constexpr decltype(auto) parser::length(count_t length) {
  return push_pattern({type_v<pattern_base::length>, length});
}

constexpr decltype(auto) parser::position(index_t index) {
  return push_pattern({type_v<pattern_base::position>, index});
}

constexpr decltype(auto) parser::position_reverse(index_t index) {
  return push_pattern({type_v<pattern_base::position_reverse>, index});
}

constexpr decltype(auto) parser::tab(index_t index) {
  return push_pattern({type_v<pattern_base::tab>, index});
}

constexpr decltype(auto) parser::tab_reverse(index_t index) {
  return push_pattern({type_v<pattern_base::tab_reverse>, index});
}

constexpr decltype(auto) parser::quit() {
  return push_pattern({type_v<pattern_base::quit>});
}

constexpr decltype(auto) parser::found() {
  return push_pattern({type_v<pattern_base::found>});
}

constexpr decltype(auto) parser::backtrack() {
  return push_pattern({type_v<pattern_base::backtrack>});
}

constexpr decltype(auto) parser::no_backtrack() {
  return push_pattern({type_v<pattern_base::no_backtrack>});
}

constexpr decltype(auto) parser::no_retry(pattern p) {
  return push_pattern({type_v<pattern_base::no_retry>, p.m_index});
}

constexpr decltype(auto) parser::chain(pattern p) {
  return push_pattern({type_v<pattern_base::chain>, p.m_index,
                       optional<decltype(trait_type::max_patterns())>{}});
}
constexpr decltype(auto) parser::choice(pattern p) {
  return push_pattern({type_v<pattern_base::choice>, p.m_index,
                       optional<decltype(trait_type::max_patterns())>{}});
}

constexpr decltype(auto) parser::representation(const pattern &p) const {
  return m_patterns[p.m_index.value()].m_type;
}

constexpr decltype(auto) parser::representation(const pattern &p) {
  return m_patterns[p.m_index.value()].m_type;
}

template <typename PatternType>
constexpr decltype(auto)
parser::is_type(decltype(trait_type::max_patterns()) index,
                type<PatternType>) const {
  if (index == trait_type::max_patterns())
    return false;
  bool is_type = false;
  m_patterns[index < 0 ? -(index + 1) : index + 0]
      .m_type.each([&is_type](const PatternType &) mutable constexpr->decltype(
          auto) { is_type = true; });
  return is_type;
}

constexpr decltype(auto)
parser::get_string(const pattern_base::char_set &cs) const {
  space strings{m_string_pool.data()};
  auto string =
      strings.truncate(strings.span() - m_string_indices[cs.m_pool_index])
          .rebase(cs.m_pool_index
                      ? m_string_indices[cs.m_pool_index - 1]
                      : decltype(trait_type::string_pool_length()){0});
  return string;
}

template <typename Tracer>
constexpr decltype(auto) parser::convert_to_tracer(concrete<Tracer> &tracer) {
  return optional{ntuple{tracer.base(),
                         tracer.template func<match_op::MATCH, pattern, pattern,
                                              space<const char *, integer<12>>,
                                              integer<12>, integer<12>>(),
                         tracer.template func<match_op::FAIL, void, pattern,
                                              space<const char *, integer<12>>,
                                              integer<12>, integer<12>>()}};
}

constexpr decltype(auto) parser::match(
    pattern p, space<const trait_type::value_type *, integer<12>> input,
    optional<
        ntuple<ref<any &>,
               function<pattern (any::*)(constant<match_op::MATCH>, pattern,
                                         space<const char *, integer<12>>,
                                         integer<12>, integer<12>)>,
               function<void (any::*)(constant<match_op::FAIL>, pattern,
                                      space<const char *, integer<12>>,
                                      integer<12>, integer<12>)>>>
        tracer) {
  if (&p.m_parser.get() != this)
    return false;

  struct {
    // constexpr decltype(auto) rotate(decltype(parse_stack_read) f,
    //                                 decltype(parse_stack_read) m,
    //                                 decltype(parse_stack_read) l) {

    //   auto pf = parse_stack.begin() + f, pm = parse_stack.begin() + m,
    //        pl = parse_stack.begin() + l;
    //   auto lpf = pf;

    //   auto mid = pm;
    //   while (pf != pm && pm != pl) {
    //     while (pf != mid && pm != pl) {
    //       auto t = *pm;
    //       *pm++ = *pf;
    //       *pf++ = t;
    //     }
    //     if (pf == mid)
    //       mid = pm;
    //     if (pm == pl) {
    //       pm = mid;
    //       if (lpf == parse_stack.begin() + f)
    //         lpf = pf;
    //     }
    //   }
    //   return decltype(parse_stack_read)(pl - lpf);
    // }
    ref<parser &> m_parser;
    decltype(input) m_input;
    decltype(tracer) m_tracer;
    using pattern_index_type = decltype(trait_type::max_patterns());
    buffer<pattern_index_type, trait_type::stack_size()>
        m_stack{}; // hi -> lo pattern chain
    using stack_frame_index_type = decltype(integer{m_stack.count()});
    stack_frame_index_type m_program_counter = 0;
    buffer<stack_frame_index_type, trait_type::stack_depth()> m_stack_frames{};
    buffer<stack_frame_index_type, trait_type::stack_depth()> m_stack_returns{};
    using stack_frame_count_type = decltype(trait_type::stack_depth());
    /*
    One-past-the-end of m_stack_returns.
    */
    stack_frame_count_type m_stack_frame_count = 0;
    /*

    */
    buffer<pattern_index_type, trait_type::retry_size()>
        m_retry{}; // hi -> lo pattern choice
    using retry_frame_index_type = decltype(integer{m_retry.count()});
    buffer<retry_frame_index_type, trait_type::retry_depth()> m_retry_frames{};
    using parse_position_type = decltype(input)::pointer_type;
    buffer<parse_position_type, trait_type::retry_depth()> m_retry_position{};
    /*
    Pointer into m_stack.

    Retry return address is required because we may have proceeded matching
    quite far down a chain. If the match fails, we want to jump right back to
    the retry without unwinding the stack.

    What if retry return address is at a higher stack, we return to a lower
    stack, but still need to backtrack, potentially overwriting the retry
    pattern?
    */
    bool m_match_on_fail = false;
    /*
    Only to be set when rolling back during failure lands on a non-choice retry
    pattern.

    For those patterns, they match against the null string first. Subsequent
    retries advance the starting position. This flag signals when the pattern is
    being processed for retry when they are encountered in the main loop.

    If those patterns fail, they can set set this flag before processing the
    failure to signal that we can finally pop them off the retry stack.
    */
    buffer<stack_frame_index_type, trait_type::retry_depth()> m_retry_return{};
    using retry_frame_count_type = decltype(integer{m_retry_position.count()});
    retry_frame_count_type m_retry_frame_count = 0;

    constexpr decltype(auto) stack_top() const {
      return m_stack_frames[m_stack_frame_count];
    }
    constexpr decltype(auto) stack_top() {
      return m_stack_frames[m_stack_frame_count];
    }
    constexpr decltype(auto) stack_top_frame_empty() const {
      return m_stack_frame_count > 0
                 ? (stack_top() == m_stack_frames[m_stack_frame_count - 1])
                 : (stack_top() == 0);
    }
    constexpr decltype(auto) retry_top() const {
      return m_retry_frames[m_retry_frame_count];
    }
    constexpr decltype(auto) retry_top() {
      return m_retry_frames[m_retry_frame_count];
    }
    constexpr decltype(auto) parse_retry_pos() const {
      return m_retry_position[m_retry_frame_count];
    }
    constexpr decltype(auto) parse_retry_pos() {
      return m_retry_position[m_retry_frame_count];
    }
    constexpr decltype(auto) retry_return_address() const {
      return m_retry_return[m_retry_frame_count - 1];
    }
    constexpr decltype(auto) retry_return_position() const {
      return m_retry_position[m_retry_frame_count - 1];
    }
    constexpr decltype(auto) retry_return_position() {
      return m_retry_position[m_retry_frame_count - 1];
    }
    constexpr decltype(auto) retry_top_frame_empty() const {
      return m_retry_frame_count > 0
                 ? (retry_top() == m_retry_frames[m_retry_frame_count - 1])
                 : (retry_top() == 0);
    }
    constexpr decltype(auto) push_stack(pattern_index_type index) {
      if (m_parser.get().is_type(index, type_v<pattern_base::trace>)) {
        m_stack[stack_top()] = trait_type::max_patterns();
        ++stack_top();
      }
      m_stack[stack_top()] = index;
      ++stack_top();
    }
    constexpr decltype(auto) push_retry(pattern_index_type index) {
      m_retry[retry_top()] = index;
      ++retry_top();
    }
    /*
    -->
    <h4 id="defn-parser-match-stack-push_retry_return"><code>
    */
    constexpr decltype(auto) push_stack_return(stack_frame_index_type counter) {
      /*
      </code></h4>
      <pre>
      */
      m_stack_returns[m_stack_frame_count] = counter;
      /*
      </pre>
      <p class="comment">Save where we are for when the submatch succeeds.</p>
      <pre>
      */
      ++m_stack_frame_count;
      /*
      </pre>
      <p class="comment">Push a new frame onto pattern stack.</p>
      <pre>
      */
      stack_top() = m_stack_frames[m_stack_frame_count - 1];
      /*
      </pre>
      <p class="comment">Prepare for pushing new items onto the current pattern
      frame.</p>
      <pre>
      <!--
      */
    }
    constexpr decltype(auto) push_stack_one(pattern_index_type index,
                                            bool with_return) {
      if (with_return)
        push_stack_return(m_program_counter);
      push_stack(index);
      m_program_counter = stack_top() - 1;
    }
    /*
    -->
    <h4 id="defn-parser-match-stack-push_stack"><code>
    */
    constexpr decltype(auto) push_stack(optional<pattern_base::chain> chain) {
      /*
      </code></h4>
      <!--
      */
      push_stack_return(m_program_counter);
      /*
      -->
      <pre>
      */
      while (chain) {
        /*
        </pre>
        <p class="comment">Traverse the linked list until the end, when the next
        pattern is no longer a <em>chain</em> pattern.</p>
        <pre>
        */
        push_stack(chain.value().m_next.value());
        /*
        </pre>
        <p class="comment">Linked list is in reverse order, but we also move the
        program counter in the same direction.</p>
        <!--
        */
        m_parser.get()
            .m_patterns[chain.value().m_pattern_index.value()]
            .m_type.each(
                [ this, &chain ](auto &&p) mutable constexpr->decltype(auto) {
                  /*
                  -->
                  <pre>
                  */
                  if constexpr (type_v<decltype(p)>.remove_cvref() ==
                                type_v<pattern_base::chain>)
                    chain = p;
                  /*
                  </pre>
                  <p class="comment"></p>
                  <pre>
                  */
                  else {
                    push_stack(chain.value().m_pattern_index.value());
                    chain.reset();
                    /*
                    </pre>
                    <p class="comment">Breaks the loop.</p>
                    <!--
                    */
                  }
                });
      }

      m_program_counter = stack_top() - 1;
    }
    constexpr decltype(auto) push_retry_frame() {
      /*
      </code></h4>
      <pre>
      */
      m_retry_return[m_retry_frame_count] = m_program_counter;
      /*
      </pre>
      <p class="comment">Save where we are for fast rollback.</p>
      <pre>
      */
      ++m_retry_frame_count;
      parse_retry_pos() = retry_return_position();
      /*
      </pre>
      <p class="comment">Push a new frame onto retry stack.</p>
      <pre>
      */
      retry_top() = m_retry_frames[m_retry_frame_count - 1];
      /*
      </pre>
      <p class="comment">Prepare for pushing new items onto the current retry
      frame.</p>
      <pre>
      */
    }
    constexpr decltype(auto) push_retry_one() {
      push_retry_frame();
      push_retry(m_stack[m_program_counter]);
    }
    /*
    -->
    <h4 id="defn-parser-match-stack-push_retry"><code>
    */
    constexpr decltype(auto) push_retry(optional<pattern_base::choice> choice) {
      push_stack_return(m_program_counter);
      push_retry_frame();
      while (choice) {
        /*
        </pre>
        <p class="comment">Traverse the linked list until the end, when the next
        pattern is no longer a <em>choice</em> pattern.</p>
        <pre>
        */
        push_retry(choice.value().m_next.value());
        /*
        </pre>
        <p class="comment">Linked list is in reverse order, but we also try the
        next alternative in the same direction.</p>
        <!--
        */
        m_parser.get()
            .m_patterns[choice.value().m_pattern_index.value()]
            .m_type.each(
                [ this, &choice ](auto &&p) mutable constexpr->decltype(auto) {
                  /*
                  -->
                  <pre>
                  */
                  if constexpr (type_v<decltype(p)>.remove_cvref() ==
                                type_v<pattern_base::choice>)
                    choice = p;
                  /*
                  </pre>
                  <p class="comment"></p>
                  <pre>
                  */
                  else {
                    push_retry(choice.value().m_pattern_index.value());
                    choice.reset();
                    /*
                    </pre>
                    <p class="comment">Breaks the loop.</p>
                    <!--
                    */
                  }
                });
      }

      push_stack_one(m_retry[retry_top() - 1], false);
    }
    constexpr stack_frame_count_type
    locate_stack_frame(stack_frame_index_type index) const {
      for (auto frame = m_stack_frame_count; frame >= 0; --frame)
        if (m_stack_frames[frame] <= index)
          return frame;
      return 0;
    }
    constexpr decltype(auto) at_frame_boundary() const {
      return m_stack_frames[locate_stack_frame(m_program_counter)] ==
             m_program_counter;
    }
    constexpr stack_frame_count_type
    locate_retry_return_frame(stack_frame_index_type index) const {
      for (auto frame = m_retry_frame_count - 1; frame >= 0; --frame)
        if (m_retry_return[frame] == index)
          return frame;
      return 0;
    }
    constexpr decltype(auto) highest_retry() const {
      if (m_retry_frame_count == 0)
        return stack_frame_index_type{};
      auto highest_retry = retry_return_address();
      for (auto frame = 0; frame < m_retry_frame_count; ++frame)
        highest_retry = highest_retry < m_retry_return[frame]
                            ? m_retry_return[frame]
                            : highest_retry;
      return highest_retry;
    }
    constexpr decltype(auto) is_arbitrary(stack_frame_index_type index) const {
      return m_parser.get().is_type(m_stack[index],
                                    type_v<pattern_base::arbitrary_pattern>);
    }
    constexpr decltype(auto)
    in_arbitrary(stack_frame_index_type retry_return_index,
                 stack_frame_index_type return_index) const {
      if (!is_arbitrary(retry_return_index))
        return false;
      auto stack_frame = locate_stack_frame(return_index);
      for (; stack_frame; stack_frame = locate_stack_frame(return_index)) {
        return_index = m_stack_returns[stack_frame];
        if (return_index == retry_return_index)
          return true;
      }
      return false;
    }
    constexpr decltype(auto) is_choice(stack_frame_index_type index) const {
      return m_parser.get().is_type(m_stack[index],
                                    type_v<pattern_base::choice>);
    }
    constexpr decltype(auto) is_retry(stack_frame_index_type index) const {
      return m_parser.get().is_type(m_stack[index],
                                    type_v<pattern_base::retry>);
    }
    constexpr decltype(auto) is_no_retry(stack_frame_index_type index) const {
      return m_parser.get().is_type(m_stack[index],
                                    type_v<pattern_base::no_retry>);
    }
    constexpr decltype(auto) is_trace(stack_frame_index_type index) const {
      return m_stack[index] < 0;
    }
    constexpr decltype(auto)
    commit(optional<stack_frame_index_type> index = {}) {
      // Drop alternatives and retries, for single frame or whole parse.
      if (!index) {
        m_retry_position[0] = parse_retry_pos();
        for (; m_retry_frame_count > 0; --m_retry_frame_count)
          m_stack[retry_return_address()] = trait_type::max_patterns();
      } else
        m_retry_frame_count = locate_retry_return_frame(index.value());

      auto highest = highest_retry();
      if (m_program_counter > highest)
        highest = m_program_counter;
      m_stack_frame_count = locate_stack_frame(highest) + 1;
      stack_top() = highest + 1;
    }
    constexpr decltype(auto) trace_success(integer<12> successpos) {
      if (m_tracer && is_trace(m_program_counter))
        // Predicates are never at frame boundary.
        m_stack[m_program_counter - 1] =
            m_tracer.value()
                .get(index_v<1>)(m_tracer.value().get(index_v<0>).get(),
                                 constant_v<MATCH>,
                                 pattern{m_parser.get(),
                                         {-(m_stack[m_program_counter] + 1)}},
                                 m_input,
                                 m_retry_position[locate_retry_return_frame(
                                     m_program_counter)],
                                 successpos)
                .m_index.value();
    }
    constexpr decltype(auto) success(parse_position_type offset) {
      if (m_program_counter == 0) {
        --m_program_counter;
        return;
      }

      bool has_non_choice_retry = ([this]() mutable constexpr->decltype(auto) {
        for (auto top =
                 m_stack_frames[locate_stack_frame(m_program_counter) + 1] - 1;
             top >= m_program_counter; --top)
          if (!is_choice(top) && is_retry(top))
            return true;
        return false;
      }());
      offset += parse_retry_pos();
      auto highest = highest_retry();
      while (at_frame_boundary()) {
        auto stack_frame = locate_stack_frame(m_program_counter);
        if (is_no_retry(m_stack_returns[stack_frame])) {
          commit(m_stack_returns[stack_frame]);
          highest = highest_retry();
        }
        m_program_counter = m_stack_returns[stack_frame];
        if (is_arbitrary(m_program_counter) && !has_non_choice_retry &&
            m_retry_position[locate_retry_return_frame(m_program_counter)] <
                offset) {
          push_retry_one();
          retry_return_position() = offset;
        }
        if (m_program_counter > highest) {
          m_stack_frame_count = stack_frame;
          stack_top() = m_program_counter + 1;
        }
      }

      trace_success(offset);
      parse_retry_pos() = offset;

      if (m_stack_frame_count < 0)
        return;

      --m_program_counter;
      highest = highest_retry();
      if (m_program_counter > highest)
        highest = m_program_counter;
      m_stack_frame_count = locate_stack_frame(highest) + 1;
      stack_top() = highest + 1;
    }
    constexpr decltype(auto) should_pop_retry() const {
      return is_choice(retry_return_address()) ||
             is_no_retry(retry_return_address()) ||
             in_arbitrary(retry_return_address(), m_program_counter);
    }
    constexpr decltype(auto) trace_fail(integer<12> failpos) {
      if (m_retry_frame_count && m_tracer && is_trace(retry_return_address()))
        m_tracer.value().get(index_v<2>)(
            m_tracer.value().get(index_v<0>).get(), constant_v<FAIL>,
            pattern{m_parser.get(), {-(m_stack[retry_return_address()] + 1)}},
            m_input, retry_return_position(), failpos);
    }
    constexpr decltype(auto) fail(integer<12> failpos) {
      if (failpos > m_input.span())
        failpos = m_input.span();
      trace_fail(failpos);
      auto try_next = [this](
                          auto &&extra_test,
                          auto &&get_next) mutable constexpr->decltype(auto) {
        if (!retry_top_frame_empty() && (extra_test || should_pop_retry())) {
          --retry_top();
          get_next();
        }
      };
      // Retry next alternative, or arb pattern from saved position.
      // Fenced patterns are handled during success parsing.

      // Pop all stack frames with no retry_return entries pointing inside them.
      // => Pop until highest retry_return address.
      // ==> Highest return address determined after we rollback to latest retry
      //     return with viable alternatives.
      try_next(m_match_on_fail, [this]() { m_match_on_fail = false; });
      /*
      <p class="comment">Current alternative fail, so pop off current retry
      frame.</p>
      */
      while (retry_top_frame_empty()) {
        --m_retry_frame_count;
        if (m_retry_frame_count < 0)
          return;
        /*
        <p class="comment">Keep popping retry frames until we hit a frame with
        at least one viable alternative. This handles the case of successive
        alternatives and we are on the last branch of all alternatives in the
        chain.</p>
        */
        trace_fail(failpos);
        try_next(false, []() {});
        /*
        <p class="comment">Current alternative fail, so pop off current retry
        frame.</p>
        */
      }

      if (!is_choice(retry_return_address())) {
        if (is_arbitrary(retry_return_address())) {
          auto highest = highest_retry();
          m_stack_frame_count = locate_stack_frame(highest) + 1;
          parse_retry_pos() = retry_return_position();
        }
        m_match_on_fail = true;
        m_program_counter = retry_return_address();
        return;
      }

      auto highest = highest_retry();
      m_stack_frame_count = locate_stack_frame(highest) + 1;
      if (!stack_top_frame_empty())
        stack_top() = highest + 1;
      if (!stack_top_frame_empty()) {
        ++m_stack_frame_count;
        stack_top() = m_stack_frames[m_stack_frame_count - 1];
      }
      push_stack_one(m_retry[retry_top() - 1], false);
      m_stack_returns[m_stack_frame_count - 1] = retry_return_address();
      parse_retry_pos() = retry_return_position();
    }
    constexpr decltype(auto) range_checker(pattern_base::char_range p) {
      return [p](auto &&c) mutable constexpr->decltype(auto) {
        return p.m_first <= c && c <= p.m_last;
      };
    }
    constexpr decltype(auto) set_checker(pattern_base::char_set p) {
      return [set = m_parser.get().get_string(p)](
                 auto &&c) mutable constexpr->decltype(auto) {
        for (auto [last, first] = buffer{{set.span(), {}}}; first != last;
             ++first)
          if (set[first].value() == c)
            return true;
        return false;
      };
    }
    constexpr decltype(auto) span_checker() {
      return [this](auto &&in_class) mutable constexpr->decltype(auto) {
        auto f = parse_retry_pos();
        for (; f != m_input.span(); ++f)
          if (in_class(m_input[f].value()))
            break;
        return f;
      };
    }
    constexpr decltype(auto) negate_handler() {
      return [](auto &&checker) {
        return [&checker](auto &&c) mutable constexpr->decltype(auto) {
          return !checker(c);
        };
      };
    }
    constexpr decltype(auto) chunk_handler() {
      return [this](auto &&chunk,
                    auto &&within_span) mutable constexpr->decltype(auto) {
        if (chunk > (m_input.span() - parse_retry_pos())) {
          fail(m_input.span());
          return false;
        }
        within_span();
        return true;
      };
    }
    constexpr decltype(auto) char_handler() {
      return [this](auto &&in_range) mutable constexpr->decltype(auto) {
        chunk_handler()(
            1, [ this, &in_range ]() mutable constexpr->decltype(auto) {
              if (in_range(m_input[parse_retry_pos()].value())) {
                success(1);
                return;
              }
              fail(parse_retry_pos() + 1);
            });
      };
    }
    constexpr decltype(auto) not_span_greedy_chunk_checker() {
      return [this](auto &&checker) mutable constexpr->decltype(auto) {
        return chunk_handler()(
            1, [ this, &checker ]() mutable constexpr->decltype(auto) {
              m_match_on_fail = false;
              parse_retry_pos() = span_checker()(
                  [&checker,
                   f = 0 ](auto &&c) mutable constexpr->decltype(auto) {
                    // Prevent trying to parse from the same position.
                    return f++ && checker(c);
                  });
            });
      };
    }
    constexpr decltype(auto) match_chars_on_fail_handler() {
      return [this](auto &&checker) mutable constexpr->decltype(auto) {
        if (m_match_on_fail) {
          if (!checker())
            return;
        } else
          push_retry_one();
        success(0);
      };
    }
    constexpr decltype(auto) handle_span(parse_position_type f) {
      if (f == parse_retry_pos())
        fail(parse_retry_pos() + 1);
      else
        success(f - parse_retry_pos());
    }
    constexpr decltype(auto) handle_inner(pattern_index_type index) {
      push_retry_one();
      push_stack_one(index, true);
    }
    constexpr decltype(auto) handle_chunk(parse_position_type chunk) {
      chunk_handler()(chunk, [ this, &chunk ]() mutable constexpr->decltype(
                                 auto) { success(chunk); });
    }
    constexpr decltype(auto) handle_anchor(parse_position_type anchor) {
      if (anchor != parse_retry_pos()) {
        fail(parse_retry_pos() + 1);
        return;
      }
      success(0);
    }
    constexpr decltype(auto) operator()(pattern_base::string p) {
      auto set = m_parser.get().get_string(p);
      chunk_handler()(
          set.span(), [ this, &set ]() mutable constexpr->decltype(auto) {
            auto checker = [&set, span = buffer{{set.span(), {}}} ](
                               auto c) mutable constexpr->decltype(auto) {
              return span[1] == span[0] || set[span[1]++].value() != c;
            };
            auto f = span_checker()(checker);
            if (f != set.span() + parse_retry_pos())
              fail(f + 1);
            else
              success(f - parse_retry_pos());
          });
    }
    constexpr decltype(auto) operator()(pattern_base::range p) {
      char_handler()(range_checker(p));
    }
    constexpr decltype(auto) operator()(pattern_base::not_range p) {
      char_handler()(negate_handler()(range_checker(p)));
    }
    constexpr decltype(auto) operator()(pattern_base::span_range p) {
      handle_span(span_checker()(negate_handler()(range_checker(p))));
    }
    constexpr decltype(auto) operator()(pattern_base::not_span_range p) {
      handle_span(span_checker()(range_checker(p)));
    }
    constexpr decltype(auto) operator()(pattern_base::not_span_range_greedy p) {
      match_chars_on_fail_handler()([ this, &p ]() mutable constexpr->decltype(
          auto) { return not_span_greedy_chunk_checker()(range_checker(p)); });
    }
    constexpr decltype(auto) operator()(pattern_base::set p) {
      char_handler()(set_checker(p));
    }
    constexpr decltype(auto) operator()(pattern_base::not_set p) {
      char_handler()(negate_handler()(set_checker(p)));
    }
    constexpr decltype(auto) operator()(pattern_base::span_set p) {
      handle_span(span_checker()(negate_handler()(set_checker(p))));
    }
    constexpr decltype(auto) operator()(pattern_base::not_span_set p) {
      handle_span(span_checker()(set_checker(p)));
    }
    constexpr decltype(auto) operator()(pattern_base::not_span_set_greedy p) {
      match_chars_on_fail_handler()([ this, &p ]() mutable constexpr->decltype(
          auto) { return not_span_greedy_chunk_checker()(set_checker(p)); });
    }
    constexpr decltype(auto) operator()(pattern_base::arbitrary_character) {
      match_chars_on_fail_handler()([this]() mutable constexpr->decltype(auto) {
        return chunk_handler()(1, [this]() mutable constexpr->decltype(auto) {
          m_match_on_fail = false;
          ++parse_retry_pos();
        });
      });
    }
    constexpr decltype(auto) operator()(pattern_base::arbitrary_pattern p) {
      match_chars_on_fail_handler()(
          [ this, &p ]() mutable constexpr->decltype(auto) {
            m_match_on_fail = false;
            push_stack_one(p.m_pattern_index.value(), true);
            return false;
          });
    }
    constexpr decltype(auto) operator()(pattern_base::remaining) {
      success(m_input.span() - parse_retry_pos());
    }
    constexpr decltype(auto) operator()(pattern_base::chain p) {
      push_stack(p);
    }
    constexpr decltype(auto) operator()(pattern_base::choice p) {
      push_retry(p);
    }
    constexpr decltype(auto) operator()(pattern_base::trace p) {
      m_stack[m_program_counter] = -(p.m_pattern_index.value() + 1);
    }
    constexpr decltype(auto) operator()(pattern_base::length p) {
      handle_chunk({p.m_value});
    }
    constexpr decltype(auto) operator()(pattern_base::position p) {
      handle_anchor({p.m_value});
    }
    constexpr decltype(auto) operator()(pattern_base::position_reverse p) {
      handle_anchor(m_input.span() - p.m_value);
    }
    constexpr decltype(auto) operator()(pattern_base::tab p) {
      handle_chunk(p.m_value - parse_retry_pos() % p.m_value);
    }
    constexpr decltype(auto) operator()(pattern_base::tab_reverse p) {
      handle_chunk((m_input.span() - parse_retry_pos()) % p.m_value);
    }
    constexpr decltype(auto) operator()(pattern_base::quit) {
      m_retry_frame_count = -1;
    }
    constexpr decltype(auto) operator()(pattern_base::found) {
      m_program_counter = -1;
    }
    constexpr decltype(auto) operator()(pattern_base::backtrack) {
      fail(parse_retry_pos() + 1);
    }
    constexpr decltype(auto) operator()(pattern_base::no_backtrack) {
      success(0);
      commit();
    }
    constexpr decltype(auto) operator()(pattern_base::no_retry p) {
      handle_inner(p.m_pattern_index.value());
    }
    constexpr decltype(auto) current_index() const {
      auto index = m_stack[m_program_counter];
      if (index >= 0)
        return index + 0;
      return -(index + 1);
    }
  } state{*this, input, tracer};

  state.push_stack_one(p.m_index.value(), false);
  while (state.m_program_counter > -1 && state.m_retry_frame_count > -1 &&
         state.m_retry_frame_count < state.m_retry_frames.count() &&
         state.m_stack_frame_count < state.m_stack_frames.count())
    if (state.current_index() == trait_type::max_patterns())
      state.success(0);
    else
      m_patterns[state.current_index()].m_type.each(state);

  return state.m_program_counter == -1;
}

constexpr pattern operator|(pattern l, pattern r) {
  if (&l.m_parser.get() != &r.m_parser.get() || !l.m_index || !r.m_index)
    return pattern{l.m_parser.get()};

  auto &&parser = l.m_parser.get();
  l = parser.choice(l);
  parser.representation(l).each(
      [&parser, &l, &
       r ](const pattern_base::choice &c) mutable constexpr->decltype(auto) {
        parser.m_patterns[l.m_index.value()] = pattern_base{
            type_v<pattern_base::choice>, c.m_pattern_index, r.m_index};
      });
  return l;
}

constexpr pattern operator&(pattern l, pattern r) {
  if (&l.m_parser.get() != &r.m_parser.get())
    return pattern{l.m_parser.get()};

  auto &&parser = l.m_parser.get();
  l = parser.chain(l);
  parser.representation(l).each([
    &parser, &l, &r
  ](const pattern_base::chain &c) mutable constexpr->decltype(auto) {
    parser.m_patterns[l.m_index.value()] =
        pattern_base{type_v<pattern_base::chain>, c.m_pattern_index, r.m_index};
  });
  return l;
}
} // namespace gut
