#pragma once

#include "pp.hpp"

#define GUT_PP_FOR(FUNC, CONTEXT, ARGS)                                        \
  GUT_PP_WHILE_0(FUNC, CONTEXT, GUT_PP_IDENT ARGS)
#define GUT_PP_LOOP_CONTEXT(s, ...) s
#define GUT_PP_LOOP_VAR(s, m, ...) m
#define GUT_PP_LOOP_CHECK_FIRST(s, m, f, ...) f
#define GUT_PP_LOOP_CHECK_LAST(s, m, f, l, ...) l
#define GUT_PP_LOOP_INDEX(s, m, f, l, i) i
#define GUT_PP_COMMA ,
#define GUT_PP_IDENT(...) __VA_ARGS__
#define GUT_PP_SINK(...)
#define GUT_PP_STRINGIZE(...) GUT_PP_STRINGIZE_(__VA_ARGS__)
#define GUT_PP_CONCAT(a, b) GUT_PP_CONCAT_(a, b)
#define GUT_PP_HEAD(...) GUT_PP_HEAD_(__VA_ARGS__, )
#define GUT_PP_TAIL(...) GUT_PP_FOR(GUT_PP_REMOVE_HEAD, , (__VA_ARGS__))
#define GUT_PP_ISEMPTY(...)                                                    \
  GUT_PP_ISEMPTY_(GUT_PP_CONCAT(GUT_PP_COMMA_, GUT_PP_HEAD(__VA_ARGS__))())
#define GUT_PP_IF(COND, T, F)                                                  \
  GUT_PP_IDENT(GUT_PP_CONCAT(GUT_PP_CONCAT(GUT_PP_IF, COND),                   \
                             _INVERT)()(T)GUT_PP_CONCAT(GUT_PP_IF, COND)()(F))
#define GUT_PP_VAROPT(...)                                                     \
  GUT_PP_IF(GUT_PP_ISEMPTY(__VA_ARGS__), GUT_PP_SINK, GUT_PP_COMMA_)()
#define GUT_PP_SHIFTLEFT(...) GUT_PP_SHIFTLEFT_(__VA_ARGS__, )
#define GUT_PP_ARG_COUNT(...)                                                  \
  GUT_PP_IF(GUT_PP_ISEMPTY(__VA_ARGS__), 0,                                    \
            GUT_PP_FOR(GUT_PP_CHECK, , (__VA_ARGS__)))

#define GUT_PP_FIRST_GUT_PP_NOTLAST(x)
#define GUT_PP_NOTFIRST_GUT_PP_LAST(x)
#define GUT_PP_FIRST_GUT_PP_LAST(x)
#define GUT_PP_NOTFIRST_GUT_PP_NOTLAST(x) x
#define GUT_PP_NOTFIRST(x) x
#define GUT_PP_FIRST(x)
#define GUT_PP_NOTLAST(x) x
#define GUT_PP_LAST(x)
#define GUT_PP_FIRST_GUT_PP_NOTLAST_INVERT(x) x
#define GUT_PP_NOTFIRST_GUT_PP_LAST_INVERT(x) x
#define GUT_PP_FIRST_GUT_PP_LAST_INVERT(x) x
#define GUT_PP_NOTFIRST_GUT_PP_NOTLAST_INVERT(x)
#define GUT_PP_NOTFIRST(x) x
#define GUT_PP_FIRST(x)
#define GUT_PP_NOTLAST(x) x
#define GUT_PP_LAST(x)
#define GUT_PP_NOTFIRST_INVERT(x)
#define GUT_PP_FIRST_INVERT(x) x
#define GUT_PP_NOTLAST_INVERT(x)
#define GUT_PP_LAST_INVERT(x) x
#define GUT_PP_CHECK(...)                                                      \
  GUT_PP_CONCAT(GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__), _INVERT)                  \
  (GUT_PP_LOOP_INDEX(__VA_ARGS__))
#define GUT_PP_WHILE_CONTEXT(...) GUT_PP_HEAD(GUT_PP_SHIFTLEFT(__VA_ARGS__))
#define GUT_PP_WHILE_ARGS(...) GUT_PP_SHIFTLEFT(GUT_PP_SHIFTLEFT(__VA_ARGS__))
#define GUT_PP_WHILE_ISLAST(...)                                               \
  GUT_PP_IF(GUT_PP_ISEMPTY(GUT_PP_WHILE_ARGS(__VA_ARGS__)), GUT_PP_LAST,       \
            GUT_PP_NOTLAST)
#define GUT_PP_WHILE_CALLFUNC(FUNC, ISFIRST, INDEX, ...)                       \
  FUNC(GUT_PP_HEAD(__VA_ARGS__), GUT_PP_WHILE_CONTEXT(__VA_ARGS__), ISFIRST,   \
       GUT_PP_WHILE_ISLAST(__VA_ARGS__), INDEX)
#define GUT_PP_COMMA_(...) GUT_PP_COMMA
#define GUT_PP_STRINGIZE_(...) #__VA_ARGS__
#define GUT_PP_CONCAT_(a, b) a##b
#define GUT_PP_HEAD_(h, ...) h
#define GUT_PP_EMPTY_LIST(...) __VA_ARGS__, TRUE, FALSE,
#define GUT_PP_EMPTY_COUNT_(_0, _1, COUNT, ...) COUNT
#define GUT_PP_EMPTY_COUNT(...) GUT_PP_EMPTY_COUNT_(__VA_ARGS__)
#define GUT_PP_ISEMPTY_(...) GUT_PP_EMPTY_COUNT(GUT_PP_EMPTY_LIST(__VA_ARGS__))
#define GUT_PP_IFFALSE() GUT_PP_IDENT
#define GUT_PP_IFTRUE() GUT_PP_SINK
#define GUT_PP_IFFALSE_INVERT() GUT_PP_SINK
#define GUT_PP_IFTRUE_INVERT() GUT_PP_IDENT
#define GUT_PP_SHIFTLEFT_(h, ...) __VA_ARGS__,

#define GUT_PP_REPLACE_HEAD(h, ...)                                            \
  GUT_PP_FOR(GUT_PP_REPLACE_HEAD_, h, __VA_ARGS__)
#define GUT_PP_REMOVE_HEAD(...)                                                \
  GUT_PP_LOOP_CHECK_FIRST(__VA_ARGS__)                                         \
  (GUT_PP_LOOP_VAR(__VA_ARGS__))                                               \
      GUT_PP_CONCAT(GUT_PP_LOOP_CHECK_FIRST(__VA_ARGS__),                      \
                    GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__))(GUT_PP_COMMA)
#define GUT_PP_REPLACE_HEAD_(...)                                              \
  GUT_PP_CONCAT(GUT_PP_LOOP_CHECK_FIRST(__VA_ARGS__), _INVERT)                 \
  (GUT_PP_LOOP_CONTEXT(__VA_ARGS__))                                           \
      GUT_PP_LOOP_CHECK_FIRST(__VA_ARGS__)(GUT_PP_LOOP_VAR(__VA_ARGS__))       \
          GUT_PP_LOOP_CHECK_LAST(__VA_ARGS__)(GUT_PP_COMMA)
