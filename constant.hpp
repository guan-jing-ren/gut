#pragma once

#include "config.hpp"

namespace gut {
template <auto Constant> struct constant {
  constexpr static auto value = Constant;
  constexpr operator decltype(Constant)() const { return Constant; }
};

template <auto Constant>
inline constexpr constant constant_v = constant<Constant>{};

/*
<p>Cannot put at top because we need the full constant definition.</p>
*/
template <typename...>
inline constexpr constant is_constant_v = constant_v<false>;
template <auto C>
inline constexpr constant is_constant_v<constant<C>> = constant_v<true>;
} // namespace gut
