#include "something.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

#include "concepts.hpp"
#include "nothing.hpp"

using namespace gut;

namespace {
struct unused {};
static_assert(
    sizeof(something) == 1 && std::is_trivial_v<something>,
    "Special `something` is supposed to be the most useless type possible.");
static_assert(std::is_assignable_v<something, unused>,
              "Special `something` is supposed to be assignable by anything.");
static_assert(std::is_assignable_v<unused, something>,
              "Special `something` is supposed to be convertible to anything.");

GUT_MEMFN(callablewithanything,
          operator()(std::declval<callablewithanything_args>()...));

static_assert(is_callablewithanything_v<something, nothing>,
              "Special `something` should be callable with anything.");
} // namespace
