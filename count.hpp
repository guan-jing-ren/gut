#pragma once

#include "constant.hpp"

namespace gut {
using count_t = std::intmax_t;

template <count_t Count> struct count;

template <typename...> inline constexpr constant is_count_v = constant_v<false>;
template <count_t Count>
inline constexpr constant is_count_v<count<Count>> = constant_v<true>;

template <count_t Count> struct count {
  constexpr static auto value() { return Count; }
  constexpr operator count_t() const { return Count; }
};

template <auto Count>
inline constexpr count count_v = count<static_cast<count_t>(Count)>{};

template <count_t MinCount> struct count_min : private count<MinCount> {
  using base_type = count<MinCount>;
  using base_type::value;
  using base_type::operator count_t;
};

template <count_t MaxCount> struct count_max : private count<MaxCount> {
  using base_type = count<MaxCount>;
  using base_type::value;
  using base_type::operator count_t;
};
} // namespace gut
