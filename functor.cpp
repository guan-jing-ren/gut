/*
<td>
<textarea readonly rows="40" cols="80">
*/
#include "functor.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

#include "buffer.hpp"

using namespace gut;

namespace {
static_assert(
    type_v<ntuple<>> ==
    remove_cvref_v<decltype(woven_arguments(
        std::declval<ntuple<>>(), std::declval<ntuple<arg, arg, arg>>()))>);
static_assert(
    type_v<ntuple<int, int, int>> ==
    type_v<decltype(woven_arguments(std::declval<ntuple<int &, arg, int &>>(),
                                    std::declval<ntuple<int &>>()))>);
static_assert(
    type_v<ntuple<int, int, int>> ==
    type_v<decltype(woven_arguments(std::declval<ntuple<int &, arg, int &>>(),
                                    std::declval<ntuple<int &&>>()))>);
static_assert(type_v<ntuple<int, int, arg, int>> ==
              type_v<decltype(woven_arguments(
                  std::declval<ntuple<int &, arg, arg, int &>>(),
                  std::declval<ntuple<int &>>()))>);
static_assert(type_v<ntuple<int, arg, int, int>> ==
              type_v<decltype(woven_arguments(
                  std::declval<ntuple<int &, arg, arg, int &>>(),
                  std::declval<ntuple<arg, int &&>>()))>);

constexpr auto lmb = [](auto, auto, auto) {};
static_assert(arity_v<remove_cvref_t<decltype(lmb)>> == 3);
constexpr function array_capture([a = buffer{{0, 1, 2, 3, 4, 5, 6, 7, 8,
                                              9}}]() constexpr->decltype(auto) {
  return (a);
});
static_assert(array_capture()[0] == 0);
static_assert(array_capture()[1] == 1);
static_assert(array_capture()[2] == 2);
static_assert(array_capture()[3] == 3);
static_assert(array_capture()[4] == 4);
static_assert(array_capture()[4] == 4);
static_assert(array_capture()[5] == 5);
static_assert(array_capture()[5] == 5);
static_assert(array_capture()[6] == 6);
static_assert(array_capture()[6] == 6);
static_assert(array_capture()[7] == 7);
static_assert(array_capture()[8] == 8);
static_assert(array_capture()[9] == 9);
struct What {
  constexpr bool operator()(int i, int j, int k) const { return i + j - k > 0; }
};
static_assert(arity_v<What> == 3);
static_assert(arity_v<constant<&What::operator()>> == 4);
constexpr function sf{What{}};
static_assert(sizeof(sf) == 1);
static_assert(!sf(1, 2, 3));
static_assert(!sf(1, 2, arg{})(3));
constexpr char unnamed(int, long, void *) { return ' '; }
constexpr function sfunc{unnamed};
static_assert(sizeof(sfunc) == sizeof(&unnamed));
static_assert(sizeof(sfunc) == sizeof(std::intptr_t));
static_assert(sfunc.arity() == 3);
static_assert(sfunc(0, 0, nullptr) == ' ');
struct Unnamed {
  constexpr bool operator()(short, unsigned) const { return true; }
};
constexpr function mfunc{&Unnamed::operator()};
static_assert(sizeof(mfunc) == sizeof(&Unnamed::operator()));
static_assert(sizeof(mfunc) >= sizeof(std::intptr_t));
static_assert(mfunc.arity() == 3);
static_assert(mfunc(Unnamed{}, 0, 0));
constexpr function ofunc{Unnamed{}};
static_assert(ofunc.arity() == 2);
static_assert(ofunc(0, 0));

constexpr function csfunc{constant_v<unnamed>};
static_assert(sizeof(csfunc) == 1);
static_assert(csfunc.arity() == 3);
static_assert(csfunc(0, arg_v, nullptr)(1) == ' ');
constexpr function cmfunc{constant_v<&Unnamed::operator()>};
static_assert(sizeof(cmfunc) == 1);
static_assert(cmfunc.arity() == 3);
static_assert(cmfunc(Unnamed{}, 0, 0));

constexpr auto lambda = [](char c, short s, auto i, long l, long long ll) {
  return ntuple{c, s, i, l, ll};
};
static_assert(is_callable_v<decltype(lambda), something, something, something,
                            something, something>);
constexpr function f0{lambda};
constexpr function f0_0 = f0(arg_v, arg_v, arg_v, arg_v, arg_v);
static_assert(type_v<decltype(f0_0)> == type_v<decltype(f0)>);
constexpr function f1 = f0_0(arg_v, arg_v, 2, arg_v, arg_v);
constexpr function f2 = f1(char{0}, arg_v, arg_v, arg_v);
constexpr function f3 = f2(arg_v, arg_v, 4ll);
constexpr function f4 = f3(arg_v, 3l);
static_assert(type_v<decltype(f0.callable())> ==
              type_v<decltype(f4.callable())>);
static_assert(sizeof(f0) == 1);
static_assert(sizeof(f0_0) == 1);
static_assert(sizeof(f1) == 12);
static_assert(sizeof(f2) == 16);
static_assert(sizeof(f3) == 24);
static_assert(sizeof(f4) == 32);
static_assert(sizeof(f4.arguments()) == 24);
static_assert(sizeof(f4.callable()) == 1);
static_assert(f2(10, 20, 10).reduce([](auto &&... f) { return (... + f); }) ==
              42ll);
static_assert(type_v<decltype(f4(1))> ==
              type_v<ntuple<char, short, int, long, long long>>);
static_assert(type_v<decltype(f4(11))> ==
              type_v<ntuple<char, short, int, long, long long>>);
static_assert(f4(11).reduce([](auto &&... f) { return (... + f); }) == 20);

constexpr auto farray =
    arg_v[arg_v](buffer{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}}, arg_v);
static_assert(sizeof(farray) == 44);
static_assert(farray(0) == 0);
static_assert(farray(1) == 1);
static_assert(farray(2) == 2);
static_assert(farray(3) == 3);
static_assert(farray(4) == 4);
static_assert(farray(5) == 5);
static_assert(farray(6) == 6);
static_assert(farray(7) == 7);
static_assert(farray(8) == 8);
static_assert(farray(9) == 9);
constexpr auto findex = (arg_v[arg_v] == arg_v)(arg_v, 5);
static_assert(sizeof(findex) == 12);
static_assert(findex(buffer{{37, 38, 39, 40, 41, 42, 43, 44, 45}}, 42));
constexpr buffer bufferref{{37, 38, 39, 40, 41, 42, 43, 44, 45}};
static_assert(type_v<decltype(findex(bufferref, 42))> == type_v<bool>);
static_assert(findex(bufferref, 42));

constexpr auto side_effect = []() {
  auto farray_ = farray;
  farray_(0) = 1;
  if (farray_(0) == 0)
    return 3;

  auto ffunc = (arg{} = arg{});
  int i = 0;
  ffunc(ref(i), function{[]() { return 6; }}());
  if (i == 6)
    ffunc(i, 5);
  return i;
}();
static_assert(side_effect == 5);
static_assert(function{[]() { return 5; }}() == 5);

constexpr auto fixed_args = arg_v(1, 2, 3, 4, 5, 6, 7, 8, 9);
constexpr auto sum_args = fixed_args([](auto &&... i) { return (... + i); });
static_assert(sum_args == (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9));
constexpr auto mult_args = fixed_args([](auto &&... i) { return (... * i); });
static_assert(mult_args == (1 * 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9));
constexpr auto bisum = (arg_v + arg_v) + (arg_v + arg_v);
static_assert(bisum.arity() == 4);
static_assert(bisum(1, 2, 3, 4) == 10);
constexpr auto bisummult = (arg_v + arg_v) * (arg_v + arg_v);
static_assert(bisummult(1, 2, 3, 4) == 21);
constexpr auto precedence = arg_v + arg_v * arg_v + arg_v;
static_assert(precedence(1, 2, 3, 4) == 11);
constexpr auto lefttoright = arg_v * arg_v + arg_v + arg_v;
static_assert(lefttoright(1, 2, 3, 4) == 9);
struct H {
  constexpr char operator->*(int i) { return 'a' + i; };

  template <typename Member>
  constexpr decltype(auto) operator->*(Member H::*m) {
    return this->*m;
  }

  template <typename Member>
  constexpr decltype(auto) operator->*(Member H::*m) const {
    return this->*m;
  }

  constexpr int operator()() { return 21; }
  constexpr int operator()(int i) { return i; }

  int val = 42;
};
static_assert((arg_v->*function([](auto &&a) constexpr->decltype(auto) {
                return (a);
              }))(arg_v, 5)(H{}) == 'f');
static_assert((arg_v->*arg_v + arg_v)(arg_v, &H::val, arg_v)(H{}, arg_v)(2) -
                  2 ==
              42);
static_assert((arg_v->*arg_v + arg_v)(arg_v, &H::val, arg_v)(arg_v, 2)(H{}) -
                  2 ==
              42);
static_assert((arg_v->*arg_v + arg_v)(H{}, arg_v, arg_v)(arg_v, 2)(&H::val) -
                  2 ==
              42);
static_assert((arg_v->*arg_v + arg_v)(H{}, arg_v, 2)(&H::val) - 2 == 42);
static_assert((H{}->*arg_v + arg_v)(arg_v, 2)(&H::val) - 2 == 42);
static_assert((H{}->*arg_v)(&H::val) == 42);
static_assert((arg_v->*(&H::val) == arg_v)(H{})(42));
static_assert((arg_v->*arg_v == arg_v)(H{}, &H::val)(42));
static_assert(function{(arg_v->*arg_v == arg_v)}(H{}, &H::val)(42));
static_assert((arg_v->*arg_v == arg_v)(H{}, arg_v)(&H::val)(42));
static_assert((arg_v->*arg_v == arg_v)(arg_v, &H::val)(H{})(42));
constexpr auto deref = arg_v->*arg_v == 42;
static_assert(deref(H{}, &H::val));
static_assert(function{deref}(H{}, &H::val));
static_assert(deref(H{}, arg_v)(&H::val));
static_assert(deref(arg_v, &H::val)(H{}));
static_assert((arg_v() + arg_v())(H{}, H{}) == 42);
static_assert((arg_v() + arg_v())(H{}, arg_v)(H{}) == 42);
static_assert((arg_v() + arg_v())(arg_v, H{})(H{}) == 42);
static_assert((arg_v() + H{}())(H{}) == 42);
static_assert((arg_v(20) + H{}())(H{}) == 41);
constexpr auto bicallsum = (arg_v() + arg_v());
static_assert(bicallsum(H{}, H{}) == 42);
static_assert(bicallsum(H{}, arg_v)(H{}) == 42);
static_assert(bicallsum(arg_v, H{})(H{}) == 42);
} // namespace
/*
</textarea>
</td>
</tr>
</table>
*/
