#pragma once

#include "type.hpp"

namespace gut {
template <typename DeclaredType> struct thing_trait {
  using declared_type = DeclaredType;
  using value_type = remove_cvref_t<declared_type>;
  using storage_type =
      std::conditional_t<std::is_reference_v<declared_type>,
                         std::remove_reference_t<declared_type> *,
                         std::conditional_t<std::is_empty_v<DeclaredType>,
                                            DeclaredType, declared_type>>;
  constexpr static auto declared_type_v();
  constexpr static auto value_type_v();
  constexpr static auto storage_type_v();
};

template <typename EmptyType>
struct thing_empty : private thing_trait<EmptyType>, EmptyType {
  using base_type = thing_trait<EmptyType>;
  using declared_type = typename base_type::declared_type;
  using value_type = typename base_type::value_type;
  using storage_type = typename base_type::storage_type;
  constexpr static auto base_type_v();
  using base_type::declared_type_v;
  using base_type::storage_type_v;
  using base_type::value_type_v;

  constexpr thing_empty() = default;
  constexpr thing_empty(const thing_empty &) = default;
  constexpr thing_empty(thing_empty &&) = default;
  constexpr thing_empty &operator=(const thing_empty &) = default;
  constexpr thing_empty &operator=(thing_empty &&) = default;

  constexpr thing_empty(value_type);
  constexpr decltype(auto) operator=(value_type);

  constexpr const value_type &get() const;
  constexpr value_type &get();
};

template <typename> struct thing_ref;
template <typename...>
inline constexpr constant is_thing_ref_v = constant_v<false>;
template <typename RefType>
inline constexpr constant is_thing_ref_v<thing_ref<RefType>> = constant_v<true>;

template <typename RefType> struct thing_ref : private thing_trait<RefType> {
  using base_type = thing_trait<RefType>;
  using declared_type = typename base_type::declared_type;
  using value_type = typename base_type::value_type;
  using storage_type = typename base_type::storage_type;
  constexpr static auto base_type_v();
  using base_type::declared_type_v;
  using base_type::storage_type_v;
  using base_type::value_type_v;

  constexpr thing_ref() = delete;
  constexpr thing_ref(const thing_ref &) = default;
  constexpr thing_ref(thing_ref &&) = default;
  constexpr thing_ref &operator=(const thing_ref &) = default;
  constexpr thing_ref &operator=(thing_ref &&) = default;

  template <typename ThatValueType>
  constexpr explicit thing_ref(const thing_ref<ThatValueType> &);
  template <typename ThatValueType>
  constexpr explicit thing_ref(thing_ref<ThatValueType> &);
  template <typename ThatValueType>
  constexpr explicit thing_ref(thing_ref<ThatValueType> &&);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(const thing_ref<ThatValueType> &);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(thing_ref<ThatValueType> &);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(thing_ref<ThatValueType> &&);

  constexpr thing_ref(const value_type &);
  constexpr thing_ref(value_type &);
  constexpr thing_ref(value_type &&);
  constexpr decltype(auto) operator=(const value_type &);
  constexpr decltype(auto) operator=(value_type &);
  constexpr decltype(auto) operator=(value_type &&);

  constexpr const declared_type get() const;
  constexpr declared_type get();

private:
  storage_type m_ref;
};
template <typename RefType,
          typename = std::enable_if_t<!is_thing_ref_v<remove_cvref_t<RefType>>>>
thing_ref(RefType &&)->thing_ref<RefType>;

template <typename ValueType>
struct thing_val : private thing_trait<ValueType> {
  using base_type = thing_trait<ValueType>;
  using declared_type = typename base_type::declared_type;
  using value_type = typename base_type::value_type;
  using storage_type = typename base_type::storage_type;
  constexpr static auto base_type_v();
  using base_type::declared_type_v;
  using base_type::storage_type_v;
  using base_type::value_type_v;

  constexpr thing_val() = default;
  constexpr thing_val(const thing_val &) = default;
  constexpr thing_val(thing_val &&) = default;
  constexpr thing_val &operator=(const thing_val &) = default;
  constexpr thing_val &operator=(thing_val &&) = default;

  template <typename ThatValueType>
  constexpr explicit thing_val(const thing_val<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr explicit thing_val(thing_val<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr explicit thing_val(thing_val<ThatValueType> &&that);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(const thing_val<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(thing_val<ThatValueType> &that);
  template <typename ThatValueType>
  constexpr decltype(auto) operator=(thing_val<ThatValueType> &&that);

  constexpr thing_val(const value_type &);
  constexpr thing_val(value_type &);
  constexpr thing_val(value_type &&);
  constexpr decltype(auto) operator=(const value_type &);
  constexpr decltype(auto) operator=(value_type &);
  constexpr decltype(auto) operator=(value_type &&);

  constexpr const value_type &get() const;
  constexpr value_type &get();

private:
  storage_type m_val;
};
template <typename ValueType>
thing_val(ValueType &&)->thing_val<remove_cvref_t<ValueType>>;

template <typename DeclaredType>
using thing_base = std::conditional_t<
    std::is_reference_v<DeclaredType>, thing_ref<DeclaredType>,
    std::conditional_t<
        std::is_empty_v<DeclaredType> &&
            std::is_trivially_default_constructible_v<DeclaredType>,
        thing_empty<DeclaredType>, thing_val<DeclaredType>>>;

template <typename> struct thing;
template <typename...> inline constexpr constant is_thing_v = constant_v<false>;
template <typename DeclaredType>
inline constexpr constant is_thing_v<thing<DeclaredType>> = constant_v<true>;

template <typename DeclaredType>
struct thing : private thing_base<DeclaredType> {
  using base_type = thing_base<DeclaredType>;
  using declared_type = typename base_type::declared_type;
  using value_type = typename base_type::value_type;
  using storage_type = typename base_type::storage_type;
  constexpr static auto base_type_v();
  using base_type::declared_type_v;
  using base_type::storage_type_v;
  using base_type::value_type_v;

  constexpr thing() = default;
  constexpr thing(const thing &) = default;
  constexpr thing(thing &&) = default;
  constexpr thing &operator=(const thing &) = default;
  constexpr thing &operator=(thing &&) = default;

  template <typename ThatDeclaredType>
  constexpr explicit thing(const thing<ThatDeclaredType> &that);
  template <typename ThatDeclaredType>
  // constexpr explicit thing(thing<ThatDeclaredType> &that);
  // template <typename ThatDeclaredType>
  constexpr explicit thing(thing<ThatDeclaredType> &&that);
  template <typename ThatDeclaredType>
  constexpr decltype(auto) operator=(const thing<ThatDeclaredType> &that);
  // template <typename ThatDeclaredType>
  // constexpr decltype(auto) operator=(thing<ThatDeclaredType> &that);
  template <typename ThatDeclaredType>
  constexpr decltype(auto) operator=(thing<ThatDeclaredType> &&that);

  using base_type::base_type;
  using base_type::operator=;
  using base_type::get;
};
template <typename ValueType,
          typename = std::enable_if_t<!is_thing_v<remove_cvref_t<ValueType>>>>
thing(ValueType &&)->thing<remove_cvref_t<ValueType>>;

template <typename RefType> struct ref : thing<RefType> {
  using base_type = thing<RefType>;
  using declared_type = typename base_type::declared_type;
  using value_type = typename base_type::value_type;
  using storage_type = typename base_type::storage_type;
  constexpr static auto base_type_v();
  using base_type::declared_type_v;
  using base_type::storage_type_v;
  using base_type::value_type_v;

  using base_type::base_type;
  using base_type::operator=;
  using base_type::get;
};
template <typename RefType,
          typename = std::enable_if_t<!is_thing_ref_v<remove_cvref_t<RefType>>>>
ref(RefType &&)->ref<RefType>;

////////////////////
// IMPLEMENTATION //
////////////////////

template <typename DeclaredType>
constexpr auto thing_trait<DeclaredType>::declared_type_v() {
  return type_v<declared_type>;
}

template <typename DeclaredType>
constexpr auto thing_trait<DeclaredType>::value_type_v() {
  return type_v<value_type>;
}

template <typename DeclaredType>
constexpr auto thing_trait<DeclaredType>::storage_type_v() {
  return type_v<storage_type>;
}

template <typename DeclaredType>
constexpr auto thing_empty<DeclaredType>::base_type_v() {
  return type_v<base_type>;
}

template <typename DeclaredType>
constexpr thing_empty<DeclaredType>::thing_empty(value_type) {}

template <typename DeclaredType>
constexpr decltype(auto) thing_empty<DeclaredType>::operator=(value_type) {
  return *this;
}

template <typename DeclaredType>
constexpr auto thing_empty<DeclaredType>::get() const -> const value_type & {
  return *this;
}

template <typename DeclaredType>
constexpr auto thing_empty<DeclaredType>::get() -> value_type & {
  return *this;
}
template <typename RefType> constexpr auto thing_ref<RefType>::base_type_v() {
  return type_v<base_type>;
}

template <typename RefType>
template <typename ThatValueType>
constexpr thing_ref<RefType>::thing_ref(const thing_ref<ThatValueType> &that)
    : m_ref(that.m_ref) {}

template <typename RefType>
template <typename ThatValueType>
constexpr thing_ref<RefType>::thing_ref(thing_ref<ThatValueType> &that)
    : m_ref(that.m_ref) {}

template <typename RefType>
template <typename ThatValueType>
constexpr thing_ref<RefType>::thing_ref(thing_ref<ThatValueType> &&that)
    : m_ref(that.m_ref) {}

template <typename RefType>
template <typename ThatValueType>
constexpr decltype(auto) thing_ref<RefType>::
operator=(const thing_ref<ThatValueType> &that) {
  m_ref = that.m_ref;
  return *this;
}

template <typename RefType>
template <typename ThatValueType>
constexpr decltype(auto) thing_ref<RefType>::
operator=(thing_ref<ThatValueType> &that) {
  m_ref = that.m_ref;
  return *this;
}

template <typename RefType>
template <typename ThatValueType>
constexpr decltype(auto) thing_ref<RefType>::
operator=(thing_ref<ThatValueType> &&that) {
  m_ref = that.m_ref;
  return *this;
}

template <typename RefType>
constexpr thing_ref<RefType>::thing_ref(const value_type &that)
    : m_ref(std::addressof(that)) {}

template <typename RefType>
constexpr thing_ref<RefType>::thing_ref(value_type &that)
    : m_ref(std::addressof(that)) {}

template <typename RefType>
constexpr thing_ref<RefType>::thing_ref(value_type &&that)
    : m_ref(std::addressof(that)) {}

template <typename RefType>
constexpr decltype(auto) thing_ref<RefType>::operator=(const value_type &that) {
  *m_ref = that;
  return *this;
}

template <typename RefType>
constexpr decltype(auto) thing_ref<RefType>::operator=(value_type &that) {
  *m_ref = that;
  return *this;
}

template <typename RefType>
constexpr decltype(auto) thing_ref<RefType>::operator=(value_type &&that) {
  *m_ref = that;
  return *this;
}

template <typename RefType>
constexpr auto thing_ref<RefType>::get() const -> const declared_type {
  return static_cast<const declared_type>(*m_ref);
}

template <typename RefType>
constexpr auto thing_ref<RefType>::get() -> declared_type {
  return static_cast<declared_type>(*m_ref);
}

template <typename ValueType>
constexpr auto thing_val<ValueType>::base_type_v() {
  return type_v<base_type>;
}

template <typename ValueType>
template <typename ThatValueType>
constexpr thing_val<ValueType>::thing_val(const thing_val<ThatValueType> &that)
    : thing_val(that.get()) {}

template <typename ValueType>
template <typename ThatValueType>
constexpr thing_val<ValueType>::thing_val(thing_val<ThatValueType> &that)
    : thing_val(that.get()) {}

template <typename ValueType>
template <typename ThatValueType>
constexpr thing_val<ValueType>::thing_val(thing_val<ThatValueType> &&that)
    : thing_val(std::move(that.get())) {}

template <typename ValueType>
template <typename ThatValueType>
constexpr decltype(auto) thing_val<ValueType>::
operator=(const thing_val<ThatValueType> &that) {
  m_val = that.get();
  return *this;
}

template <typename ValueType>
template <typename ThatValueType>
constexpr decltype(auto) thing_val<ValueType>::
operator=(thing_val<ThatValueType> &&that) {
  m_val = std::move(that.get());
  return *this;
}

template <typename DeclaredType>
constexpr thing_val<DeclaredType>::thing_val(const value_type &that)
    : m_val(that) {}

template <typename DeclaredType>
constexpr thing_val<DeclaredType>::thing_val(value_type &that) : m_val(that) {}

template <typename DeclaredType>
constexpr thing_val<DeclaredType>::thing_val(value_type &&that)
    : m_val(std::move(that)) {}

template <typename DeclaredType>
constexpr decltype(auto) thing_val<DeclaredType>::
operator=(const value_type &that) {
  m_val = that;
  return *this;
}

template <typename DeclaredType>
constexpr decltype(auto) thing_val<DeclaredType>::operator=(value_type &that) {
  m_val = that;
  return *this;
}

template <typename DeclaredType>
constexpr decltype(auto) thing_val<DeclaredType>::operator=(value_type &&that) {
  m_val = std::move(that);
  return *this;
}

template <typename DeclaredType>
constexpr auto thing_val<DeclaredType>::get() const -> const value_type & {
  return m_val;
}

template <typename DeclaredType>
constexpr auto thing_val<DeclaredType>::get() -> value_type & {
  return m_val;
}

template <typename DeclaredType>
constexpr auto thing<DeclaredType>::base_type_v() {
  return type_v<base_type>;
}

template <typename DeclaredType>
template <typename ThatDeclaredType>
constexpr thing<DeclaredType>::thing(const thing<ThatDeclaredType> &that)
    : base_type(that.get()) {}

// template <typename DeclaredType>
// template <typename ThatDeclaredType>
// constexpr thing<DeclaredType>::thing(thing<ThatDeclaredType> &that)
//     : base_type(that.get()) {}

template <typename DeclaredType>
template <typename ThatDeclaredType>
constexpr thing<DeclaredType>::thing(thing<ThatDeclaredType> &&that)
    : base_type(std::move(that.get())) {}

template <typename DeclaredType>
template <typename ThatDeclaredType>
constexpr decltype(auto) thing<DeclaredType>::
operator=(const thing<ThatDeclaredType> &that) {
  base_type::operator=(that.get());
  return *this;
}

// template <typename DeclaredType>
// template <typename ThatDeclaredType>
// constexpr decltype(auto) thing<DeclaredType>::
// operator=(thing<ThatDeclaredType> &that) {
//   base_type::operator=(that.get());
//   return *this;
// }

template <typename DeclaredType>
template <typename ThatDeclaredType>
constexpr decltype(auto) thing<DeclaredType>::
operator=(thing<ThatDeclaredType> &&that) {
  base_type::operator=(std::move(that.get()));
  return *this;
}
} // namespace gut
