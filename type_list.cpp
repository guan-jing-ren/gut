#include "type_list.hpp"

#if !__has_include("config.hpp")
#error "All modules must include config.hpp."
#endif

using namespace gut;

namespace {
static_assert(type_v<int> == to_type_v<int>,
              "Expected raw type to be adapted.");
static_assert(type_v<int> == to_type_v<type<int>>,
              "Expected type adapter to collapse.");

constexpr auto idxs = make_index_list<5>();
constexpr auto list = make_type_list<int, short, long, short, char>();
constexpr auto list_count = list.count();
constexpr auto short_type = get_type<1>(list);
constexpr auto short_indices = get_indices<short>(list);
static_assert(!is_type_list_v<decltype(list)>,
              "Reference of type list is not a type list.");
static_assert(is_type_list_v<std::decay_t<decltype(list)>>,
              "type list should be type list.");
static_assert(idxs[index_v<0>] == 0, "Incorrect index as specified.");
static_assert(idxs[index_v<1>] == 1, "Incorrect index as specified.");
static_assert(idxs[index_v<2>] == 2, "Incorrect index as specified.");
static_assert(idxs[index_v<3>] == 3, "Incorrect index as specified.");
static_assert(idxs[index_v<4>] == 4, "Incorrect index as specified.");
static_assert(idxs.count() == 5, "Incorrect index list size as specified.");
static_assert(list_count == 5, "Incorrect number of types in list.");
static_assert(short_type == type_v<short>,
              "Expected short at index as specified.");
static_assert(short_indices[index_v<0>] == 1,
              "Incorrect index for type as specified.");
static_assert(short_indices[index_v<1>] == 3,
              "Incorrect index for type as specified.");
enum Idx {
  a = 20,
  b = 30,
  c = 40,
};

constexpr auto constants = make_constant_list<a, b, c>();
static_assert(constant_index<a>(constants) == 0);
static_assert(constant_index<b>(constants) == 1);
static_assert(constant_index<c>(constants) == 2);
static_assert(get_constant<0>(constants) == 20);
static_assert(get_constant<1>(constants) == 30);
static_assert(get_constant<2>(constants) == 40);
} // namespace
